define([
    'worldCanvas',
    'utils/cloning',
    'utils/range',
    'observe/subject',
    'evolution/score',
    //'evolution/rouletteWheelSelectionByFitness',
    'evolution/rouletteWheelSelectionByRank',
    //'evolution/tournamentSelection',
    'genetics/genome',
    'entities/options',
    'entities/animal'
],
    function (worldCanvas, cloning, Range, Subject, Score, Selection, Genome, entitiesOptions, Animal, undefined) {
        "use strict";

        var canvas = worldCanvas;

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = entitiesOptions.animal;

            this.populationSize = options.populationSize.get();
        }

        var initialRunNumbers = {
            size: 0,
            ageTotal: 0,
            gainedEnergyTotal: 0,
            usedEnergyTotal: 0,
            lostEnergyTotal: 0,
            netEnergyTotal: 0,
            healthTotal: 0,
            fittest: 0
        };

        function createGenome() {
            return new Genome(15, 6);
        }

        function createRandomPosition() {
            var range = new Range(-0.8, 0.8);
            return {
                // Starting position and angle
                x: canvas.width / 2 + canvas.width / 2 * range.random(),
                y: canvas.height / 2 + canvas.height / 2 * range.random(),
                a: Math.random() * Math.PI * 2
            };
        }

        function createRandomPositions(count) {
            var positions = [];

            for (var i = 0; i < count; i++) {
                positions.push(createRandomPosition())
            }

            return positions;
        }

        function recordAnimalNumbers(numbers, entity) {
            numbers.ageTotal += entity.life.age;
            numbers.gainedEnergyTotal += entity.life.gainedEnergy;
            numbers.usedEnergyTotal += entity.life.usedEnergy;
            numbers.lostEnergyTotal += entity.life.lostEnergy;
            numbers.netEnergyTotal += entity.life.netEnergy();
            numbers.healthTotal += entity.life.health();
        }

        function recordPopulationNumbers(numbers, population) {
            numbers.size = population.entities.length;
            numbers.fittest = population.entities[0].life.health();
        }

        var Self = function () {
            var options = new Options();

            this.runNumbers = cloning.shallowClone(initialRunNumbers);
            this.entities = [];
            this.winningAnimal = {};
            this.entityCreated = new Subject();
            this.newWinnerNotifier = new Subject();
            this.entityRunNotifier = new Subject();

            var that = this;
            this.fitnessSelection = new Selection(
                function (index) {
                    var entity = that.entities[index];
                    return new Score(entity, entity.life.score());
                }
            );
        };

        Self.prototype = (function () {
            function sortSuccess() {
                /*jshint validthis: true */
                this.entities.sort(function (a, b) {
                    return b.life.score() - a.life.score();
                });
            }

            function addEntity(entity) {
                this.entityCreated.notify(entity);
                entity.population = this;
                this.entities.push(entity);
            }

            var findBest = function () {
                var winner = this.entities[0];

                if (winner !== this.winningAnimal) {
                    this.winningAnimal = winner;
                    this.newWinnerNotifier.notify(winner);
                }
            };

            var selectParents = function () {
                var parents = [];
                for (var i = 0; i < 2; i++) {
                    var winningEntity = this.fitnessSelection.select(this.entities.length);

                    parents.push(winningEntity);
                }

                return parents;
            };

            return {
                createEntities: function () {
                    var options = new Options();

                    // Fill the population with entities
                    for (var i = 0; i < options.populationSize; i++) {
                        var genome = createGenome();
                        var position = createRandomPosition();
                        var entity = new Animal(genome, position);

                        addEntity.call(this, entity);
                    }
                },

                addAnimal: function (animal) {
                    addEntity.call(this, animal);
                },

                getState: function () {
                    sortSuccess.call(this);
                    var genomeStates = [];

                    for (var i = 0; i < this.entities.length; i++) {
                        var genome = this.entities[i].getGenome();
                        genomeStates.push(genome.getState());
                    }

                    return { genomes: genomeStates };
                },

                loadState: function (genomeStates, merge) {
                    if (!merge) this.entities = [];

                    var genomes = genomeStates.genomes;

                    for (var i = 0; i < genomes.length; i++) {
                        var genome = new Genome(undefined, undefined, genomes[i]);
                        var position = createRandomPosition();
                        var animal = new Animal(genome, position);
                        addEntity.call(this, animal);
                    }
                },

                run: function (plants) {
                    var tempNumbers = cloning.shallowClone(initialRunNumbers);
                    sortSuccess.call(this);

                    for (var i = 0; i < this.entities.length; i++) {
                        var entity = this.entities[i];

                        entity.run(plants, this.entities);
                        this.entityRunNotifier.notify(entity);

                        recordAnimalNumbers(tempNumbers, entity);

                        // Check entity lifecycle and remove dead entities
                        if (entity.lives()) continue;

                        this.entities.splice(i, 1);
                    }

                    if (this.entities.length === 0) return;

                    // Find the best scoring entity
                    findBest.call(this);

                    recordPopulationNumbers(tempNumbers, this);
                    cloning.shallowMixin(tempNumbers, this.runNumbers);

                    // Make sure the population doesn't shrink below the size
                    var options = new Options();
                    if (this.entities.length > options.populationSize - 2) return;

                    var parents = selectParents.call(this);
                    var positions = createRandomPositions(2);
                    var children = parents[0].produceChildren(parents[1], positions);

                    for (var i = 0; i < children.length; i++) {
                        addEntity.call(this, children[i]);
                    }
                }
            };
        })();

        return Self;
    }
);