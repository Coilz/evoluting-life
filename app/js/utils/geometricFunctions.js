/*
    Original source code from:
    http://bl.ocks.org/milkbread/11000965
*/

define([],
    function (undefined) {
        "use strict";

        // BASIC GEOMETRIC functions
        function distance (a,b) {
            return Math.sqrt( Math.pow(a[0]-b[0], 2) + Math.pow(a[1]-b[1], 2) );
        }

        function is_on (a, b, c) {
            return distance(a,c) + distance(c,b) == distance(a,b);
        }

        function intersectionPoint (a, b, d, tdt) {
            /*jshint validthis: true */
            return {
                coords: [
                    (tdt * d[0]) + a[0],
                    (tdt * d[1]) + a[1]
                ], 
                onLine: is_on(a, b, this.coords)
            };
        }

        // GEOMETRIC function to get the intersections
        var getIntersections = function (a, b, c) {
            // Calculate the euclidean distance between a & b
            eDistAtoB = Math.sqrt( Math.pow(b[0]-a[0], 2) + Math.pow(b[1]-a[1], 2) );

            // compute the direction vector d from a to b
            d = [ (b[0]-a[0])/eDistAtoB, (b[1]-a[1])/eDistAtoB ];

            // Now the line equation is x = dx*t + ax, y = dy*t + ay with 0 <= t <= 1.

            // compute the value t of the closest point to the circle center (cx, cy)
            t = (d[0] * (c[0]-a[0])) + (d[1] * (c[1]-a[1]));

            // compute the coordinates of the point e on line and closest to c
            var e = {
                coords: [
                    (t * d[0]) + a[0],
                    (t * d[1]) + a[1]
                ],
                onLine: false
            };

            // Calculate the euclidean distance between c & e
            eDistCtoE = Math.sqrt( Math.pow(e.coords[0]-c[0], 2) + Math.pow(e.coords[1]-c[1], 2) );

            // test if the line intersects the circle
            if( eDistCtoE < c[2] ) {
                // compute distance from t to circle intersection point
                dt = Math.sqrt( Math.pow(c[2], 2) - Math.pow(eDistCtoE, 2));

                return {
                    points: {
                        intersection1: intersectionPoint(a, b, d, t-dt),
                        intersection2: intersectionPoint(a, b, d, t+dt)
                    },
                    pointOnLine: e
                };

            } else if (parseInt(eDistCtoE) === parseInt(c[2])) {
                // console.log("Only one intersection");
                return {points: false, pointOnLine: e};
            } else {
                // console.log("No intersection");
                return {points: false, pointOnLine: e};
            }
        };

        var getAngles = function (a, b, c) {
            // calculate the angle between ab and ac
            angleAB = Math.atan2( b[1] - a[1], b[0] - a[0] );
            angleAC = Math.atan2( c[1] - a[1], c[0] - a[0] );
            angleBC = Math.atan2( b[1] - c[1], b[0] - c[0] );
            angleA = Math.abs((angleAB - angleAC) * (180/Math.PI));
            angleB = Math.abs((angleAB - angleBC) * (180/Math.PI));
            return [angleA, angleB];
        };

        return {
            getIntersections: getIntersections,
            getAngles: getAngles
        };

    }
);