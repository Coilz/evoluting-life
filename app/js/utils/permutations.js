define([],
    function (undefined) {
        "use strict";

        function createBinaryNumberWithLeadingZeroes(positionCount, number) {
            var binaryString  = number.toString(2);
            var leadingZeroCount = positionCount - binaryString.length;
            if (leadingZeroCount) {
                binaryString = new Array(leadingZeroCount+1).join("0") + binaryString;
            }

            return binaryString;
        }

        function createPermutations (a, b, positionCount) {
            var values = [a,b];
            var result = [];

            var permutationCount = Math.pow(2, positionCount);
            while (permutationCount--) {
                var permutationString  = createBinaryNumberWithLeadingZeroes(positionCount, permutationCount);

                var permutationArray = permutationString.split('');
                var permutationValues = [];
                for (var i=0; i<permutationArray.length; i++) {
                    permutationValues.push(values[permutationArray[i]]);
                }
                result.push(permutationValues);
            }

            return result;
        }

        var Self = function (getA, getB, positionCount) {
            var permutations = createPermutations(getA, getB, positionCount);

            this.getRandomPermutation = function () {
                var i = Math.floor( Math.random() * permutations.length );
                return permutations[i];
            };

            this.getRandomPermutationPair = function () {
                var i = Math.floor( Math.random() * permutations.length );
                return [
                    permutations[permutations.length - 1 - i],
                    permutations[i]
                ];
            };
        };

        return Self;
    }
);