define([],
    function (undefined) {
        "use strict";

        var shallowMixin = function (source, dest) {
            for (var prop in source) {
                dest[prop] = source[prop];
            }

            return dest;
        };

        var shallowClone = function (source) {
            return shallowMixin(source, {});
        };

        var mixin = function (source, dest, copyFunc) {
            var name, s, i, empty = {};
            for (name in source) {
                // the (!(name in empty) || empty[name] !== s) condition avoids copying properties in "source"
                // inherited from Object.prototype.  For example, if dest has a custom toString() method,
                // don't overwrite it with the toString() method that source inherited from Object.prototype
                s = source[name];
                if (!(name in dest) || (dest[name] !== s && (!(name in empty) || empty[name] !== s))) {
                    dest[name] = copyFunc ? copyFunc(s) : s;
                }
            }

            return dest;
        };

        var clone = function (src) {
            if (!src || typeof src != "object" || Object.prototype.toString.call(src) === "[object Function]") {
                // null, undefined, any non-object, or function
                return src; // anything
            }
            if (src.nodeType && "cloneNode" in src) {
                // DOM Node
                return src.cloneNode(true); // Node
            }
            if (src instanceof Date) {
                // Date
                return new Date(src.getTime()); // Date
            }
            if (src instanceof RegExp) {
                // RegExp
                return new RegExp(src);   // RegExp
            }

            var r, i, l;
            if (src instanceof Array) {
                // array
                r = [];
                for (i = 0, l = src.length; i < l; ++i) {
                    if (i in src) {
                        r.push(clone(src[i]));
                    }
                }
                // we don't clone functions for performance reasons
                //      }else if(d.isFunction(src)){
                //          // function
                //          r = function(){ return src.apply(this, arguments); };
            } else {
                // generic objects
                r = src.constructor ? new src.constructor() : {};
            }

            return mixin(src, r, clone);
        };

        return {
            shallowMixin: shallowMixin,
            shallowClone: shallowClone,
            mixin: mixin,
            clone: clone
        };
    }
);