define([],
    function (undefined) {
        "use strict";

        var Range = function(lower, upper) {
            if (lower === undefined) lower = 0;
            if (upper === undefined) upper = 0;

            if (lower > upper) throw "upper (" + upper + ") smaller than lower (" + lower + ")";

            return {
                random: function () {
                    return lower + Math.random() * (upper - lower);
                },

                check: function (value) {
                    return (value > upper) ? upper : (value < lower) ? lower : value;
                },

                checkLower: function (value) {
                    return (value < lower) ? lower : value;
                },

                checkUpper: function (value) {
                    return (value > upper) ? upper : value;
                },

                mutation: function (mutationFraction) {
                    var randomFraction = new Range(-1 * mutationFraction, mutationFraction).random();
                    return (upper - lower) * randomFraction;
                }
            };
        };

        return Range;
    }
);