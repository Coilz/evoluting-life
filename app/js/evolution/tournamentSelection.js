define([],
    function (undefined) {
        "use strict";

        var tournamentPercentage = 0.10;

        var Self = function (callback) {
            this.getScore = callback;
        };

        Self.prototype = {
            select: function (scoreCount) {
                var bestScore;
                var tournamentSize = Math.floor( scoreCount * tournamentPercentage );

                while ( tournamentSize-- ) {
                    var randomIndex = Math.floor( Math.random() * (scoreCount-1) );

                    var score = this.getScore(randomIndex);
                    if (bestScore === undefined || score.value > bestScore.value) {
                        bestScore = score;
                    }
                }

                return bestScore.context;
            }
        };

        return Self;
    }
);