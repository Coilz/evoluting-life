define([
  'utils/range',
],
    function (Range, undefined) {
        "use strict";

        var Self = function (callback) {
            this.getScore = callback;
        };

        Self.prototype = {
            select: function (scoreCount) {
                var randomOccurenceSum = new Range(1, scoreCount * (scoreCount+1) / 2 ).random();

                for ( var i = 0; i < scoreCount; i++ ) {
                    var occurence = scoreCount - i - 1;
                    var occurenceSum = occurence * (occurence+1) / 2;

                    if (randomOccurenceSum > occurenceSum) return this.getScore(i).context;
                }

                throw "rouletteWheelSelectionByRank didn't select a score: " + randomOccurenceSum;
            }
        };

        return Self;
    }
);

/*
rank based would be using: n(n+1)/2
For instance, in a population of ten individuals,
the chance of the first-ranked individual is 10 to 55, the chance of the 10th individual is 1 to 55

rank  occ   occSum
0     10    55
1      9    45
2      8    36
3      7    28
4      6    21
5      5    15
6      4    10
7      3    6
8      2    3
9      1    1
*/

/*
Scaled Rank

SP = Selective Pressure (2.0 >= SP >= 1.0, so that SP is the selective pressure of the fittest and 2-SP is the selective pressure of the weakest)
n = population size
Pos = position of an individual sorted by score (Pos=1 is the weakest, Pos=n is the fittest)

Rank (Pos) = 2-SP + 2 * (SP-1) * (Pos-1)/(n-1)

2 * (SP-1) = 9 * (2-SP)
2SP - 2 = 18 - 9SP
20=11SP
SP=20/11 = 1.818181...

10 = x * (2-20/11 + 2 * (20/11-1))
x = 10 / (2-20/11 + 2 * (20/11-1)) => 20/11 = 1 + 9/11
x = 10 / (1 - 9/11 + 2 * 9/11)
x = 10 / (2/11 + 1 + 7/11)
x = 10 / (1 + 9/11)
x = 10 * 11/20
x = 11/2 = 5.5

x = 5.5

Rank(3) = 2 - 20/11 + 2 * (20/11 - 1) * (2/9)
        = 2/11 + 2 * 9/11 * 2/9
        = 2/11 + 9/11 * 4/9
        = 2/11 + 4/11 = 6/11

        11/2 * 6/11 = 6/2 = 3 = ok
*/