define([],
    function (undefined) {
        "use strict";
        
        var Self = function(context, score) {
            this.context = context;
            this.value = score;
        };

        Self.prototype = {
        };

        return Self;
    }
);