define([],
    function (undefined) {
        "use strict";
        // This works fine at the beginning, but towards the end, the fitness values of the individuals vary only slightly
        var Self = function (callback) {
            this.getScore = callback;
        };

        Self.prototype = {
            select: function (scoreCount) {
                var weightedList = [];
                for ( var i = 0; i < scoreCount; i++ ) {
                    var score = this.getScore(i);

                    var successFactor = Math.floor(score.value);
                    for ( var j = 0; j < successFactor; j++ ) {
                        weightedList.push( score );
                    }
                }

                var randomIndex = Math.floor( Math.random() * weightedList.length );
                var winningScore = weightedList[ randomIndex ];

                return winningScore.context;
            }
        };

        return Self;
    }
);