define([],
    function (undefined) {
        "use strict";

        var someFunction = function() {
        };

        return {someFunction: someFunction};
    }
);