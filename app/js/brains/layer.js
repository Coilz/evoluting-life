define([
    'brains/neuron'
    ],
    function (Neuron, undefined) {
        "use strict";

        var Self = function (genomeLayer, targetLayer) {
            var neurons = [];

            var i = genomeLayer.length;
            var targetNeurons = targetLayer === undefined ? undefined : targetLayer.getNeurons();
            while (i--) {
                neurons.push(new Neuron(genomeLayer[i], targetNeurons));
            }

            this.getNeurons = function () {
                return neurons;
            };
        };

        Self.prototype = {
            transmit: function() {
                var excitations = [];
                var neurons = this.getNeurons();
                for (var i=0; i<neurons.length; i++) {
                    excitations.push(
                        neurons[i].transmit()
                    );
                }

                return excitations;
            }
        };

        return Self;
    }
);