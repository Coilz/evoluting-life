define([
    'brains/axon'
    ],
    function (Axon, undefined) {
        "use strict";

        var Self = function(gene, targetNeurons) {
            var axons = [];
            var i = gene.axons.length;

            if (targetNeurons === undefined) {
                while (i--) { // Assemble an output neuron
                    axons.push(new Axon(gene.axons[i])); // TODO: check if this is a valid situation
                }
            } else { // Assemble a neuron that transmits to its axons
                while (i--) {
                    axons.push(new Axon(gene.axons[i], targetNeurons[i]));
                }
            }

            this.getAxons = function () {
                return axons;
            };

            this.getThreshold = function () {
                return gene.threshold;
            };

            this.getRelaxation = function () {
                return gene.relaxation;
            };

            this.excitation = 0;
        };

        Self.prototype = {
            excite: function (value) {
                this.excitation += value;
            },

            transmit: function () {
                if (this.excitation > this.getThreshold()) {
                    var excitation = this.excitation;
                    this.excitation = 0;

                    var axons = this.getAxons();
                    for (var i=0; i<axons.length; i++) {
                        axons[i].transmit();
                    }

                    return excitation;
                }
                else {
                    this.excitation = this.excitation > 0 ? this.excitation * (1-this.getRelaxation()) : 0;
/*
                    var axons = this.getAxons();
                    for (var i=0; i<axons.length; i++) {
                        axons[i].weaken();
                    }
*/
                    return 0;
                }
            }
        };

        return Self;
    }
);