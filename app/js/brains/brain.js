define([
	'brains/layer',
	'brains/inputLayer'
],
	function (Layer, InputLayer, undefined) {
		"use strict";

		var Self = function (gen) {
			var layers = [];
			var genLayers = gen.getLayers(); // TODO: make sure there are 2 or more layers

			// The output layer has no target layer
			var layer = new Layer(genLayers[0]);
			layers.push(layer);

			// The other layers have target layers
			for (var i = 1; i < genLayers.length - 1; i++) {
				layer = new Layer(genLayers[i], layers[layers.length - 1]);
				layers.push(layer);
			}

			// The input layer is of a different type
			layer = new InputLayer(genLayers[genLayers.length - 1], layers[layers.length - 1]);
			layers.push(layer);

			this.getLayers = function () {
				return layers;
			};
		};

		Self.prototype = {
			think: function (input) {
				var layers = this.getLayers();

				// Excite the input neurons
				layers[layers.length - 1].sense(input);

				var output;
				var i = layers.length;
				while (i--) {
					output = layers[i].transmit();
				}

				return output;
			}
		};

		return Self;
	}
);