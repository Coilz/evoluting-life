define([
    'option'
    ],
    function (Option, undefined) {
        "use strict";

        var options = {

            brain: {

                reset: function() {
                }
            },

            reset: function() {
                this.brain.reset();
            }
        };

        return options;
    }
);
