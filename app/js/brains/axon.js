define([],
  function(undefined) {
    "use strict";

    var Self = function(axonGene, targetNeuron) {
      var strength = axonGene.strength;

      this.getStrength = function() {
        return strength;
      };

      this.transmit = function() {
        targetNeuron.excite(strength);
        //strength *= (1 + axonGene.strengthening);
      };

      this.weaken = function() {
        //strength *= (1 - axonGene.weakening);
      };
    };

    Self.prototype = {
    };

    return Self;
  }
);