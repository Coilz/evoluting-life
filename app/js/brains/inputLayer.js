define([
  'brains/layer'
],
  function(Layer, undefined) {
    "use strict";

    var Self = function(genomeLayer, targetLayer) {
      Layer.call(this, genomeLayer, targetLayer);
    };

    Self.prototype = Object.create(Layer.prototype);
    Self.prototype.sense = function(input) {
      // Excite the input neurons
      var inputNeurons = this.getNeurons();
      var i = inputNeurons.length;
      while (i--) {
        inputNeurons[i].excite(input[i]);
      }
    };

    return Self;
  }
);