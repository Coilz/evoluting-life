define([
    'option'
    ],
    function (Option, undefined) {
        "use strict";
        
        var options = {

            eyes: {

                reset: function() {
                }
            },

            reset: function() {
                this.eyes.reset();
            }
        };

        return options;
    }
);