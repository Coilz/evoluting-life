define(['worldCanvas'],
  function(world, undefined) {
    "use strict";

    var Self = function(owner, sensorGen) {
      this.viewDistance = sensorGen.viewDistance;
      this.fieldOfView = sensorGen.fieldOfView;

	    	this.getPosition = function() {
        return owner.position;
	    	};

	    	this.getOwner = function() {
        return owner;
	    	};
    };

    Self.prototype = (function() {
      // Calculates the distance to the closest wall
      function wallDistance() {
        /*jshint validthis: true */
        var p = this.getPosition();
        var adj, angle, hyp;

        // If not near to a wall
        if (p.x > this.viewDistance &&
          world.width - p.x > this.viewDistance &&
          p.y > this.viewDistance &&
          world.height - p.y > this.viewDistance) {
          return this.viewDistance;
        }

        // Keep angles within bounds
        p.a = p.a % (Math.PI * 2);
        if (p.a < 0) {
          p.a += Math.PI * 2;
        }

        // cosine(angle) = adjacent / hypotenuse
        // hypotenuse = adjacent / cosine(angle)

        // sine(angle) = opposite / hypotenuse
        // hypotenuse = opposite / sine(angle)

        var distX, distY;
        if (p.a >= 0 && p.a <= Math.PI * 0.5) {
          // Facing right wall, bottom wall: x=world.width, y=world.height
          distX = (world.width - p.x) / Math.cos(p.a);
          distY = (world.height - p.y) / Math.sin(p.a);

          return Math.min(distX, distY, this.viewDistance);
        }

        if (p.a >= Math.PI * 0.5 && p.a <= Math.PI) {
          // Facing left wall, bottom wall: x=0, y=world.height
          distX = (0 - p.x) / Math.cos(p.a);
          distY = (world.height - p.y) / Math.sin(p.a);

          return Math.min(distX, distY, this.viewDistance);
        }

        if (p.a >= Math.PI && p.a <= Math.PI * 1.5) {
          // Facing left wall, top wall: x=0, y=0
          distX = (0 - p.x) / Math.cos(p.a);
          distY = (0 - p.y) / Math.sin(p.a);

          return Math.min(distX, distY, this.viewDistance);
        }

        if (p.a >= Math.PI * 1.5 && p.a < Math.PI * 2) {
          // Facing right wall, top wall: x=world.width, y=0
          distX = (world.width - p.x) / Math.cos(p.a);
          distY = (0 - p.y) / Math.sin(p.a);

          return Math.min(distX, distY, this.viewDistance);
        }
      }

      function sortFoodVectorsCallback(a, b) {
        return a.distance - b.distance;
      }

      function findOrganisms(organisms) {
        /*jshint validthis: true */
        var p = this.getPosition();

        // An array of vectors to foods from this entity's perspective
        var foodVectors = [];

        // Loop through foodSupply
        for (var i = 0; i < organisms.length; i++) {
          var organism = organisms[i];

          if (organism === this.getOwner()) continue;

          // Find polar coordinates of food relative this entity
          var dx = organism.position.x - p.x;
          var dy = organism.position.y - p.y;

          // Check bounding box first for performance
          if (Math.abs(dx) > this.viewDistance || Math.abs(dy) > this.viewDistance) continue;

          // Find angle of food relative to entity
          if (dx === 0) dx = 0.000000000001;
          var angle = p.a - Math.atan2(dy, dx);

          // Convert angles to right of center into negative values
          if (angle > Math.PI) angle -= 2 * Math.PI;

          // Calculate distance to this food
          var distance = Math.sqrt(dx * dx + dy * dy);

          // If the food is outside the viewing range, skip it
          if (Math.abs(angle) > this.fieldOfView / 2 || distance > this.viewDistance) continue;

          foodVectors.push({
            distance: distance,
            angle: angle,
            organism: organism
          });
        }

        // Sort our food vectors by distance
        return foodVectors.sort(sortFoodVectorsCallback);
      }

      return {
        sense: function(plants, animals) {
          return {
            plants: findOrganisms.call(this, plants || []),
            animals: findOrganisms.call(this, animals || []),
            wall: {
              distance: wallDistance.call(this)
            }
          };
        }
      };
    })();

    return Self;
  }
);