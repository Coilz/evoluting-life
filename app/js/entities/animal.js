define([
  'utils/range',
  'costCalculator',
  'utils/cloning',
  'observe/subject',
  'options',
  'genetics/options',
  'entities/options',
  'entities/organism',
  'brains/brain',
  'sensors/eyes'
],
  function(Range, cost, cloning, Subject, worldOptions, geneticsOptions, entitiesOptions, Organism, Brain, Eyes, undefined) {
    "use strict";

    // For convenience and preventing errors due to loading sequence
    function Options() {
      var options = entitiesOptions.animal;

      this.size = options.size.get();

      this.angularFriction = worldOptions.physics.angularFriction.get();
      this.linearFriction = worldOptions.physics.linearFriction.get();
    }

    var initialOutput = {
      turnLeft: 0,    // Left angle
      turnRight: 0,   // Right angle
      accelerate: 0,  // Velocity accelerator
      decelerate: 0,  // Velocity suppressor
      eat: 0,
      mate: 0
    };

    function createInput(visibleFood, visibleAnimal, wall) {
      /*jshint validthis: true */
      var eyes = this.getEyes();
      var fieldOfView = eyes.fieldOfView;
      var viewDistance = eyes.viewDistance;

      var inputs = [
        // left
        visibleAnimal ? (fieldOfView / 2 + visibleAnimal.angle) / fieldOfView : 0,
        // right
        visibleAnimal ? (fieldOfView / 2 - visibleAnimal.angle) / fieldOfView : 0,
        // distance
        visibleAnimal ? (viewDistance - visibleAnimal.distance) / viewDistance : 0,
        // food supply
        visibleAnimal ? visibleAnimal.organism ? visibleAnimal.organism.life.healthN() : 0 : 0,
        // eat signal
        visibleAnimal ? visibleAnimal.organism ? visibleAnimal.organism.willEat() ? 1 : 0 : 0 : 0,
        // mate signal
        visibleAnimal ? visibleAnimal.organism ? visibleAnimal.organism.willMate() ? 1 : 0 : 0 : 0,

        // left
        visibleFood ? (fieldOfView / 2 + visibleFood.angle) / fieldOfView : 0,
        // right
        visibleFood ? (fieldOfView / 2 - visibleFood.angle) / fieldOfView : 0,
        // distance
        visibleFood ? (viewDistance - visibleFood.distance) / viewDistance : 0,
        // food supply
        visibleFood ? visibleFood.organism ? visibleFood.organism.life.healthN() : 0 : 0,

        // negative nutrition
        visibleFood ? visibleFood.organism ? visibleFood.organism.getNutrition() < 0 ? visibleFood.organism.getNutritionN() : 0 : 0 : 0,
        // positive nutrition
        visibleFood ? visibleFood.organism ? visibleFood.organism.getNutrition() > 0 ? visibleFood.organism.getNutritionN() : 0 : 0 : 0,

        // distance to wall
        (viewDistance - wall.distance) / viewDistance,
        // energy
        this.life.healthN(),

        // random
        Math.random()
      ];

      var normalizationFactor = (geneticsOptions.neuron.maxThreshold.get() + geneticsOptions.neuron.minThreshold.get()) / 2;
      // Normalize inputs
      for (var i = 0; i < inputs.length; i++) {
        inputs[i] *= normalizationFactor;
      }

      return inputs;
    }

    var Self = function(genome, position) {
      var options = new Options();

      Organism.call(this, genome, position, options);
      this.linearVelocity = 0;
      this.angularVelocity = 0;

      var eyes = new Eyes(this, genome.getSensor());
      this.getEyes = function() {
        return eyes;
      };

      this.linearForce = genome.getMovement().linearForce;
      this.angularForce = genome.getMovement().angularForce;

      this.brain = new Brain(genome.getBrain());
      this.output = cloning.shallowClone(initialOutput);

      this.life.initialEnergy = genome.getLife().initialEnergy;
      this.life.gainedEnergy = 0;   // Gained energy
      this.life.usedEnergy = 0; // Used energy
      this.life.lostEnergy = 0; // Lost energy

      this.life.netEnergy = function () {
        return this.gainedEnergy - this.usedEnergy - this.lostEnergy;
      };
      this.life.health = function() {
        return this.initialEnergy + this.netEnergy();
      };
      this.life.healthN = function() {
        var health = this.health();
        return health > 0 ? 1 - 1 / Math.exp(health / 200) : 0;
      };
      this.life.score = function() { // TODO: move this to a scoring thing
        return this.netEnergy();
      };

      this.eatNotifier = new Subject();
      this.consumedNotifier = new Subject();
      this.mateNotifier = new Subject();
      this.senseNotifier = new Subject();

      this.getGenome = function() {
        return genome;
      };

      this.getSize = function() {
        return options.size * (1 + 0.75 * this.life.healthN());
      };
    };

    Self.prototype = Object.create(Organism.prototype);

    Self.prototype.consume = function() {
      var health = this.life.health();
      this.life.lostEnergy++;

      var nutrition = 1.0;
      var consumed = health < 0 ? 0 : health < nutrition ? health : nutrition;
      this.consumedNotifier.notify(consumed); // static value until nutricion becomes available

      return consumed;
    };

    Self.prototype.willEat = function () {
      return this.output.eat;
    };

    Self.prototype.willMate = function () {
      return this.life.age > this.life.matureAge * this.life.oldAge && this.output.mate;
    };

    Self.prototype.mate = function() {
      if (!this.willMate()) return false;

      this.life.usedEnergy += cost.mate(this.life.initialEnergy);

      return true;
    };

    Self.prototype.produceChildren = function (partner, childPositions) {
        var parentGenomeA = this.getGenome();
        var parentGenomeB = partner.getGenome();
        var children = [];

        var childrenGenomes = parentGenomeA.mate(parentGenomeB);
        for (var i=0; i<2; i++) {
            var childGenome = childrenGenomes[i];
            childGenome.mutate();

            // Spawn a new entity from it
            var position = childPositions[i];
            var newAnimal = new Self( childGenome, position );
            newAnimal.population = this;
            children.push(newAnimal);
        }

        return children;
    };

    // Process Animal lifecycle
    Self.prototype.run = (function() {
      var range = new Range(-1, 1);

      function createNearPosition() {
        return {
              // Starting position and angle
              x: this.position.x + this.getSize() * range.random(),
              y: this.position.y + this.getSize() * range.random(),
              a: Math.random() * Math.PI * 2
          };
      }

      function matePartner(partner) {
        /*jshint validthis: true */
        if (partner === undefined) return;

        // Use formula for a circle to find food
        var x2 = (this.position.x - partner.position.x); x2 *= x2;
        var y2 = (this.position.y - partner.position.y); y2 *= y2;
        var s2 = partner.getSize() + 2; s2 *= s2;

        // If we are within the circle, mate it
        if (x2 + y2 >= s2) {
          return;
        }

        // Can only reproduce if the partner mates
        if (!partner.mate()) return;
        this.mate();

        // Can only reproduce when there is enough energy
        // if ( this.life.health() < 0  ) return;

        // Produce children
        var positions = [createNearPosition.call(this), createNearPosition.call(this)];

        var children = this.produceChildren.call(this, partner, positions);
        for (var i=0; i<children.length; i++) {
            this.population.addAnimal(children[i]);
        }

        this.mateNotifier.notify(children.length);
      }

      function eatOrganism(organism) {
        /*jshint validthis: true */
        if (organism === undefined) return;

        // Use formula for a circle to find food
        var x2 = (this.position.x - organism.position.x); x2 *= x2;
        var y2 = (this.position.y - organism.position.y); y2 *= y2;
        var s2 = organism.getSize() + 2; s2 *= s2;

        // If we are within the circle, eat it
        if (x2 + y2 >= s2) {
          return;
        }

        // Increase entities total eaten counter
        var consumed = organism.consume();
        this.life.gainedEnergy += consumed;

        // Increment global eaten counter
        this.eatNotifier.notify(consumed);
      }

      var setCurrentTargets = function (targets) {
        this.targets = [];

        if (targets.plants.length > 0) this.targets.push(targets.plants[0].organism);
        if (targets.animals.length > 0) this.targets.push(targets.animals[0].organism);
      };

      var think = function(targets) {
        var inputs = createInput.call(this, targets.plants[0], targets.animals[0], targets.wall);
        var thoughtOutput = this.brain.think(inputs);
        for (var i in thoughtOutput) {
          this.output[['turnLeft', 'turnRight', 'accelerate', 'decelerate', 'eat', 'mate'][i]] = thoughtOutput[i];
        }
      };

      var mate = function(targets) {
        if (!this.willMate()) return;

        if (targets.animals.length > 0) matePartner.call(this, targets.animals[0].organism);
      };

      var eat = function(targets) {
        if (!this.output.eat) return;

        if (targets.plants.length > 0) eatOrganism.call(this, targets.plants[0].organism);
        if (targets.animals.length > 0) eatOrganism.call(this, targets.animals[0].organism);
      };

      var move = function() {
        var options = new Options();

        var p = this.position;

        var angularAcceleration = (this.output.turnLeft - this.output.turnRight) * this.angularForce;
        this.angularVelocity += angularAcceleration;
        this.angularVelocity -= this.angularVelocity * options.angularFriction;
        p.a += this.angularVelocity;

        // Keep angles within bounds
        p.a = p.a % (Math.PI * 2);
        if (p.a < 0) p.a += Math.PI * 2;

        // F=m*a => a=F/m, dv=a*dt => dv=dt*F/m, dt=one cycle, m=1
        var linearAcceleration = (this.output.accelerate - this.output.decelerate) * this.linearForce;
        this.linearVelocity += linearAcceleration;
        this.linearVelocity -= this.linearVelocity * options.linearFriction;

        // Convert movement vector into polar
        var dx = (Math.cos(p.a) * this.linearVelocity);
        var dy = (Math.sin(p.a) * this.linearVelocity);

        // Move the entity
        p.x += dx;
        p.y += dy;

        // Register the cost of the forces applied for acceleration
        this.life.usedEnergy += cost.rotate(angularAcceleration * this.life.health());
        this.life.usedEnergy += cost.accelerate(linearAcceleration * this.life.health());
      };

      return function(plants, animals) {
        this.life.age++;

        var targets = this.getEyes().sense(plants, animals);
        this.senseNotifier.notify(targets);

        setCurrentTargets.call(this, targets);
        think.call(this, targets);
        mate.call(this, targets);
        eat.call(this, targets);
        move.call(this);

        // Register the cost of the cycle
        this.life.lostEnergy += cost.cycle();
      };
    })();

    return Self;
  }
);