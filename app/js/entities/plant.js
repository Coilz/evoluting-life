define([
    'utils/range',
    'genetics/options',
    'entities/options',
    'entities/organism'
    ],
    function (Range, geneticsOptions, entitiesOptions, Organism, undefined) {
        "use strict";

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = entitiesOptions.plant;

            this.minFoodSize = options.minFoodSize.get();
            this.maxFoodSize = options.maxFoodSize.get();
            this.minGrowthPercentage = options.minGrowthPercentage.get();
            this.maxGrowthPercentage = options.maxGrowthPercentage.get();

            this.minNutrition = geneticsOptions.life.minNutrition.get();
            this.maxNutrition = geneticsOptions.life.maxNutrition.get();
        }

        var Self = function (genome, position) {
            var options = new Options();
            Organism.call(this, genome, position, options);

            var growthPercentage = new Range(options.minGrowthPercentage, options.maxGrowthPercentage).random();
            var size = new Range(options.minFoodSize, options.maxFoodSize).random();
            this.getSize = function() {
                return size;
            };

            var nutrition = genome.getLife().nutrition;
            this.getNutrition = function() {
                return nutrition; // Is the result of consume.
            };
            this.getNutritionN = function() {
                if (nutrition < 0) {
                    return nutrition / options.minNutrition;
                }
                else {
                    return nutrition / options.maxNutrition;
                }
            };

            this.consume = function() {
                size--;
                return nutrition; // Energy can vary per type of food in the future
            };

            this.run = function() {
                this.life.age++;
                size += size * growthPercentage / 100;
                if (size > options.maxFoodSize) {
                    size = options.maxFoodSize;
                }
            };

            this.life.health = function() {
                return size;
            };
            this.life.healthN = function() {
                return size / options.maxFoodSize;
            };
        };

        Self.prototype = Object.create(Organism.prototype);

        return Self;
    }
);