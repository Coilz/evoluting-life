define([
    'option'
    ],
    function (Option, undefined) {
        "use strict";

        var options = {
            plant: {
                minFoodSize: new Option(2),
                maxFoodSize: new Option(8),
                minGrowthPercentage: new Option(0.01),
                maxGrowthPercentage: new Option(0.05),
                populationSize: new Option(10*8),

                reset: function() {
                    this.minFoodSize.reset();
                    this.maxFoodSize.reset();
                    this.minGrowthPercentage.reset();
                    this.maxGrowthPercentage.reset();
                    this.populationSize.reset();
                }
            },

            animal: {
                size: new Option(12),
                populationSize: new Option(16*8),

                reset: function() {
                    this.size.reset();
                    this.populationSize.reset();
                }
            },

            reset: function() {
                this.plant.reset();
                this.animal.reset();
            }
        };

        return options;
    }
);
