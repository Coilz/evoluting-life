define([
  'observe/subject',
  'worldCanvas'
],
  function(Subject, worldCanvas, undefined) {
    "use strict";

    var world = worldCanvas;

    var Self = function(genome, position, options) {
      this.position = position;

      this.wanderedNotifier = new Subject();
      this.starvedNotifier = new Subject();
      this.diedOfAgeNotifier = new Subject();

      this.life = {
        age: 0,
        matureAge: genome.getLife().matureAge,
        oldAge: genome.getLife().oldAge,
        ageN: function() { return this.age / this.oldAge; }
      };
    };

    Self.prototype = {
      lives: function() {
        var p = this.position;

        if (p.x > world.width || p.x < 0 || p.y > world.height || p.y < 0) {
          this.wanderedNotifier.notify(1);
          return false;
        }

        // Kill entities if it's exceeded starvation threshold
        if (this.life.health() <= 0) {
          this.starvedNotifier.notify(1);
          return false;
        }

        // Randomly kill entities who've entered old age
        if (this.life.age > this.life.oldAge) {
          // Vulnerable entities have 1/100 chance of death
          if (Math.random() * 100 <= 1) {
            this.diedOfAgeNotifier.notify(1);
            return false;
          }
        }

        return true;
      }
    };

    return Self;
  }
);