define([
    'worldCanvas',
    'observe/observer'
    ],
    function (worldCanvas, Observer, undefined) {
        "use strict";

        var Self = function () {
            var that = this;
            this.entityRunObserver = new Observer(function (context) {
                that.draw(context);
            });
        };

        Self.prototype = {
            // Draw an entity on the canvas
            draw: function (plant) {
                var context = worldCanvas.getContext('2d');
                context.beginPath();
                context.lineWidth = 2;
                // context.lineWidth = 2 + Math.floor(plant.life.age / (plant.life.oldAge / 3));

                var r = Math.floor( 255 * (1 - plant.getNutritionN()) );
                var color = "rgb(" + r + ",220,0)"; //187

                if (plant.getNutrition() < 0) {
                    context.strokeStyle = color;
                    context.fillStyle = "#000";
                }
                else {
                    context.strokeStyle = "#000";
                    context.fillStyle = color;
                }

                context.arc( plant.position.x, plant.position.y, plant.getSize(), 0, Math.PI*2, true );

                context.stroke();
                context.fill();
                context.closePath();
            }
        };

        return Self;
    }
);