define([
  'observe/observer',
  'genetics/options'
],
  function (Observer, geneticsOptions, undefined) {
    "use strict";

    var CAMERA_DISTANCE_FACTOR = 0.5;

    /**
     * Calculates the factor for distance. Returns 1 when distance is 0, and returns 0 when distance is maximal.
     * @return {Number} factor
     */
    function calculateDistanceFactor(target, sensor) {
      return 1 - target.distance / sensor.viewDistance;
    }

    // Calculate perspective x
    function calculatePerspectiveSizeX(element, target, sensor) {
      // element.width = x * sensor.fieldOfView * target.distance;
      // x = element.width / (sensor.fieldOfView * target.distance);
      // sizeX = target.size * x;
      return target.organism.getSize() * element.width / (sensor.fieldOfView * target.distance);
    }

    // Calculate perspective y
    function calculatePerspectiveSizeY(element, target, sensor) {
      return (1 + CAMERA_DISTANCE_FACTOR) * CAMERA_DISTANCE_FACTOR * element.height * sensor.viewDistance * target.organism.getSize() /
      ((CAMERA_DISTANCE_FACTOR * sensor.viewDistance + target.distance) * (CAMERA_DISTANCE_FACTOR * sensor.viewDistance + target.distance + target.organism.getSize()));
    }

    function getDimensions(element, target, sensor) {
      return {
        x: calculatePerspectiveSizeX(element, target, sensor),
        y: calculatePerspectiveSizeY(element, target, sensor)
      };
    }

    // Calculate the x coordinate
    function calculateX(element, target, sensor) {
      return element.width / 2 - element.width * target.angle / sensor.fieldOfView;
    }

    // Calculate the y coordinate
    function calculateY(element, target, sensor) {
      return element.height - (1 + CAMERA_DISTANCE_FACTOR) * element.height * target.distance /
      (CAMERA_DISTANCE_FACTOR * sensor.viewDistance + target.distance);
    }

    function getPosition(element, reference, target, sensor) {
      return {
        x: calculateX(element, target, sensor),
        y: calculateY(element, target, sensor),
        a: target.organism.position.a - reference.position.a - Math.PI / 2 + target.angle
      };
    }

    // Get the sensor of an animal
    function getSensor(animal) {
      var eyes = animal ? animal.getEyes() : undefined;

      return {
        viewDistance: eyes ? eyes.viewDistance : 0,
        fieldOfView: eyes ? eyes.fieldOfView : 0
      }
    }

    // Calculate the y-opacity
    function getOpacity(sensor, target) {
      return 1.0;//(sensor.viewDistance - target.distance) / sensor.viewDistance;
    }

    // Get the absolute value of the score
    function getAbsoluteScore(target) {
      return target.life.score() < 0 ? 0 : target.life.score();
    }

    var Self = function (element) {
      // Get canvas and context for diagram
      var context = element.getContext('2d');
      context.font = "12px 'Trebuchet MS'";

      var currentWinner;

      // Draw the reticle (crosshair)
      function drawReticle() {
        context.lineWidth = 0.2;
        context.strokeStyle = "#000";

        var x = element.width / 2;

        context.beginPath();
        context.moveTo(x, 0);
        context.lineTo(x, element.height);
        context.stroke();
        context.closePath();
      }

      // Draw a line around the object
      function addOutLine() {
        context.strokeStyle = "#000";
        context.lineWidth = 2;
        context.stroke();
      }

      // Draw a line to a target
      function addTargetLine(x, y) {
        context.lineWidth = 1;
        context.strokeStyle = "#fff";

        context.moveTo(x, y);
        context.lineTo(x, element.height);
        context.stroke();
      }

      function drawTriangle(position, dimensions) {
        var ba = position.a + Math.PI; // Find the angle 180deg of entity

        // Find left back triangle point
        var WEDGE_ANGLE = Math.PI * 0.25;
        var lx = Math.cos(ba + (WEDGE_ANGLE / 2)) * dimensions.x/2;
        var ly = Math.sin(ba + (WEDGE_ANGLE / 2)) * dimensions.y/2;

        // Find right back triangle point
        var rx = Math.cos(ba - (WEDGE_ANGLE / 2)) * dimensions.x/2;
        var ry = Math.sin(ba - (WEDGE_ANGLE / 2)) * dimensions.y/2;

        // Find the curve control point
        var cx = Math.cos(ba) * dimensions.x/2 * 0.3;
        var cy = Math.sin(ba) * dimensions.y/2 * 0.3;

        // Draw the triangle
        context.moveTo(position.x, position.y);
        context.lineTo(position.x + lx, position.y + ly);
        context.quadraticCurveTo(position.x + cx, position.y + cy, position.x + rx, position.y + ry);
      }

      // Draw an elipse
      function drawBezierCurve(position, dimensions) {
        context.moveTo(position.x, position.y - dimensions.y / 2); // A1

        context.bezierCurveTo(
          position.x + dimensions.x / 2, position.y - dimensions.y / 2, // C1
          position.x + dimensions.x / 2, position.y + dimensions.y / 2, // C2
          position.x, position.y + dimensions.y / 2); // A2

        context.bezierCurveTo(
          position.x - dimensions.x / 2, position.y + dimensions.y / 2, // C3
          position.x - dimensions.x / 2, position.y - dimensions.y / 2, // C4
          position.x, position.y - dimensions.y / 2); // A1
      }

      // Draw an elipse
      function drawEllipse(position, dimensions) {
        context.ellipse(position.x, position.y, dimensions.x, dimensions.y, 0, 0, 2 * Math.PI, false);
      }

      // Draw a rectangle
      function drawRectangle(position, dimensions) {
        context.rect(position.x, position.y, dimensions.x, dimensions.y);
      }

      // Draw a shape
      function drawShape(position, dimensions, fillStyle, isTarget, shapeDraw) {
        context.beginPath();

        shapeDraw(position, dimensions);

        if (isTarget) {
          addOutLine();
          //addTargetLine(position);
        }

        context.fillStyle = fillStyle;
        context.fill();

        context.closePath();
      }

      // Draw a representation of the wall
      function drawWall(target) {
      }

      // Draw a representation of an animal
      function drawAnimal(target, isTarget) {
        var animal = target.organism;
        var sensor = getSensor(currentWinner);

        var currentBestScore = getAbsoluteScore(currentWinner);
        var g = Math.floor(255 * (1 - (currentBestScore === 0 ? 0 : getAbsoluteScore(animal) / currentBestScore)));
        var a = getOpacity(sensor, target);
        var fillStyle = "rgba(255," + g + ",0," + a + ")";

        var position = getPosition(element, currentWinner, target, sensor);
        var dimensions = getDimensions(element, target, sensor);

        drawShape(position, dimensions, fillStyle, isTarget, drawTriangle);
      }

      // Draw a representation of a plant
      function drawPlant(target, isTarget) {
        var plant = target.organism;
        var sensor = getSensor(currentWinner);

        var r = Math.floor(255 * (1 - plant.getNutritionN()));
        var a = getOpacity(sensor, target);
        var fillStyle = "rgba(" + r + ",220,0," + a + ")";

        var position = getPosition(element, currentWinner, target, sensor);
        var dimensions = getDimensions(element, target, sensor);

        drawShape(position, dimensions, fillStyle, isTarget, drawBezierCurve);
      }

      // Draw representations of all targets
      function draw(targets) {
        var i = targets.animals.length;
        while (i--) {
          var target = targets.animals[i];
          drawAnimal(target, i === 0);
        }

        i = targets.plants.length;
        while (i--) {
          var target = targets.plants[i];
          drawPlant(target, i === 0);
        }
      }

      // Observe sensed targets for the winning entity
      var sensedObserver = new Observer(function (targets) {
        // Clear the drawing area
        context.clearRect(0, 0, element.width, element.height);

        draw(targets);
        drawReticle();
      });

      // Observer for a new winning entity
      this.newWinnerObserver = new Observer(function (winner) {
        if (currentWinner) {
          currentWinner.senseNotifier.removeObserver(sensedObserver);
        }

        context.fillStyle = 'rgba(0,0,0,0.1)';
        context.fillRect(0, 0, element.width, element.height);

        currentWinner = winner;
        currentWinner.senseNotifier.addObserver(sensedObserver);
      });
    };

    return Self;
  }
);