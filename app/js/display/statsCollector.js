define([
    'utils/cloning',
    'observe/observer'
    ],
    function (cloning, Observer, undefined) {
        "use strict";

        function getInitialCounters() {
            return {
                starved: 0,
                wandered: 0,
                eaten: 0,
                consumed: 0,
                mated: 0,
                natural: 0
            };
        }

        function calculatePopulationAverages(populationTotals) {
            return populationTotals.size ?
            {
                ageAverage: populationTotals.ageTotal / populationTotals.size,
                gainedEnergyAverage: populationTotals.gainedEnergyTotal / populationTotals.size,
                usedEnergyAverage: populationTotals.usedEnergyTotal / populationTotals.size,
                lostEnergyAverage: populationTotals.lostEnergyTotal / populationTotals.size,
                netEnergyAverage: populationTotals.netEnergyTotal / populationTotals.size,
                healthAverage: populationTotals.healthTotal / populationTotals.size
            } :
            {
                ageAverage: 0,
                gainedEnergyAverage: 0,
                usedEnergyAverage: 0,
                lostEnergyAverage: 0,
                netEnergyAverage: 0,
                healthAverage: 0
            };
        }

        function subtractCounters(a, b) {
            var result = {};

            for (var i in a) {
                result[i] = a[i] - b[i];
            }

            return result;
        }

        var Self = function (getPopulationTotals) {
            var counters = getInitialCounters();

            this.getCounters = function () {
                var currentCounters = counters;
                counters = getInitialCounters();

                var populationTotals = getPopulationTotals();
                var populationNumbers = calculatePopulationAverages(populationTotals);
                populationNumbers.fittest = populationTotals.fittest;
                currentCounters.plantDiet = currentCounters.eaten === 0 ? 0 : 100 * (currentCounters.eaten - currentCounters.consumed) / currentCounters.eaten;

                return cloning.shallowMixin(populationNumbers, currentCounters);
            };

            this.starvedObserver = new Observer(function (context) {
                counters.starved += context;
            });

            this.wanderedObserver = new Observer(function (context) {
                counters.wandered += context;
            });

            this.eatObserver = new Observer(function (context) {
                counters.eaten += context;
            });

            this.consumedObserver = new Observer(function (context) {
                counters.consumed += context;
            });

            this.mateObserver = new Observer(function (context) {
                counters.mated++;// += context;
            });

            this.diedOfAgeObserver = new Observer(function (context) {
                counters.natural += context;
            });
        };

        return Self;
    }
);