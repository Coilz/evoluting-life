define([
    'observe/observer',
    'genetics/options'
],
    function (Observer, geneticsOptions, undefined) {
        "use strict";

        // Drawing parameters
        var BORDER = 20; // Border around diagram
        var SPREAD = 32; // Width of connection spread
        var NODE_RADIUS = 18;
        var maxStrength = geneticsOptions.axon.maxStrength.get();

        // Calculate the distance between nodes for x and y
        function calculateNodeDistances(element, brainDimensions) {
            // Find drawing area ( minus the borders )
            var drawAreaWidth = element.width - (BORDER * 2);
            var drawAreaHeight = element.height - (BORDER * 2);

            // Find the distance between nodes
            return {
                x: (drawAreaWidth - (NODE_RADIUS * 2)) / (brainDimensions.width - 1),
                y: (drawAreaHeight - (NODE_RADIUS * 2)) / (brainDimensions.height - 1)
            };
        };

        function calculateNodePosition(nodeDistances, nodeX, nodeY) {
            return {
                x: Math.floor(BORDER + NODE_RADIUS + (nodeX * nodeDistances.x)),
                y: Math.floor(BORDER + NODE_RADIUS + (nodeY * nodeDistances.y))
            };
        };

        // Calculate the number of nodes of the brain for the width and the height
        function calculateBrainDimensions (brainLayers) {
            var height = brainLayers.length;
            var width = 0;

            for (var i = 0; i < height; i++) {
                var neuronCount = brainLayers[i].getNeurons().length;
                if (neuronCount > width) {
                    width = neuronCount;
                }
            }

            return {
                height: height,
                width: width
            }
        };

        var Self = function (element) {
            // Get canvas and context for diagram
            var context = element.getContext('2d');
            context.font="12px 'Trebuchet MS'";

            function drawNode(neuron, nodePosition) {
                // Draw node with white outer border
                context.strokeStyle = "#ccc";
                context.lineWidth = 3;
                context.fillStyle = "#222";

                context.beginPath();
                context.arc(nodePosition.x, nodePosition.y, NODE_RADIUS, 0, Math.PI * 2, true);
                context.stroke();
                context.fill();
                context.closePath();

                // Align text in node circle
                context.textAlign = "center";
                context.textBaseline = "middle";

                // White text
                context.fillStyle = "#ccc";

                // Draw threshold and relaxation rate
                context.fillText(neuron.getThreshold().toFixed(1), nodePosition.x, nodePosition.y - 6);
                context.fillText(((1 - neuron.getRelaxation()) * 100).toFixed(0) + "%", nodePosition.x, nodePosition.y + 6);
            };

            function drawAxons(axons, nodeY, nodeDistances, nodePosition) {
                // Loop through axons
                for (var k in axons) {
                    var axon = axons[k];

                    if (axon.getStrength() === 0) continue;

                    // Calculate coordinates of axon targets
                    var target = calculateNodePosition(nodeDistances, k, nodeY+1);

                    // Size line width relative to axon strength
                    context.lineWidth = 2 * Math.abs(axon.getStrength()) / maxStrength;
                    // Color coding ( green = excitory / red = inhibitory )
                    if (axon.getStrength() > 0) context.strokeStyle = "#090";
                    else context.strokeStyle = "#900";

                    // Draw the axon
                    context.beginPath();
                    context.moveTo(nodePosition.x, nodePosition.y);
                    context.lineTo(target.x, target.y);
                    context.stroke();
                    context.closePath();
                }
            };

            function draw (brainLayers) {
                var brainDimensions = calculateBrainDimensions(brainLayers);
                var nodeDistances = calculateNodeDistances(element, brainDimensions);

                // Loop through layers
                for (var i = 0; i < brainDimensions.height; i++) {
                    var nodeY = brainDimensions.height - 1 - i;
                    var layer = brainLayers[i];
                    var neurons = layer.getNeurons();

                    // Loop through neurons
                    for (var j = 0; j < neurons.length; j++) {
                        var neuron = neurons[j];

                        // Find coordinates of node circle
                        var nodePosition = calculateNodePosition(nodeDistances, j, nodeY);

                        // Draw axon connections if not last layer
                        if ( i > 0 ) {
                            var axons = neuron.getAxons();
                            drawAxons(axons, nodeY, nodeDistances, nodePosition);
                        }

                        drawNode(neuron, nodePosition);
                    }
                }
            };

            // Observer for a new winning animal
            this.newWinnerObserver = new Observer(function (winner) {
                // Clear the drawing area
                context.clearRect(0, 0, element.width, element.height);

                var brainLayers = winner.brain.getLayers();
                draw(brainLayers);
            });
        };

        return Self;
    }
);