define([
    'utils/cloning',
    'observe/observer'
],
    function (cloning, Observer, undefined) {
        "use strict";

        var Self = function (getIteration, getPopulationTotals) {
            var startTime = Date.now();
            var oldRunningTime = 0;
            var oldIteration = 0;

            var counters = {
                populationSize: 0,
                born: 0,
                mutated: 0,
                iteration: 0,
                runningTime: 0,
                fps: 0,
                highScore: 0
            };

            this.getCounters = function () {
                counters.iteration = getIteration();

                var populationTotals = getPopulationTotals();

                if (populationTotals.fittest > counters.highScore) {
                    counters.highScore = populationTotals.fittest;
                }

                counters.populationSize = populationTotals.size;

                var newRunningTime = Math.floor((Date.now() - startTime) / 1000);
                if (newRunningTime != oldRunningTime){
                    counters.runningTime = newRunningTime;
                    counters.fps = counters.iteration - oldIteration;
                    oldRunningTime = counters.runningTime;
                    oldIteration = counters.iteration;
                }

                return cloning.shallowMixin(populationTotals, counters);
            };

            this.bornObserver = new Observer(function (context) {
                counters.born++;
            });

            this.mutatedObserver = new Observer(function (context) {
                counters.mutated += context;
            });
        };

        return Self;
    }
);