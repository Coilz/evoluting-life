define([],
    function (undefined) {
        "use strict";

        // Drawing parameters
        var BORDER = 20;
        var PERIOD = 50;

        var Self = function (element, getPopulationTotals) {
            var context = element.getContext('2d');
            var historyLength = element.width - (BORDER * 2);

            function getCurrentValues() {
                var populationTotals = getPopulationTotals();

                return {
                    netEnergyAverage: populationTotals.size === 0 ? 0 : populationTotals.netEnergyTotal / populationTotals.size,
                    ageAverage: populationTotals.size === 0 ? 0 : populationTotals.ageTotal / populationTotals.size
                };
            }

            function recordBoudaryValue (currentValue, boundaries) {
                if (currentValue > boundaries.high) boundaries.high = currentValue;
                if (currentValue < boundaries.low) boundaries.low = currentValue;
            };

            this.historyList = [];
            this.boundaryValues = {
                netEnergyAverage: {
                    low: 0,
                    high: 0
                },
                ageAverage: {
                    low: 0,
                    high: 0
                },
            };

            this.record = function () {
                var currentValues = getCurrentValues();
                this.historyList.push(currentValues);

                // Trim historyList
                if (this.historyList.length > historyLength) this.historyList.shift();

                return currentValues;
            };

            this.recordBoudaryValues = function (currentValues) {
                recordBoudaryValue(currentValues.ageAverage, this.boundaryValues.ageAverage);
                recordBoudaryValue(currentValues.netEnergyAverage, this.boundaryValues.netEnergyAverage);
            };

            this.clear = function () {
                context.clearRect(0, 0, element.width, element.height);
            }

            this.draw = function (historyValue, currentValue, boundaries, color) {
                var lowScore = boundaries.low;
                var highScore = boundaries.high;

                // Find drawing area dimensions
                var drawAreaWidth = element.width - (BORDER * 2);
                var drawAreaHeight = element.height - (BORDER * 2);

                // Find distance between plots and vertical scaling facter
                var distanceX = this.historyList.length === 0 ? 1 : drawAreaWidth / (this.historyList.length - 1);
                var yRange = (highScore - lowScore);
                var yScale = yRange === 0 ? 0 : drawAreaHeight / yRange;

                // Find starting point of graph
                var ox = 0;
                var oy = 0;

                // Draw 1 pixel wide white line
                context.strokeStyle = color;
                context.lineWidth = 1;

                // Draw the graph line
                context.beginPath();
                var newLowScore = highScore;
                var newHighScore = lowScore;
                for (var i = 0; i < this.historyList.length; i++) {
                    var value = historyValue(i);

                    // Find new x,y
                    var x = BORDER + (i * distanceX);
                    var y = element.height - (BORDER + ((value - lowScore) * yScale));

                    // Find new boundaries
                    if (value < newLowScore) newLowScore = value;
                    if (value > newHighScore) newHighScore = value;

                    // Break out on first loop through, we only want to find starting point
                    if (i === 0) {
                        ox = x;
                        oy = y;
                        continue;
                    }

                    // Add line segment
                    context.moveTo(ox, oy);
                    context.lineTo(x, y);

                    ox = x;
                    oy = y;
                }

                context.stroke();
                context.closePath();

                boundaries.low = newLowScore;
                boundaries.high = newHighScore;
            };
        };

        Self.prototype = {
            periodicalReport: function () {
                var that = this;
                var periodCounter = 0;

                var drawNetEnergyAverage = function (currentValue) {
                    that.draw(function (i) {
                        return that.historyList[i].netEnergyAverage
                    }, currentValue.netEnergyAverage, that.boundaryValues.netEnergyAverage, "#fff");
                };

                var drawAgeAverage = function (currentValue) {
                    that.draw(function (i) {
                        return that.historyList[i].ageAverage
                    }, currentValue.ageAverage, that.boundaryValues.ageAverage, "#0ff");
                };

                return function () {
                    if (periodCounter++ < PERIOD) return;
                    periodCounter = 0;

                    var currentValue = that.record();
                    that.recordBoudaryValues(currentValue);

                    // Clear the graph canvas
                    that.clear();

                    // Draw the new graphs
                    drawNetEnergyAverage(currentValue);
                    drawAgeAverage(currentValue);
                };
            }
        };

        return Self;
    }
);