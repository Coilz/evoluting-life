define([],
    function (undefined) {
        "use strict";

        function leadZero( v ) {
            return ( v < 10 ? "0" : "" ) + v;
        }

        function print(element, counters) {
            var h = Math.floor( counters.runningTime / 3600 );
            var m = Math.floor( ( counters.runningTime % 3600 ) / 60 );
            var s = counters.runningTime % 60;

            s = leadZero( s );
            m = leadZero( m );

            element.innerHTML = h + ":" + m + ":" + s + " / " + counters.fps + "<br/>"
             + counters.populationSize + " / " + counters.born + " / " + counters.iteration + "<br/>"
             + counters.fittest.toFixed(2) + " / " + counters.highScore.toFixed(2);
        }

        var Self = function (element, countersCollector) {

            this.drawCounters = function() {
                var counters = countersCollector.getCounters();
                print(element, counters);
            };
        };

        return Self;
    }
);