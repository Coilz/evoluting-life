define([],
    function () {
        "use strict";

        var PERIOD = 300; // each x cycles
        var maxNumberOfRows = 50; // keep 50 lines of history;

        function tableRow( items ) {
            var row = document.createElement('tr');
            for (var i in items) {
                var cell = document.createElement('td');
                cell.innerHTML = items[ i ];
                row.appendChild( cell );
            }
            return row;
        }

        var Self = function (statsTable, statsCollector) {
            this.Table = statsTable;
            this.statsCollector = statsCollector;
        };

        Self.prototype = {
            report: function () {
                var statsTable = this.Table;
                var counters = this.statsCollector.getCounters();

                // Add our new table row
                statsTable.insertBefore(
                    tableRow([
                        Math.floor( PERIOD ), // Cycles
                        counters.ageAverage.toFixed(2), // Average age
                        counters.gainedEnergyAverage.toFixed(2), // Average eaten
                        counters.usedEnergyAverage.toFixed(2), // Average used energy
                        counters.lostEnergyAverage.toFixed(2), // Average lost energy
                        counters.netEnergyAverage.toFixed(2), // Average energy balance
                        counters.healthAverage.toFixed(2), // Average health
                        counters.starved, // Starved
                        counters.wandered, // Wandered
                        counters.natural, // Died of age
                        counters.eaten.toFixed(2), // Total eaten
                        counters.plantDiet.toFixed(2), // Total consumed
                        counters.mated, // Total born from mating
                        counters.fittest.toFixed(3) // Current best
                    ]),
                    statsTable.firstChild );

                if (statsTable.rows.length > maxNumberOfRows) {
                    statsTable.deleteRow(statsTable.rows.length - 1);
                }
            },

            periodicalReport: function () {
                var that = this;
                var periodCounter = 0;

                return function () {
                    if (periodCounter++ < PERIOD) return;
                    periodCounter = 0;

                    that.report();
                };
            }
        };

        return Self;
    }
);