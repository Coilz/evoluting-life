define([
    'worldCanvas',
    'observe/observer'
    ],
    function (worldCanvas, Observer, undefined) {
        "use strict";

        var canvas = worldCanvas;
        var context = canvas.getContext('2d');

        function drawTargetLines (animal) {
            var p = animal.position;

            for (var i=0; i<animal.targets.length; i++) {
                var t = animal.targets[i].position;

                context.beginPath();
                context.strokeStyle = "rgba(255, 255, 255, 0.3)";
                context.lineWidth = 1;
                context.moveTo(p.x, p.y);
                context.lineTo(t.x, t.y);
                context.stroke();
            }
        }

        function drawHalo (animal) {
            var haloSize = animal.getSize();
            var p = animal.position;
            var ba = p.a + Math.PI; // Find the angle 180deg of entity

            var hX = p.x + ( Math.cos( ba ) * ( haloSize / 2 ) );
            var hY = p.y + ( Math.sin( ba ) * ( haloSize / 2 ) );
            var highlight = context.createRadialGradient( hX, hY, 0, hX, hY, haloSize );
            highlight.addColorStop( 0, "rgba( 255, 255, 255, 0.4 )" );
            highlight.addColorStop( 1, "rgba( 255, 255,  255, 0.0 )" );

            context.fillStyle = highlight;
            context.beginPath();
            context.arc( hX , hY, haloSize, 0, Math.PI*2, true );
            context.closePath();
            context.fill();
        }

        function drawMateHalo (animal) {
            var haloSize = animal.getSize() * 0.3;
            var p = animal.position;
            var ba = p.a + Math.PI; // Find the angle 180deg of entity

            var hX = p.x + ( Math.cos( ba ) * -1 * haloSize );
            var hY = p.y + ( Math.sin( ba ) * -1 * haloSize );
            var highlight = context.createRadialGradient( hX, hY, 0, hX, hY, haloSize );
            highlight.addColorStop( 0, "rgba( 0, 100, 255, 1.0 )" );
            highlight.addColorStop( 1, "rgba( 0, 100,  255, 0.0 )" );

            context.fillStyle = highlight;
            context.beginPath();
            context.arc( hX , hY, haloSize, 0, Math.PI*2, true );
            context.closePath();
            context.fill();
        }

        function drawEatHalo (animal) {
            var haloSize = animal.getSize() * 0.4;
            var p = animal.position;
            var ba = p.a + Math.PI; // Find the angle 180deg of entity

            var hX = p.x + ( Math.cos( ba ) );
            var hY = p.y + ( Math.sin( ba ) );
            var highlight = context.createRadialGradient( hX, hY, 0, hX, hY, haloSize );
            highlight.addColorStop( 0, "rgba( 255, 255, 255, 1.0 )" );
            highlight.addColorStop( 1, "rgba( 255, 255, 255, 0.0 )" );

            context.fillStyle = highlight;
            context.beginPath();
            context.arc( hX , hY, haloSize, 0, Math.PI*2, true );
            context.closePath();
            context.fill();
        }

        function drawFieldOfView (animal) {
            var p = animal.position;
            var e = animal.getEyes();

            context.beginPath();
            context.fillStyle = "rgba(255, 255, 255, 0.03)";

            context.moveTo(p.x, p.y);
            context.arc( p.x, p.y, e.viewDistance, p.a + e.fieldOfView/2, p.a - e.fieldOfView/2, true );

            context.fill();
            context.closePath();
        }

        function drawSize (animal) {
            var entitySize = animal.getSize();
            var p = animal.position;

            context.beginPath();
            context.lineWidth = 0.5;
            context.strokeStyle = "rgba(0, 0, 0, 0.3)";
            // context.fillStyle = "rgba(255, 255, 255, 0.1)";
            context.arc( p.x, p.y, entitySize, 0, Math.PI*2, true );
            context.stroke();
            // context.fill();
            context.closePath();
        }

        function drawAnimal (animal, currentBestAnimal) {
            var entitySize = animal.getSize();
            var p = animal.position;
            var ba = p.a + Math.PI; // Find the angle 180deg of entity

            // Find left back triangle point
            var WEDGE_ANGLE = Math.PI * 0.25;
            var lx = Math.cos( ba + ( WEDGE_ANGLE / 2 ) ) * entitySize;
            var ly = Math.sin( ba + ( WEDGE_ANGLE / 2 ) ) * entitySize;

            // Find right back triangle point
            var rx = Math.cos( ba - ( WEDGE_ANGLE / 2 ) ) * entitySize;
            var ry = Math.sin( ba - ( WEDGE_ANGLE / 2 ) ) * entitySize;

            // Find the curve control point
            var cx = Math.cos( ba ) * entitySize * 0.3;
            var cy = Math.sin( ba ) * entitySize * 0.3;

            // Color code entity based on score compared to most successful
            var currentBestScore = getAbsoluteScore(currentBestAnimal);
            var g = Math.floor(255 * (1 - (currentBestScore === 0 ? 0 : getAbsoluteScore(animal) / currentBestScore )));
            context.fillStyle = "rgb(255," + g + ",0)";

            // Draw line based on Age
            context.strokeStyle = animal.life.age < 30 ? "#fff" : "#000";
            context.lineWidth = 2 + Math.floor(animal.life.ageN() * 5);

            // Draw the triangle
            context.beginPath();
            context.moveTo( p.x, p.y );
            context.lineTo( p.x + lx, p.y + ly );
            context.quadraticCurveTo( p.x + cx, p.y + cy, p.x + rx, p.y + ry );
            context.closePath();
            context.stroke();
            context.fill();
        }

        function getAbsoluteScore(animal) {
            return animal.life.score() < 0 ? 0 : animal.life.score();
        }

        var Self = function (population) {
            this.population = population;

            var that = this;
            this.entityRunObserver = new Observer(function (entity) {
                that.draw(entity);
            });
        };

        Self.prototype = {
            // Draw an entity on the canvas
            draw: function (animal) {
                var currentBestAnimal = this.population.entities[0];

                // Draw a halo around the current best entity
                if (animal === currentBestAnimal) {
                    drawTargetLines(animal);
                    drawHalo(animal);
                    drawFieldOfView(animal);
                }

                if (animal.willMate()) {
                    drawMateHalo(animal);
                }

                if (animal.willEat()) {
                    drawEatHalo(animal);
                }

                drawSize(animal);
                drawAnimal(animal, currentBestAnimal);
            }
        };

        return Self;
    }
);