define([
    'utils/permutations'
    ],
    function (Permutations, undefined) {
        "use strict";

        var permutationsCollection = [];
        function getA(a, b) { return a; }
        function getB(a, b) { return b; }

        // Gets an Array representation of the mateState of the gen.
        function convertToStateArray(gen) {
            var mateState = gen.getMateState();

            var stateArray = [];
            for (var propertyName in mateState) {
                stateArray.push( mateState[propertyName] );
            }

            return stateArray;
        }

        // Combines the propertyNames with their values.
        function convertToMateState(array, propertyNames) {
            var mateState = {};

            for (var i = 0; i < array.length; i++) {
                mateState[propertyNames[i]] = array[i];
            }

            return mateState;
        }

        // Gets the propertyNames of the mateState.
        function getPropertyNameArray(gen) {
            var mateState = gen.getMateState();

            var propertyNames = [];
            for (var propertyName in mateState) {
                propertyNames.push( propertyName );
            }

            return propertyNames;
        }

        // Creates two child entities by crossing over the genomes of a and b.
        var mate = function (a, b, createCallback) {
            var propertyNames = getPropertyNameArray(a);
            var propertyCount = propertyNames.length;

            if (permutationsCollection[propertyCount] === undefined) {
                permutationsCollection[propertyCount] = new Permutations(getA, getB, propertyCount);
            }

            var values = permutationsCollection[propertyCount].getRandomPermutationPair();
            var children = [];

            for (var i=0; i<2; i++) {
                var stateArray = [];
                for (var j=0; j<propertyCount; j++) {
                    var AorB = values[i][j](a, b);
                    stateArray.push( convertToStateArray(AorB)[j] );
                }

                var mateState = convertToMateState(stateArray, propertyNames);
                children.push( createCallback(mateState) );
            }

            return children;
        };

        return {mate: mate};
    }
);
