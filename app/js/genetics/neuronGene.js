define([
    'utils/range',
    'genetics/genetics',
    'genetics/axonGene',
    'genetics/options'
    ],
    function (Range, genetics, AxonGene, geneticsOptions, undefined) {
        "use strict";

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = geneticsOptions;

            this.minThreshold = options.neuron.minThreshold.get();
            this.maxThreshold = options.neuron.maxThreshold.get();
            this.maxRelaxation = options.neuron.maxRelaxation.get();

            this.thresholdMutationRate = options.neuron.thresholdMutationRate.get();
            this.thresholdReplacementRate = options.neuron.thresholdReplacementRate.get();
            this.relaxationMutationRate = options.neuron.relaxationMutationRate.get();
            this.relaxationReplacementRate = options.neuron.relaxationReplacementRate.get();

            this.axonGeneReplacementRate = options.neuron.axonGeneReplacementRate.get();

            this.mutationFraction = options.evolution.mutationFraction.get();
        }

        var Self = function (maxOutputs, state) {
            this.axons = [];

            if (state === undefined) {
                var options = new Options();

                this.threshold = new Range(options.minThreshold, options.maxThreshold).random();
                this.relaxation = Math.floor(new Range(0, options.maxRelaxation).random()) / 100;

                for (var i=0; i<maxOutputs; i++) {
                    this.axons.push(new AxonGene());
                }
            }
            else {
                this.threshold = state.threshold;
                this.relaxation = state.relaxation;

                for (var i=0; i<state.axons.length; i++) {
                    this.axons.push(new AxonGene(state.axons[i]));
                }
            }

            // Get state for persistance
            this.getState = function () {
                var axons = [];
                for (var i=0; i<this.axons.length; i++) {
                    axons.push(this.axons[i].getState());
                }

                return {
                    threshold: this.threshold,
                    relaxation: this.relaxation,
                    axons: axons
                };
            };

            // Get state for reproduction
            this.getMateState = function () {
                var state = this.getState();
                delete state.axons;

                return state;
            };
        };

        Self.prototype = {
            mutate: function() {
                var options = new Options();
                var range;

                if (Math.random() < options.thresholdReplacementRate) {
                    this.threshold = new Range(options.minThreshold, options.maxThreshold).random();
                }
                else if (Math.random() < options.thresholdMutationRate) {
                    range = new Range(options.minThreshold, options.maxThreshold);
                    this.threshold += range.mutation(options.mutationFraction);
                    this.threshold = range.checkLower(this.threshold);
                }

                if (Math.random() < options.relaxationReplacementRate) {
                    this.relaxation = Math.floor(new Range(0, options.maxRelaxation).random()) / 100;
                }
                else if (Math.random() < options.relaxationMutationRate) {
                    range = new Range(0, options.maxRelaxation);
                    this.relaxation += Math.floor(range.mutation(options.mutationFraction)) / 100;
                    this.relaxation = range.check(this.relaxation);
                }

                for (var i=0; i<this.axons.length; i++) {
                    if (Math.random() < options.axonGeneReplacementRate) {
                        this.axons[i] = new AxonGene();
                        continue;
                    }

                    this.axons[i].mutate();
                }
            },

            clone: function() {
                return new Self(undefined, this.getState());
            },

            mate: function (partner) {
                var children = genetics.mate(this, partner, function (child) {
                    child.axons = [];
                    return new Self(undefined, child);
                });

                for (var i=0; i<this.axons.length; i++) {
                    var childAxons = this.axons[i].mate(partner.axons[i]);

                    for (var j=0; j<children.length; j++) {
                        children[j].axons.push( childAxons[j] );
                    }
                }

                return children;
            }
        };

        return Self;
    }
);
