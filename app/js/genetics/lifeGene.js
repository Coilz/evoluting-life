define([
    'utils/range',
    'genetics/genetics',
    'genetics/options'
    ],
    function (Range, genetics, geneticsOptions, undefined) {
        "use strict";

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = geneticsOptions;

            this.minInitialEnergy = options.life.minInitialEnergy.get();
            this.maxInitialEnergy = options.life.maxInitialEnergy.get();
            this.minOldAge = options.life.minOldAge.get();
            this.maxOldAge = options.life.maxOldAge.get();
            this.minMatureAge = options.life.minMatureAge.get();
            this.maxMatureAge = options.life.maxMatureAge.get();
            this.minNutrition = options.life.minNutrition.get();
            this.maxNutrition = options.life.maxNutrition.get();

            this.initialEnergyMutationRate = options.life.initialEnergyMutationRate.get();
            this.oldAgeMutationRate = options.life.oldAgeMutationRate.get();
            this.matureAgeMutationRate = options.life.matureAgeMutationRate.get();
            this.nutritionMutationRate = options.life.nutritionMutationRate.get();

            this.mutationFraction = options.evolution.mutationFraction.get();
        }

        var Self = function (state) {
            if (state === undefined) {
                var options = new Options();

                this.initialEnergy = new Range(options.minInitialEnergy, options.maxInitialEnergy).random();
                this.oldAge = new Range(options.minOldAge, options.maxOldAge).random();
                this.matureAge = new Range(options.minMatureAge, options.maxMatureAge).random();
                this.nutrition = new Range(options.minNutrition, options.maxNutrition).random();
            }
            else {
                this.initialEnergy = state.initialEnergy;
                this.oldAge = state.oldAge;
                this.matureAge = state.matureAge;
                this.nutrition = state.nutrition;
            }

            this.getState = function () {
                return {
                    initialEnergy: this.initialEnergy,
                    oldAge: this.oldAge,
                    matureAge: this.matureAge,
                    nutrition: this.nutrition
                };
            };

            this.getMateState = function () {
                return this.getState();
            };
        };

        Self.prototype = {
            mutate: function() {
                var options = new Options();

                if (Math.random() <= options.initialEnergyMutationRate) {
                    this.initialEnergy += new Range(options.minInitialEnergy, options.maxInitialEnergy).mutation(options.mutationFraction);
                }

                if (Math.random() <= options.oldAgeMutationRate) {
                    this.oldAge += new Range(options.minOldAge, options.maxOldAge).mutation(options.mutationFraction);
                }

                if (Math.random() <= options.matureAgeMutationRate) {
                    this.matureAge += new Range(options.minMatureAge, options.maxMatureAge).mutation(options.mutationFraction);
                }

                if (Math.random() <= options.nutritionMutationRate) {
                    this.nutrition += new Range(options.minNutrition, options.maxNutrition).mutation(options.mutationFraction);
                }
            },

            clone: function () {
                return new Self(this.getState());
            },

            mate: function (partner) {
                return genetics.mate(this, partner, function (mateState) {
                    return new Self(mateState);
                });
            }
        };

        return Self;
    }
);