define([
    'utils/range',
    'genetics/genetics',
    'genetics/options'
    ],
    function (Range, genetics, geneticsOptions, undefined) {
        "use strict";

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = geneticsOptions;

            this.minAngularForce = options.movement.minAngularForce.get();
            this.maxAngularForce = options.movement.maxAngularForce.get();
            this.minLinearForce = options.movement.minLinearForce.get();
            this.maxLinearForce = options.movement.maxLinearForce.get();

            this.angularForceMutationRate = options.movement.angularForceMutationRate.get();
            this.angularForceReplacementRate = options.movement.angularForceReplacementRate.get();
            this.linearForceMutationRate = options.movement.linearForceMutationRate.get();
            this.linearForceReplacementRate = options.movement.linearForceReplacementRate.get();

            this.mutationFraction = options.evolution.mutationFraction.get();
        }

        var Self = function (state) {
            if (state === undefined) {
                var options = new Options();

                this.angularForce = new Range(options.minAngularForce, options.maxAngularForce).random();
                this.linearForce = new Range(options.minLinearForce, options.maxLinearForce).random();
            }
            else {
                this.angularForce = state.angularForce;
                this.linearForce = state.linearForce;
            }

            this.getState = function () {
                return {
                    angularForce: this.angularForce,
                    linearForce: this.linearForce
                };
            };

            this.getMateState = function () {
                return this.getState();
            };
        };

        Self.prototype = {
            mutate: function() {
                var options = new Options();

                if (Math.random() <= options.angularForceReplacementRate) {
                    this.angularForce = new Range(options.minAngularForce, options.maxAngularForce).random();
                }
                else if (Math.random() <= options.angularForceMutationRate) {
                    this.angularForce += new Range(options.minAngularForce, options.maxAngularForce).mutation(options.mutationFraction);
                    this.angularForce = new Range(0, 0).checkLower(this.angularForce);
                }

                if (Math.random() <= options.linearForceReplacementRate) {
                    this.linearForce = new Range(options.minLinearForce, options.maxLinearForce).random();
                }
                else if (Math.random() <= options.linearForceMutationRate) {
                    this.linearForce += new Range(options.minLinearForce, options.maxLinearForce).mutation(options.mutationFraction);
                    this.linearForce = new Range(0, 0).checkLower(this.linearForce);
                }
            },

            clone: function () {
                return new Self(this.getState());
            },

            mate: function (partner) {
                return genetics.mate(this, partner, function (child) {
                    return new Self(child);
                });
            }
        };

        return Self;
    }
);
