define([
  'option',
  'options'
],
  function(Option, worldOptions, undefined) {
    "use strict";

    var options = {
      evolution: {
        /*
            Mutations can be:
            - replace a gene
            - adding/removing a layer after removing a gene when the number of genes is lower than x
            - adding/removing a gene
            - modify a genes threshold
            - modify a genes relaxation
            - modify axon strength
        */
        mutationFraction: new Option(0.005), // the percentual delta of the change (up or down)

        reset: function() {
          this.mutationFraction.reset();
        }
      },

      genome: {
        sensorGeneReplacementRate: new Option(0.0001),
        movementGeneReplacementRate: new Option(0.0001),

        reset: function() {
          this.sensorGeneReplacementRate.reset();
          this.movementGeneReplacementRate.reset();
        }
      },

      life: {
        minInitialEnergy: new Option(5),
        maxInitialEnergy: new Option(11),
        minOldAge: new Option(3000),
        maxOldAge: new Option(4000),
        minMatureAge: new Option(0.1),
        maxMatureAge: new Option(0.4),
        minNutrition: new Option(1.0),
        maxNutrition: new Option(3.0),

        initialEnergyMutationRate: new Option(0.06),
        oldAgeMutationRate: new Option(0.06),
        matureAgeMutationRate: new Option(0.06),
        nutritionMutationRate: new Option(0.06),

        reset: function() {
          this.minInitialEnergy.reset();
          this.maxInitialEnergy.reset();
          this.minOldAge.reset();
          this.maxOldAge.reset();
          this.minMatureAge.reset();
          this.maxMatureAge.reset();
          this.minNutrition.reset();
          this.maxNutrition.reset();

          this.initialEnergyMutationRate.reset();
          this.oldAgeMutationRate.reset();
          this.matureAgeMutationRate.reset();
          this.nutritionMutationRate.reset();
        }
      },

      sensor: {
        minViewDistance: new Option(16 * 1),
        maxViewDistance: new Option(16 * 14),
        minFieldOfView: new Option(Math.PI / 32),
        maxFieldOfView: new Option(Math.PI),

        viewDistanceMutationRate: new Option(0.1),
        viewDistanceReplacementRate: new Option(0.005),
        fieldOfViewMutationRate: new Option(0.1),
        fieldOfViewReplacementRate: new Option(0.005),

        reset: function() {
          this.minViewDistance.reset();
          this.maxViewDistance.reset();
          this.minFieldOfView.reset();
          this.maxFieldOfView.reset();

          this.viewDistanceMutationRate.reset();
          this.viewDistanceReplacementRate.reset();
          this.fieldOfViewMutationRate.reset();
          this.fieldOfViewReplacementRate.reset();
        }
      },

      movement: (function() {
        // F = Vmax * friction
        var calculateForce = function(maxVelocity, friction) {
          return maxVelocity * friction;
        };

        var minA = calculateForce(0.1, worldOptions.physics.angularFriction.get());
        var maxA = calculateForce(1.5, worldOptions.physics.angularFriction.get());
        var minL = calculateForce(5.0, worldOptions.physics.linearFriction.get());
        var maxL = calculateForce(50.0, worldOptions.physics.linearFriction.get());

        return {
          minAngularForce: new Option(minA),
          maxAngularForce: new Option(maxA),
          minLinearForce: new Option(minL),
          maxLinearForce: new Option(maxL),

          angularForceMutationRate: new Option(0.07),
          angularForceReplacementRate: new Option(0.005),
          linearForceMutationRate: new Option(0.07),
          linearForceReplacementRate: new Option(0.005),

          reset: function() {
            this.minAngularForce.reset();
            this.maxAngularForce.reset();
            this.minLinearForce.reset();
            this.maxLinearForce.reset();

            this.angularForceMutationRate.reset();
            this.angularForceReplacementRate.reset();
            this.linearForceMutationRate.reset();
            this.linearForceReplacementRate.reset();
          }
        };
      })(),

      brain: {
        minHiddenLayers: new Option(1),
        maxHiddenLayers: new Option(5),
        maxNeuronsPerLayer: new Option(18),

        layerMutationRate: new Option(0.01),    // adding or removing a gene
        neuronMutationRate: new Option(0.3),     // percentual chance of genes within a genome to mutate
        neuronReplacementRate: new Option(0.0005),  // completely replacing a neurons properties

        reset: function() {
          this.minHiddenLayers.reset();
          this.maxHiddenLayers.reset();
          this.maxNeuronsPerLayer.reset();

          this.layerMutationRate.reset();
          this.neuronMutationRate.reset();
          this.neuronReplacementRate.reset();
        }
      },

      neuron: {
        minThreshold: new Option(0.1),
        maxThreshold: new Option(1.0), // 30
        maxRelaxation: new Option(99),

        thresholdMutationRate: new Option(0.05),
        thresholdReplacementRate: new Option(0.005),
        relaxationMutationRate: new Option(0.05),
        relaxationReplacementRate: new Option(0.005),

        axonGeneReplacementRate: new Option(0.005),

        reset: function() {
          this.minThreshold.reset();
          this.maxThreshold.reset();
          this.maxRelaxation.reset();

          this.thresholdMutationRate.reset();
          this.thresholdReplacementRate.reset();
          this.relaxationMutationRate.reset();
          this.relaxationReplacementRate.reset();

          this.axonGeneReplacementRate.reset();
        }
      },

      axon: {
        maxStrength: new Option(0.6), // 30

        minStrengthening: new Option(0.000001),
        maxStrengthening: new Option(0.00002),

        minWeakening: new Option(0.000001),
        maxWeakening: new Option(0.000005),

        strengthMutationRate: new Option(0.1),
        strengtheningMutationRate: new Option(0.05),
        weakeningMutationRate: new Option(0.05),

        reset: function() {
          this.maxStrength.reset();

          this.minStrengthening.reset();
          this.maxStrengthening.reset();
          this.minWeakening.reset();
          this.maxWeakening.reset();

          this.strengthMutationRate.reset();
          this.strengtheningMutationRate.reset();
          this.weakeningMutationRate.reset();
        }
      },

      reset: function() {
        this.evolution.reset();
        this.genome.reset();
        this.life.reset();
        this.sensor.reset();
        this.movement.reset();
        this.brain.reset();
        this.neuron.reset();
        this.axon.reset();
      }
    };

    return options;
  }
);