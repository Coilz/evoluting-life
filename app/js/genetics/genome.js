define([
    'genetics/brainGene',
    'genetics/lifeGene',
    'genetics/sensorGene',
    'genetics/movementGene',
    'genetics/options'
],
    function (BrainGene, LifeGene, SensorGene, MovementGene, geneticsOptions, undefined) {
        "use strict";

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = geneticsOptions;

            this.sensorGeneReplacementRate = options.genome.sensorGeneReplacementRate.get();
            this.movementGeneReplacementRate = options.genome.movementGeneReplacementRate.get();
        }

        var Self = function (inputCount, outputCount, state) {
            var brain;
            var life;
            var sensor;
            var movement;

            if (state === undefined) {
                brain = new BrainGene(inputCount, outputCount);
                life = new LifeGene();
                sensor = new SensorGene();
                movement = new MovementGene();
            }
            else {
                brain = new BrainGene(undefined, undefined, state.brain);
                life = new LifeGene(state.life);
                sensor = new SensorGene(state.sensor);
                movement = new MovementGene(state.movement);
            }

            this.getBrain = function () {
                return brain;
            };

            this.getLife = function () {
                return life;
            };

            this.getSensor = function () {
                return sensor;
            };

            this.getMovement = function () {
                return movement;
            };

            this.getState = function () {
                return {
                    brain: brain.getState(),
                    life: life.getState(),
                    sensor: sensor.getState(),
                    movement: movement.getState()
                };
            };

            this.mutate = function () {
                var options = new Options();

                this.getBrain().mutate();
                this.getLife().mutate();

                if (Math.random() < options.sensorGeneReplacementRate) {
                    sensor = new SensorGene();
                }
                else {
                    this.getSensor().mutate();
                }

                if (Math.random() < options.movementGeneReplacementRate) {
                    movement = new MovementGene();
                }
                else {
                    this.getMovement().mutate();
                }
            }
        };

        Self.prototype = {
            clone: function () {
                return new Self(undefined, undefined, this.getState());
            },

            mate: function (partner) {
                var brainChildren = this.getBrain().mate(partner.getBrain());
                var lifeChildren = this.getLife().mate(partner.getLife());
                var sensorChildren = this.getSensor().mate(partner.getSensor());
                var movementChildren = this.getMovement().mate(partner.getMovement());

                var children = [];
                for (var i = 0; i < brainChildren.length; i++) {
                    children.push(new Self(undefined, undefined, {
                        brain: brainChildren[i].getState(),
                        life: lifeChildren[i].getState(),
                        sensor: sensorChildren[i].getState(),
                        movement: movementChildren[i].getState()
                    }));
                }

                return children;
            }
        };

        return Self;
    }
);