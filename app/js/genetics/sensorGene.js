define([
  'utils/range',
  'genetics/genetics',
  'genetics/options'
],
  function(Range, genetics, geneticsOptions, undefined) {
    "use strict";

    // For convenience and preventing errors due to loading sequence
    function Options() {
      var options = geneticsOptions;

      this.minViewDistance = options.sensor.minViewDistance.get();
      this.maxViewDistance = options.sensor.maxViewDistance.get();
      this.minFieldOfView = options.sensor.minFieldOfView.get();
      this.maxFieldOfView = options.sensor.maxFieldOfView.get();

      this.viewDistanceMutationRate = options.sensor.viewDistanceMutationRate.get();
      this.viewDistanceReplacementRate = options.sensor.viewDistanceReplacementRate.get();
      this.fieldOfViewMutationRate = options.sensor.fieldOfViewMutationRate.get();
      this.fieldOfViewReplacementRate = options.sensor.fieldOfViewReplacementRate.get();

      this.mutationFraction = options.evolution.mutationFraction.get();
    }

    var Self = function(state) {
      if (state === undefined) {
        var options = new Options();

        this.viewDistance = new Range(options.minViewDistance, options.maxViewDistance).random();
        this.fieldOfView = new Range(options.minFieldOfView, options.maxFieldOfView).random();
      }
      else {
        this.viewDistance = state.viewDistance;
        this.fieldOfView = state.fieldOfView;
      }

      this.getState = function() {
        return {
          viewDistance: this.viewDistance,
          fieldOfView: this.fieldOfView
        };
      };

      this.getMateState = function() {
        return this.getState();
      };
    };

    Self.prototype = {
      mutate: function() {
        var options = new Options();

        if (Math.random() < options.viewDistanceReplacementRate) {
          this.viewDistance = new Range(options.minViewDistance, options.maxViewDistance).random();
        }
        else if (Math.random() <= options.viewDistanceMutationRate) {
          this.viewDistance += new Range(options.minViewDistance, options.maxViewDistance).mutation(options.mutationFraction);
        }

        if (Math.random() < options.fieldOfViewReplacementRate) {
          this.fieldOfView = new Range(options.minFieldOfView, options.maxFieldOfView).random();
        }
        else if (Math.random() <= options.fieldOfViewMutationRate) {
          this.fieldOfView += new Range(options.minFieldOfView, options.maxFieldOfView).mutation(options.mutationFraction);
          this.fieldOfView = new Range(0, 2 * Math.PI).check(this.fieldOfView);
        }
      },

      clone: function() {
        return new Self(this.getState());
      },

      mate: function(partner) {
        return genetics.mate(this, partner, function(child) {
          return new Self(child);
        });
      }
    };

    return Self;
  }
);
