define([
	'utils/range',
	'genetics/options',
	'genetics/neuronGene'
	],
	function (Range, geneticsOptions, NeuronGene, undefined) {
		"use strict";

		// For convenience and preventing errors due to loading sequence
		function Options() {
			var options = geneticsOptions;

			this.minHiddenLayers = options.brain.minHiddenLayers.get();
			this.maxHiddenLayers = options.brain.maxHiddenLayers.get();
			this.maxNeuronsPerLayer = options.brain.maxNeuronsPerLayer.get();

			this.neuronMutationRate = options.brain.neuronMutationRate.get();
			this.neuronReplacementRate = options.brain.neuronReplacementRate.get();
		}

		var EmptyChild = function (layerCount) {
			this.layers = [];
			while(layerCount--) {
				this.layers.push([]);
			}
		};

		function createChildClones (a, b) {
			return Math.random() < 0.5 ?
				[a.clone(), b.clone()] :
				[b.clone(), a.clone()];
		}

		function createLayer(maxTargetCount, neuronCount) {
			var neuronGenes = [];
			while (neuronCount--) {
				neuronGenes.push( new NeuronGene(maxTargetCount) );
			}

			return neuronGenes;
		}

		var Self = function(inputCount, outputCount, state) {
			var layers = [];

			if (state === undefined) {
				// Construct the output layer
				layers.push( createLayer(0, outputCount) );

				var options = new Options();

				// Construct the hidden layers
				var minGenesPerHL = Math.max(inputCount, outputCount);
				var numberOfHL = Math.floor(new Range(options.minHiddenLayers, options.maxHiddenLayers).random());
				for (var i=0; i<numberOfHL; i++) {

					var maxTargetCount = layers[layers.length - 1].length;
					var neuronCount = Math.floor(new Range(minGenesPerHL, options.maxNeuronsPerLayer).random());

					layers.push( createLayer(maxTargetCount, neuronCount) );
				}

				var maxTargetCount = layers[layers.length - 1].length;

				// Construct the input layer
				layers.push( createLayer(maxTargetCount, inputCount) );
			}
			else {
				var stateLayers = state.layers;

				for(var i=0; i<stateLayers.length; i++) {
					var layer = [];

					for(var j=0; j<stateLayers[i].length; j++) {
						layer.push(new NeuronGene(undefined, stateLayers[i][j]));
					}

					layers.push(layer);
				}
			}

			this.getLayers = function() {
				return layers;
			};

			this.getState = function () {
				var layersState = [];
				for (var i=0; i<layers.length; i++) {
					var layerState = [];
					for (var j=0; j<layers[i].length; j++) {
						layerState.push( layers[i][j].getState() );
					}
					layersState.push( layerState );
				}

				return {layers: layersState};
			};
		};

		Self.prototype = {
			mutate: function() {
				var options = new Options();
				var layers = this.getLayers();

				for (var i=0; i<layers.length; i++) {
					for (var j=0; j<layers[i].length; j++) {
						if (Math.random() < options.neuronReplacementRate) {
							var targetCount = (i===0) ? 0 : layers[i-1].length;
							layers[i][j] = new NeuronGene(targetCount);
							continue;
						}

						if (Math.random() < options.neuronMutationRate) {
							layers[i][j].mutate();
						}
					}
				}
			},

			clone: function() {
				return new Self(undefined, undefined, this.getState());
			},

			mate: function (partner) {
				var a = this.getLayers();
				var b = partner.getLayers();

				if (a.length !== b.length) {
					return createChildClones(this, partner);
				}

				var c = new EmptyChild(a.length);
				var children = [
					new Self(undefined, undefined, c),
					new Self(undefined, undefined, c)
				];

				for (var i=0; i<a.length; i++) { // layers
					if (a[i].length !== b[i].length) {
						return createChildClones(this, partner);
					}

					for (var j=0; j<a[i].length; j++) { // neurons
						var childNeurons = a[i][j].mate( b[i][j] );
						for (var k=0; k<2; k++) {
							children[k].getLayers()[i].push(childNeurons[k]);
						}
					}
				}

				return children;
			}
		};

		return Self;
	}
);
