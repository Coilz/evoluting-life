define([
  'utils/range',
  'genetics/options',
  'genetics/genetics'
],
  function(Range, geneticsOptions, genetics, undefined) {
    "use strict";

    // For convenience and preventing errors due to loading sequence
    function Options() {
      var options = geneticsOptions;

      this.maxStrength = options.axon.maxStrength.get();
      this.minStrengthening = options.axon.minStrengthening.get();
      this.maxStrengthening = options.axon.maxStrengthening.get();
      this.minWeakening = options.axon.minWeakening.get();
      this.maxWeakening = options.axon.maxWeakening.get();

      this.strengthMutationRate = options.axon.strengthMutationRate.get();
      this.strengtheningMutationRate = options.axon.strengtheningMutationRate.get();
      this.weakeningMutationRate = options.axon.weakeningMutationRate.get();

      this.mutationFraction = options.evolution.mutationFraction.get();
    }

    var Self = function(state) {
      if (state === undefined) {
        var options = new Options();

        this.strength = Range(-1 * options.maxStrength, options.maxStrength).random();
        //this.strength = new Range(0, options.maxStrength).random();
        this.strengthening = new Range(options.minStrengthening, options.maxStrengthening).random();
        this.weakening = new Range(options.minWeakening, options.maxWeakening).random();
      }
      else {
        this.strength = state.strength;
        this.strengthening = state.strengthening;
        this.weakening = state.weakening;
      }

      this.getState = function() {
        return {
          strength: this.strength,
          strengthening: this.strengthening,
          weakening: this.weakening
        };
      };

      this.getMateState = function() {
        return this.getState();
      };
    };

    Self.prototype = {
      mutate: function() {
        var options = new Options();

        if (Math.random() <= options.strengthMutationRate) {
          var range = new Range(-1 * options.maxStrength, options.maxStrength);
          //var range = new Range(0, options.maxStrength);
          this.strength += range.mutation(options.mutationFraction);
          //this.strength = range.checkLower(this.strength);
        }

        if (Math.random() <= options.strengtheningMutationRate) {
          var range = new Range(options.minStrengthening, options.maxStrengthening);
          this.strengthening += range.mutation(options.mutationFraction);
          this.strengthening = range.check(this.strengthening);
        }

        if (Math.random() <= options.weakeningMutationRate) {
          var range = new Range(options.minWeakening, options.maxWeakening);
          this.weakening += range.mutation(options.mutationFraction);
          this.weakening = range.check(this.weakening);
        }
      },

      clone: function() {
        return new Self(this.getState());
      },

      mate: function(partner) {
        return genetics.mate(this, partner, function(child) {
          return new Self(child);
        });
      }
    };

    return Self;
  }
);
