define([], function (undefined) {
    "use strict";

    var instance;
    var init = function () {
        var worldElement = document.getElementById('world');

        window.onresize = function( event ) {
            worldElement.width = window.innerWidth;
            worldElement.height = window.innerHeight - 192;
            document.getElementById('pov').height = worldElement.height/2;
            document.getElementById('pov').width = worldElement.height;
            document.getElementById('graph').height = worldElement.height/2;
            document.getElementById('graph').width = worldElement.width - document.getElementById('sidebar').clientWidth;
            document.getElementById('diagram').height = worldElement.height;
            document.getElementById('diagram').width = worldElement.height;
            document.getElementById('sidebar').height = worldElement.height;
        };

        // Do initial canvas sizing
        window.onresize();

        // Check browser compatability
        if (!worldElement || !worldElement.getContext) {
            alert("Your browser can't run this demo. Please try the lastest Firefox or Chrome browser instead.");
            return;
        }

        return worldElement;
    };

    if (!instance) {
        instance = init();
    }

    return instance;
});
