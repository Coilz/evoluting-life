define([
    'option'
    ],
    function (Option, undefined) {
        "use strict";

        var options = {
            physics: {
                linearFriction: new Option(0.06), // 0.065 // 0.024
                angularFriction: new Option(0.09), // 0.25 // 0.08

                reset: function() {
                    this.linearFriction.reset();
                    this.angularFriction.reset();
                }
            },

            reset: function() {
                this.physics.reset();
            }
        };

        return options;
    }
);