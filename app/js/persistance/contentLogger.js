define([],
    function (undefined) {
        "use strict";

        var persist = function (content) {
            var contentAsJson = JSON.stringify(content);
            console.log(contentAsJson);
        };

        var Self = function (contentCallback) {
            var saveButton = document.getElementById('saveButton');

            saveButton.addEventListener("click", function() {
                var content = contentCallback();
                persist(content);
            });
        };

        return Self;
    }
);
