define([],
  function (undefined) {
    "use strict";

    var persist = function (content, filename) {
      if (filename === undefined || filename === null || filename === '') {
        var date = new Date();
        filename = 'populationGenes' + date.toISOString();
      }

      var data = JSON.stringify(content);
      var blob = new Blob([data], {
        type: "application/json"
      });
      var downloadUrl = URL.createObjectURL(blob);

      var a = document.getElementById('downloadAnchorElem');
      a.href = downloadUrl;
      a.download = filename + '.json';
      a.click();
    };

    var Self = function (contentCallback) {
      var filenameInput = document.getElementById('exportFilename');
      var saveButton = document.getElementById('saveButton');

      saveButton.addEventListener("click", function () {
        var content = contentCallback();
        persist(content, filenameInput.value);
      });
    };


    return Self;
  }
);
