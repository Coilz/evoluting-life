define([],
  function (undefined) {
    "use strict";

    var load = function (file, merge, contentCallback) {
      if (!file) return;

      var reader = new FileReader();
      reader.onload = function (e) {
        contentCallback(JSON.parse(e.target.result), merge);
      };
      reader.readAsText(file);
    };

    var Self = function (contentCallback) {
      var mergeImport = document.getElementById("mergeImport");
      var importData = document.getElementById("importData");
      var uploadLink = document.getElementById("uploadLink");

      importData.onchange = function(e) {
        var file = importData.files[0];
        load(file, mergeImport.checked, contentCallback);
      };
    };

    return Self;
  }
);
