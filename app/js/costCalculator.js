define([],
    function (undefined) {
		"use strict";

		var ENERGY_COST = 0.001;

		function cycleCost () {
			return ENERGY_COST * 30;
		}

		function mateCost (initialEnergy) {
			return ENERGY_COST * 1000 * initialEnergy;
		}

		function accelerationCost (acceleration) {
			return ENERGY_COST * Math.abs(acceleration);
		}

		function angularAccelerationCost (acceleration) {
			return ENERGY_COST * Math.abs(acceleration);
		}

		return {
			cycle: cycleCost,
			mate: mateCost,
			accelerate: accelerationCost,
			rotate: angularAccelerationCost
		};
	}
);