define([
  'observe/observer',
  'display/plantPrinter',
  'display/animalPrinter',
  'display/statsCollector',
  'display/statsPrinter',
  'display/fitnessGraph',
  'display/countersCollector',
  'display/countersPrinter',
  'display/brainDiagram',
  'display/povPrinter',
  'persistance/contentDownloader',
  'persistance/contentUploader',
  'foodSupply',
  'population',
  'worldCanvas'
],
  function (
    Observer,
    PlantPrinter,
    AnimalPrinter,
    StatsCollector,
    StatsPrinter,
    FitnessGraph,
    CountersCollector,
    CountersPrinter,
    BrainDiagram,
    PovPrinter,
    ContentPersister,
    ContentLoader,
    FoodSupply,
    Population,
    worldCanvas,
    undefined) {
    "use strict";

    var canvas = worldCanvas;
    var context = canvas.getContext('2d');

    // Handle non-existent consoles
    if (console === undefined) console = { log: function () { } };

    var genesis = function () {
      // Create Organisms
      var foodSupply = new FoodSupply();
      var population = new Population();

      // Import & Export of genes
      var contentPersister = new ContentPersister(function () {
        return population.getState();
      });

      var contentLoader = new ContentLoader(function (genomeStates, merge) {
        population.loadState(genomeStates, merge);
      });

      // Create printers
      var getIteration = function () {
        return iteration;
      };

      var getPopulationNumbers = function () {
        return population.runNumbers;
      };

      // OrganismPrinters
      var plantPrinter = new PlantPrinter();
      foodSupply.entityRunNotifier.addObserver(plantPrinter.entityRunObserver);

      var animalPrinter = new AnimalPrinter(population);
      population.entityRunNotifier.addObserver(animalPrinter.entityRunObserver);

      // StatsPrinter
      var statsTable = document.getElementById('stats-tbody');
      var statsCollector = new StatsCollector(getPopulationNumbers);
      var statsPrinter = new StatsPrinter(statsTable, statsCollector);

      // FitnessGraph
      var graphElement = document.getElementById('graph');
      var fitnessGraph = new FitnessGraph(graphElement, getPopulationNumbers);

      // CountersPrinter
      var countersElement = document.getElementById('time');
      var countersCollector = new CountersCollector(getIteration, getPopulationNumbers);
      var countersPrinter = new CountersPrinter(countersElement, countersCollector);

      // BrainDiagram
      var diagramElement = document.getElementById('diagram');
      var brainDiagram = new BrainDiagram(diagramElement);
      population.newWinnerNotifier.addObserver(brainDiagram.newWinnerObserver);

      // povPrinter
      var povElement = document.getElementById('pov');
      var povPrinter = new PovPrinter(povElement);
      population.newWinnerNotifier.addObserver(povPrinter.newWinnerObserver);

      var statsInterval = statsPrinter.periodicalReport();
      var graphInterval = fitnessGraph.periodicalReport();

      // Register observers
      var entityCreatedObserver = new Observer(function (entity) {
        registerObservers(statsCollector, entity);
      });

      population.entityCreated.addObserver(entityCreatedObserver);
      population.entityCreated.addObserver(countersCollector.bornObserver);

      population.createEntities();

      // Start the loop
      var iteration = 0;
      var mainLoop = function () {
        // Keep track of our iteration count
        iteration++;

        // Clear the drawing area
        context.clearRect(0, 0, canvas.width, canvas.height);

        // Run a tick of foodSupply life cycle
        foodSupply.run();

        // Run a tick of population life cycle
        population.run(foodSupply.plants);

        // Print
        countersPrinter.drawCounters();
        statsInterval();
        graphInterval();
      };

      var animatId;
      var animate = function () {
        mainLoop();
        animatId = requestAnimationFrame(animate);
      };

      var paused = false;
      document.addEventListener("keydown", function (event) {
        var code = event.keyCode || event.which;
        if (code !== 32) return;

        if (paused) {
          paused = false;
          animatId = requestAnimationFrame(animate);
          return;
        }

        paused = true;
        cancelAnimationFrame(animatId);
      });

      animatId = requestAnimationFrame(animate);
    };

    function registerObservers(collector, entity) {
      entity.wanderedNotifier.addObserver(collector.wanderedObserver);
      entity.starvedNotifier.addObserver(collector.starvedObserver);
      entity.diedOfAgeNotifier.addObserver(collector.diedOfAgeObserver);
      entity.eatNotifier.addObserver(collector.eatObserver);
      entity.consumedNotifier.addObserver(collector.consumedObserver);
      entity.mateNotifier.addObserver(collector.mateObserver);
    }

    return {
      start: genesis
    };
  }
);
