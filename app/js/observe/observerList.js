define([],
    function (undefined) {
        "use strict";

        var Self = function () {
            var list = [];
            this.get_List = function () {
                return list;
            };
        };

        Self.prototype = {
            add: function (obj) {
                return ( obj === undefined ) ? this.get_List().length : this.get_List().push( obj );
            },

            remove: function (obj) {
                var index = this.indexOf(obj, 0);

                if (index < 0) return;

                this.removeAt(index);
            },

            count: function () {
                return this.get_List().length;
            },

            get: function (index) {
                if( index > -1 && index < this.get_List().length ){
                    return this.get_List()[ index ];
                }
            },

            indexOf: function (obj, startIndex) {
                var i = startIndex;
                var list = this.get_List();

                while ( i < list.length ) {
                    if ( list[i] === obj ) {
                        return i;
                    }
                    i++;
                }

                return -1;
            },

            removeAt: function (index) {
                this.get_List().splice( index, 1 );
            }
        };

        return Self;
    }
);
