define([
    'observe/observerList'
    ],
    function (ObserverList, undefined) {
        "use strict";

        function updateAsync (observer, context) {
            setTimeout(function () {
                observer.update( context );
            }, 0);
        }

        var Self = function () {
            var observers = new ObserverList();
            this.get_ObserverList = function () {
                return observers;
            };
        };

        Self.prototype = {
            addObserver: function (observer) {
                if (observer === undefined) return;

                this.get_ObserverList().add( observer );
            },

            removeObserver: function (observer) {
                if (observer === undefined) return;

                this.get_ObserverList().remove( observer );
            },

            notifyAsync: function (context) {
                var list = this.get_ObserverList();

                var count = list.count();
                for(var i=0; i < count; i++) {
                    updateAsync( list.get(i), context );
                }
            },

            notify: function (context) {
                var list = this.get_ObserverList();

                var count = list.count();
                for(var i=0; i < count; i++) {
                    list.get(i).update( context );
                }
            }
        };

        return Self;
    }
);
