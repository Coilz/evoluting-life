define([],
    function (undefined) {
        "use strict";
        
        var Self = function (callback) {
            this.callback = callback;
        };
         
        Self.prototype = {
            update: function (context) {
                this.callback(context);
            }
        };

        return Self;
    }
);