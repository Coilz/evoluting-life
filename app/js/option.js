define([],
    function (undefined) {
        "use strict";
        
        var Self = function (value) {
            var defaultValue = value;
            var internalValue = value;

            this.get = function () {
                return internalValue;
            };
            this.set = function(value) {
                internalValue = value;
            };
            this.reset = function() {
                internalValue = defaultValue;
            };
        };

        return Self;
    }
);