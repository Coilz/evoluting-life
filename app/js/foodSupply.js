define([
    'worldCanvas',
    'observe/subject',
    'utils/range',
    'entities/options',
    'genetics/genome',
    'entities/plant'
    ],
    function (worldCanvas, Subject, Range, entitiesOptions, Genome, Plant, undefined) {
        "use strict";

        var canvas = worldCanvas;

        // For convenience and preventing errors due to loading sequence
        function Options() {
            var options = entitiesOptions.plant;
            var BORDER = 20;

            this.populationSize = options.populationSize.get();
            this.minX = BORDER;
            this.maxX = canvas.width - BORDER;
            this.minY = BORDER;
            this.maxY = canvas.height - BORDER;
        }

        function createPlant() {
            var options = new Options();

            return new Plant(new Genome(0,0),
            {
                x: new Range(options.minX, options.maxX).random(),
                y: new Range(options.minY, options.maxY).random()
            });
        }

        var Self = function () {
            var options = new Options();
            this.plants = [];

            for ( var i = 0; i < options.populationSize; i++ ) {
                this.plants.push(createPlant());
            }
            this.entityRunNotifier = new Subject();
        };

        Self.prototype = {
            run: function() {
                for (var i=0; i < this.plants.length; i++) {
                    var plant = this.plants[i];

                    // Replace the food if it's outside canvas boundaries
                    if ( plant.position.x < 0 || plant.position.x > canvas.width || plant.position.y < 0 || plant.position.y > canvas.height ) {
                        plant = this.plants[i] = createPlant();
                    }

                    // Replace the food if it's exhausted
                    if (!plant.lives()) {
                        plant = this.plants[i] = createPlant();
                    }

                    plant.run();
                    this.entityRunNotifier.notify(plant);
                }
            }
        };

        return Self;
    }
);