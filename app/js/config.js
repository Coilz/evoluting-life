require.config({
    paths: {
        libs: '../libs'
    }
});

// Start the main app logic.
require(['main'],
function   (main) {
    main.start();
});