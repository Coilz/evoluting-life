define(['evolution/rouletteWheelSelectionByFitness'], function(RouletteWheelSelectionByFitness) {
	"use strict";

	describe("evolution.RouletteWheelSelectionByFitness", function() {
			beforeEach(function() {
				this.getScoreCallback = function (index) {
					return {
						context: index,
						value: index+3
					};
				};
				this.sut = new RouletteWheelSelectionByFitness(this.getScoreCallback);
			});

		describe("when Math.random always returns 0.0", function() {
			beforeEach(function() {
				this.rnd = Math.random;
				Math.random = function() {return 0.0;};
			});

			afterEach(function() {
				Math.random = this.rnd;
			});

			describe("when select is called", function() {
				beforeEach(function () {
					this.selected = this.sut.select(10);
				});

				it("should return the first entry that has the lowest score", function() {
					expect(this.selected).toEqual(0);
				});
			});
		});

		describe("when Math.random always returns 0.99", function() {
			beforeEach(function() {
				this.rnd = Math.random;
				Math.random = function() {return 0.99;};
			});

			afterEach(function() {
				Math.random = this.rnd;
			});

			describe("when select is called", function() {
				beforeEach(function () {
					this.selected = this.sut.select(10);
				});

				it("should return the last entry that has the highest score", function() {
					expect(this.selected).toEqual(9);
				});
			});
		});
	});
});