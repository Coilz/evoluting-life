define(['display/countersCollector'], function(CountersCollector) {
	"use strict";

	describe("display.CountersCollector", function() {
		beforeEach(function() {

			var that = this;

			this.iteration = 0;
			this.iteratorGetter = function() {
				return that.iteration;
			};

			this.populationTotals = {
				size: 0,
				fittest: 0
			};
			this.populationTotalsGetter = function() {
				return that.populationTotals;
			};

			this.sut = new CountersCollector(this.iteratorGetter, this.populationTotalsGetter);
		});

		describe("when CountersCollector is initialized", function() {
			it("getCounters should return 0 for all counters", function() {
				var counters = this.sut.getCounters();

				for (var prop in counters) {
					expect(counters[prop]).toEqual(0);
				}
			});

			describe("when the observers are notified with a value twice", function() {
				beforeEach(function() {
					var that = this;
					this.notifyObservers = function(count, value) {
						while (count--) {
							that.sut.bornObserver.update(value);
							that.sut.mutatedObserver.update(value);
						}
					};

					this.notificationValue = 7;
					this.notifyObservers(2, this.notificationValue);
				});

				it("getCounters should return twice the value for the respectively counters", function() {
					var counters = this.sut.getCounters();
					var expectedValue = this.notificationValue * 2;

					expect(counters.born).toEqual(2);
					expect(counters.mutated).toEqual(expectedValue);
				});

				describe("when the observers are notified with the value again", function() {
					beforeEach(function() {
						this.notifyObservers(1, this.notificationValue);
					});

					it("getCounters should return three times the value for the respectively counters", function() {
						var counters = this.sut.getCounters();
						var expectedValue = this.notificationValue * 3;

						expect(counters.born).toEqual(3);
						expect(counters.mutated).toEqual(expectedValue);
					});
				});
			});
		});

		describe("when population counters have values", function() {
			beforeEach(function() {
				this.populationTotals.fittest = 7;
			});

			it("getCounters should return the values for populationCounters", function() {
				var counters = this.sut.getCounters();

				expect(counters.fittest).toEqual(7);
				expect(counters.highScore).toEqual(7);
			});

			describe("when fittest has a higher value then before", function() {
				beforeEach(function() {
					this.sut.getCounters();
					this.populationTotals.fittest = 11;
				});

				it("highScore should return the new value", function() {
					var counters = this.sut.getCounters();

					expect(counters.highScore).toEqual(11);
				});
			});

			describe("when fittest has a lower value then before", function() {
				beforeEach(function() {
					this.sut.getCounters();
					this.populationTotals.fittest = 5;
				});

				it("highScore should return the previous value", function() {
					var counters = this.sut.getCounters();

					expect(counters.highScore).toEqual(7);
				});
			});
		});

		describe("when iteration counter has a value", function() {
			beforeEach(function() {
				this.iteration = 3;
			});

			it("getCounters should return the value for the iteration counter", function() {
				var counters = this.sut.getCounters();
				var expectedValue = this.iteration;

				expect(counters.iteration).toEqual(expectedValue);
			});

			describe("when iteration counter is incremented with a value", function() {
				beforeEach(function() {
					this.iterationIncrement = 4;
					this.iteration += this.iterationIncrement;
				});

				it("getCounters should return the final value for the iteration counter", function() {
					var counters = this.sut.getCounters();
					var expectedValue = this.iteration;

					expect(counters.iteration).toEqual(expectedValue);
				});
			});
		});
	});
});
