define(['display/statsCollector'], function (StatsCollector) {
	"use strict";

	describe("display.StatsCollector", function () {
		beforeEach(function () {
			var that = this;

			this.populationTotals = {
				ageTotal: 0,
				gainedEnergyTotal: 0,
				usedEnergyTotal: 0,
				lostEnergyTotal: 0,
				netEnergyTotal: 0,
				healthTotal: 0,
				fittest: 0,
				size: 0
			};
			this.populationTotalsGetter = function () {
				return that.populationTotals;
			};

			this.sut = new StatsCollector(this.populationTotalsGetter);
		});

		describe("when StatsCollector is initialized", function () {
			it("getCounters should return 0 for all counters", function () {
				var counters = this.sut.getCounters();

				for (var prop in counters) {
					expect(counters[prop]).toEqual(0);
				}
			});

			describe("when the observers are notified with a value twice", function () {
				beforeEach(function () {
					var that = this;
					this.notifyObservers = function (count, value) {
						while (count--) {
							that.sut.starvedObserver.update(value);
							that.sut.wanderedObserver.update(value);
							that.sut.eatObserver.update(value);
							that.sut.consumedObserver.update(value);
							that.sut.diedOfAgeObserver.update(value);
						}
					};

					this.notificationValue = 7;
					this.notifyObservers(2, this.notificationValue);
				});

				it("getCounters should return twice the value for the respectively counters", function () {
					var counters = this.sut.getCounters();
					var expectedValue = this.notificationValue * 2;

					expect(counters.starved).toEqual(expectedValue);
					expect(counters.wandered).toEqual(expectedValue);
					expect(counters.eaten).toEqual(expectedValue);
					expect(counters.consumed).toEqual(expectedValue);
					expect(counters.natural).toEqual(expectedValue);
				});

				describe("when the observers are notified with the value again", function () {
					beforeEach(function () {
						this.sut.getCounters();
						this.notifyObservers(1, this.notificationValue);
					});

					it("getCounters should return once the value for the respectively counters", function () {
						var counters = this.sut.getCounters();
						var expectedValue = this.notificationValue;

						expect(counters.starved).toEqual(expectedValue);
						expect(counters.wandered).toEqual(expectedValue);
						expect(counters.eaten).toEqual(expectedValue);
						expect(counters.consumed).toEqual(expectedValue);
						expect(counters.natural).toEqual(expectedValue);
					});
				});
			});
		});

		describe("when population counters have values", function () {
			beforeEach(function () {
				this.populationTotals.ageTotal = 6;
				this.populationTotals.gainedEnergyTotal = 4;
				this.populationTotals.usedEnergyTotal = 8;
				this.populationTotals.lostEnergyTotal = 10;
				this.populationTotals.netEnergyTotal = 12;
				this.populationTotals.healthTotal = 14;
				this.populationTotals.size = 2;
				this.populationTotals.fittest = 7;
			});

			it("getCounters should return the average values for populationCounters", function () {
				var counters = this.sut.getCounters();

				expect(counters.ageAverage).toEqual(3);
				expect(counters.gainedEnergyAverage).toEqual(2);
				expect(counters.usedEnergyAverage).toEqual(4);
				expect(counters.lostEnergyAverage).toEqual(5);
				expect(counters.netEnergyAverage).toEqual(6);
				expect(counters.healthAverage).toEqual(7);
				expect(counters.fittest).toEqual(7);
			});
		});
	});
});