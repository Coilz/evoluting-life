define(['genetics/options'], function (geneticsOptions) {
	"use strict";

	describe("genetics.Genome", function () {
		beforeEach(function () {
			geneticsOptions.reset();
			geneticsOptions.genome.sensorGeneReplacementRate.set(0.0);
			geneticsOptions.genome.movementGeneReplacementRate.set(0.0);

			this.inputCount = 3;
			this.outputCount = 4;

			var that = this;
			var Mock = function (state) {
				return {
					mutate: function () { },
					clone: function () { },
					mate: function (partner) {
						return [{}, {}];
					},
					getState: function () {
						return { state: state };
					}
				};
			};
			this.brainGeneMock = new Mock('brain');
			this.lifeGeneMock = new Mock('life');
			this.sensorGeneMock = new Mock('sensor');
			this.movementGeneMock = new Mock('movement');

			this.brainGeneCtor = function () { return that.brainGeneMock; };
			this.lifeGeneCtor = function () { return that.lifeGeneMock; };
			this.sensorGeneCtor = function () { return that.sensorGeneMock; };
			this.movementGeneCtor = function () { return that.movementGeneMock; };

			define('brainGeneMock', [], function () {
				return that.brainGeneCtor;
			});
			define('lifeGeneMock', [], function () {
				return that.lifeGeneCtor;
			});
			define('sensorGeneMock', [], function () {
				return that.sensorGeneCtor;
			});
			define('movementGeneMock', [], function () {
				return that.movementGeneCtor;
			});

			require.undef('genetics/genome');
			require.config({
				map: {
					'genetics/genome': {
						'genetics/brainGene': 'brainGeneMock',
						'genetics/lifeGene': 'lifeGeneMock',
						'genetics/sensorGene': 'sensorGeneMock',
						'genetics/movementGene': 'movementGeneMock'
					}
				}
			});
		});

		afterEach(function () {
			require.undef('brainGeneMock');
			require.undef('lifeGeneMock');
			require.undef('sensorGeneMock');
			require.undef('movementGeneMock');
			require.config({
				map: {
					'genetics/genome': {
						'genetics/brainGene': 'genetics/brainGene',
						'genetics/lifeGene': 'genetics/lifeGene',
						'genetics/sensorGene': 'genetics/sensorGene',
						'genetics/movementGene': 'genetics/movementGene'
					}
				}
			});
		});

		describe("when a genome is created with inputCount and outputCount", function () {
			beforeEach(function (done) {
				spyOn(this, 'brainGeneCtor').and.callThrough();
				spyOn(this, 'lifeGeneCtor').and.callThrough();
				spyOn(this, 'sensorGeneCtor').and.callThrough();
				spyOn(this, 'movementGeneCtor').and.callThrough();

				var that = this;
				require(['genetics/genome'], function (Genome) {
					that.sut = new Genome(that.inputCount, that.outputCount);
					done();
				});
			});

			it("should have created a brainGene", function () {
				expect(this.sut.getBrain()).not.toBeUndefined();
				expect(this.sut.getBrain()).not.toBeNull();
			});

			it("should have created a lifeGene", function () {
				expect(this.sut.getLife()).not.toBeUndefined();
				expect(this.sut.getLife()).not.toBeNull();
			});

			it("should have created a sensorGene", function () {
				expect(this.sut.getSensor()).not.toBeUndefined();
				expect(this.sut.getSensor()).not.toBeNull();
			});

			it("should have created a movementGene", function () {
				expect(this.sut.getMovement()).not.toBeUndefined();
				expect(this.sut.getMovement()).not.toBeNull();
			});

			it("calls the BrainGene constructor method", function () {
				expect(this.brainGeneCtor.calls.count()).toEqual(1);
			});

			it("calls the BrainGene constructor method with inputCount and outputCount as parameter", function () {
				expect(this.brainGeneCtor.calls.allArgs()).toEqual([[this.inputCount, this.outputCount]]);
			});

			it("calls the LifeGene constructor method", function () {
				expect(this.lifeGeneCtor.calls.count()).toEqual(1);
			});

			it("calls the SensorGene constructor method", function () {
				expect(this.sensorGeneCtor.calls.count()).toEqual(1);
			});

			it("calls the MovementGene constructor method", function () {
				expect(this.movementGeneCtor.calls.count()).toEqual(1);
			});

			describe("when Math.random always returns 0.0", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.0; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when sensorGeneReplacementRate is 1.0", function () {
					beforeEach(function () {
						geneticsOptions.genome.sensorGeneReplacementRate.set(1.0);

						this.sut.mutate();
					});

					it("should have created a new sensorGene", function () {
						expect(this.sut.getSensor()).not.toBeUndefined();
						expect(this.sut.getSensor()).not.toBeNull();
					});

					it("calls the SensorGene constructor method", function () {
						expect(this.sensorGeneCtor.calls.count()).toEqual(2);
					});
				});

				describe("when movementGeneReplacementRate is 1.0", function () {
					beforeEach(function () {
						geneticsOptions.genome.movementGeneReplacementRate.set(1.0);

						this.sut.mutate();
					});

					it("should have created a new movementGene", function () {
						expect(this.sut.getMovement()).not.toBeUndefined();
						expect(this.sut.getMovement()).not.toBeNull();
					});

					it("calls the MovementGene constructor method", function () {
						expect(this.movementGeneCtor.calls.count()).toEqual(2);
					});
				});
			});

			describe("when state of the the genome is retrieved", function () {
				beforeEach(function () {
					this.geneState = this.sut.getState();
				});

				it("should return a defined representation of the genome", function () {
					expect(this.geneState).toBeDefined();
				});

				it("should return a representation of the genome that contains the brainGene", function () {
					expect(this.geneState.brain).toEqual(this.sut.getBrain().getState());
				});

				it("should return a representation of the genome that contains the serialized lifeGene", function () {
					expect(this.geneState.life).toEqual(this.sut.getLife().getState());
				});

				it("should return a representation of the genome that contains the serialized sensorGene", function () {
					expect(this.geneState.sensor).toEqual(this.sut.getSensor().getState());
				});

				it("should return a representation of the genome that contains the serialized movementGene", function () {
					expect(this.geneState.movement).toEqual(this.sut.getMovement().getState());
				});
			});

			describe("when the genome is cloned", function () {
				beforeEach(function () {
					this.clone = this.sut.clone();
				});

				it("the clone should not be the same object as the source", function () {
					expect(this.clone).not.toBe(this.sut);
				});

				it("calls the brainGene constructor method to clone the brain gene in the genome", function () {
					expect(this.brainGeneCtor.calls.count()).toEqual(2);
				});

				it("calls the LifeGene constructor method to clone the life gene in the genome", function () {
					expect(this.lifeGeneCtor.calls.count()).toEqual(2);
				});

				it("calls the SensorGene constructor method to clone the sensor gene in the genome", function () {
					expect(this.sensorGeneCtor.calls.count()).toEqual(2);
				});

				it("calls the MovementGene constructor method to clone the movement gene in the genome", function () {
					expect(this.movementGeneCtor.calls.count()).toEqual(2);
				});
			});

			describe("when the genome mutates", function () {
				beforeEach(function () {
					spyOn(this.brainGeneMock, 'mutate');
					spyOn(this.lifeGeneMock, 'mutate');
					spyOn(this.sensorGeneMock, 'mutate');
					spyOn(this.movementGeneMock, 'mutate');

					this.sut.mutate();
				});

				it("calls the mutate method for the brain gene in the genome", function () {
					expect(this.brainGeneMock.mutate.calls.count()).toEqual(1);
				});

				it("calls the mutate method for the life gene in the genome", function () {
					expect(this.lifeGeneMock.mutate.calls.count()).toEqual(1);
				});

				it("calls the mutate method for the sensor gene in the genome", function () {
					expect(this.sensorGeneMock.mutate.calls.count()).toEqual(1);
				});

				it("calls the mutate method for the movement gene in the genome", function () {
					expect(this.movementGeneMock.mutate.calls.count()).toEqual(1);
				});
			});

			describe("when the genome mates with a partner", function () {
				beforeEach(function (done) {
					this.brainChildren = [this.brainGeneMock, this.brainGeneMock];
					this.lifeChildren = [this.lifeGeneMock, this.lifeGeneMock];
					this.sensorChildren = [this.sensorGeneMock, this.sensorGeneMock];
					this.movementChildren = [this.movementGeneMock, this.movementGeneMock];

					spyOn(this.brainGeneMock, 'mate').and.returnValue(this.brainChildren);
					spyOn(this.lifeGeneMock, 'mate').and.returnValue(this.lifeChildren);
					spyOn(this.sensorGeneMock, 'mate').and.returnValue(this.sensorChildren);
					spyOn(this.movementGeneMock, 'mate').and.returnValue(this.movementChildren);

					var that = this;
					require(['genetics/genome'], function (Genome) {
						that.partner = new Genome(that.inputCount, that.outputCount);
						that.children = that.sut.mate(that.partner);
						done();
					});
				});

				it("should produce 2 children that are not the parents", function () {
					expect(this.children.length).toEqual(2);

					for (var i = 0; i < 2; i++) {
						expect(this.children[i]).not.toBe(this.sut);
						expect(this.children[i]).not.toBe(this.partner);
					}
				});

				it("should produce 2 children with a brainGene", function () {
					expect(this.children[0].getBrain()).toEqual(this.brainGeneMock);
					expect(this.children[1].getBrain()).toEqual(this.brainGeneMock);
				});

				it("should produce 2 children with a lifeGene", function () {
					expect(this.children[0].getLife()).toEqual(this.lifeGeneMock);
					expect(this.children[1].getLife()).toEqual(this.lifeGeneMock);
				});

				it("should produce 2 children with a sensorGene", function () {
					expect(this.children[0].getSensor()).toEqual(this.sensorGeneMock);
					expect(this.children[1].getSensor()).toEqual(this.sensorGeneMock);
				});

				it("should produce 2 children with a movementGene", function () {
					expect(this.children[0].getMovement()).toEqual(this.movementGeneMock);
					expect(this.children[1].getMovement()).toEqual(this.movementGeneMock);
				});

				it("calls the brainGene constructor method to create the child brainGenes", function () {
					expect(this.brainGeneCtor.calls.count()).toEqual(2 + 2);
				});

				it("calls the LifeGene constructor method to create the child lifeGenes", function () {
					expect(this.lifeGeneCtor.calls.count()).toEqual(2 + 2);
				});

				it("calls the SensorGene constructor method to create the child sensorGenes", function () {
					expect(this.sensorGeneCtor.calls.count()).toEqual(2 + 2);
				});

				it("calls the MovementGene constructor method to create the child movementGenes", function () {
					expect(this.movementGeneCtor.calls.count()).toEqual(2 + 2);
				});

				it("calls the mate method for the brain gene in the genome", function () {
					expect(this.brainGeneMock.mate.calls.count()).toEqual(1);
				});

				it("calls the mate method for the brain gene in the genome with the brain gene of the partner", function () {
					expect(this.brainGeneMock.mate.calls.argsFor(0)).toEqual([this.partner.getBrain()]);
				});

				it("calls the mate method for the life gene in the genome", function () {
					expect(this.lifeGeneMock.mate.calls.count()).toEqual(1);
				});

				it("calls the mate method for the sensor gene in the genome", function () {
					expect(this.sensorGeneMock.mate.calls.count()).toEqual(1);
				});

				it("calls the mate method for the sensor gene in the genome with the sensor gene of the partner", function () {
					expect(this.sensorGeneMock.mate.calls.argsFor(0)).toEqual([this.partner.getSensor()]);
				});

				it("calls the mate method for the movement gene in the genome", function () {
					expect(this.movementGeneMock.mate.calls.count()).toEqual(1);
				});

				it("calls the mate method for the movement gene in the genome with the movement gene of the partner", function () {
					expect(this.movementGeneMock.mate.calls.argsFor(0)).toEqual([this.partner.getMovement()]);
				});
			});
		});
	});
});