define(['genetics/options', 'genetics/axonGene'], function (geneticsOptions, AxonGene) {
	"use strict";

	describe("genetics.AxonGene", function () {
		beforeEach(function () {
			geneticsOptions.reset();

			var that = this;

			// Create a geneticsMock
			this.childrenMock = [{
        strength: "strength1",
        strengthening: "strengthening1",
        weakening: "weakening1"
			},
			{
        strength: "strength2",
        strengthening: "strengthening2",
        weakening: "weakening2"
			}];

			this.geneticsMock = {
				mate: function (a, b, createCallback) {
					var children = [];
					for (var i = 0; i < 2; i++) {
						children.push(createCallback(that.childrenMock[i]));
					}
					return children;
				}
			};

			define('geneticsMock', [], function () {
				return that.geneticsMock;
			});

			require.undef('genetics/axonGene');
			require.config({
				map: {
					'genetics/axonGene': {
						'genetics/genetics': 'geneticsMock'
					}
				}
			});
		});

		afterEach(function () {
			require.undef('geneticsMock');
			require.config({
				map: {
					'genetics/axonGene': {
						'genetics/genetics': 'genetics/genetics'
					}
				}
			});
		});

		describe("when an axonGene is created and maxStrength equal to 16", function () {
			beforeEach(function () {
				geneticsOptions.axon.maxStrength.set(16);
			});

			describe("when the state of the axonGene is requested", function () {
				beforeEach(function () {
					this.sut = new AxonGene();
					this.geneState = this.sut.getState();
				});

				it("should return a defined representation of the axonGene", function () {
					expect(this.geneState).toBeDefined();
				});

				it("should return a representation of the axonGene that contains the strength", function () {
					expect(this.geneState.strength).toEqual(this.sut.strength);
				});
			});

			describe("when Math.random always returns 0.5", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.5; };

					this.sut = new AxonGene();
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				it("the strength is in the middle of min and max (0)", function () {
					//expect(this.sut.strength).toEqual(8);
					expect(this.sut.strength).toEqual(0);
				});

				describe("when mutationFraction is 0.1", function () {
					beforeEach(function () {
						geneticsOptions.evolution.mutationFraction.set(0.1);
						geneticsOptions.axon.strengthMutationRate.set(1.0);
						this.sut.mutate();
					});

					it("mutate changes the strength to the middle of min and max (0)", function () {
					//expect(this.sut.strength).toEqual(8);
					expect(this.sut.strength).toEqual(0);
					});
				});
			});

			describe("when Math.random always returns 0.25", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.25; };

					this.sut = new AxonGene();
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				it("the strength is at a 1/4 of min and max (-8)", function () {
					//expect(this.sut.strength).toEqual(4);
					expect(this.sut.strength).toEqual(-8);
				});

				describe("when mutationFraction is 0.1", function () {
					beforeEach(function () {
						geneticsOptions.evolution.mutationFraction.set(0.1);
						geneticsOptions.axon.strengthMutationRate.set(1.0);
						this.sut.mutate();
					});

					it("mutate changes the strength with -0.05 of min and max () to -9.6", function () {
						//expect(this.sut.strength).toEqual(3.2);
						expect(this.sut.strength).toEqual(-9.6);
					});
				});

				describe("when mutationRate is less than the result of Math.random", function () {
					beforeEach(function () {
						geneticsOptions.axon.strengthMutationRate.set(0.24);
						this.sut.mutate();
					});

					it("mutate does not change the strength", function () {
					//expect(this.sut.strength).toEqual(4);
					expect(this.sut.strength).toEqual(-8);
					});
				});
			});

			describe("when Math.random always returns 0.75", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.75; };

					this.sut = new AxonGene();
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				it("the strength is at 3/4 of min and max (8)", function () {
					//expect(this.sut.strength).toEqual(12);
					expect(this.sut.strength).toEqual(8);
				});

				describe("when mutationFraction is 0.1", function () {
					beforeEach(function () {
						geneticsOptions.evolution.mutationFraction.set(0.1);
						geneticsOptions.axon.strengthMutationRate.set(1.0);
						this.sut.mutate();
					});

					it("mutate changes the strength with 0.05 of min and max () to 9.6", function () {
						//expect(this.sut.strength).toEqual(12.8);
						var rounded = Math.round( this.sut.strength * 10) / 10
						expect(rounded).toEqual(9.6);
					});
				});

				describe("when mutationRate is less than the result of Math.random", function () {
					beforeEach(function () {
						geneticsOptions.axon.strengthMutationRate.set(0.74);
						this.sut.mutate();
					});

					it("mutate does not change the strength", function () {
					//expect(this.sut.strength).toEqual(12);
					expect(this.sut.strength).toEqual(8);
					});
				});
			});

			describe("when the axon is cloned", function () {
				beforeEach(function () {
					this.sut = new AxonGene();
					this.clone = this.sut.clone();
				});

				it("the clone should not be the same object as the source", function () {
					expect(this.clone).not.toBe(this.sut);
				});

				it("the clone should have the same strength", function () {
					expect(this.clone.strength).toEqual(this.sut.strength);
				});

				describe("when the properties of the clone are changed", function () {
					beforeEach(function () {
						this.clone.strength += 1;
					});

					it("the change doesn't influence the original", function () {
						expect(this.sut.strength).not.toBe(this.clone.strength);
						expect(this.sut.strength).not.toEqual(this.clone.strength);
					});
				});
			});

			describe("when two AxonGenes are created", function () {
				beforeEach(function (done) {
					var that = this;
					require(['genetics/axonGene'], function (AxonGene) {
						that.sut = new AxonGene();
						that.partner = new AxonGene();
						done();
					});
				});

				describe("when they mate with eachother", function () {
					beforeEach(function () {
						spyOn(this.geneticsMock, 'mate').and.callThrough();
						this.children = this.sut.mate(this.partner);
					});

					it("should produce 2 children that are not the parents", function () {
						expect(this.children.length).toEqual(2);

						for (var i = 0; i < 2; i++) {
							expect(this.children[i]).not.toBe(this.sut);
							expect(this.children[i]).not.toBe(this.partner);
						}
					});

					it("should return the children that were created by genetics.mate", function () {
						for (var i=0; i<2; i++) {
							expect(this.children[i].strength).toBe(this.childrenMock[i].strength);
							expect(this.children[i].strengthening).toBe(this.childrenMock[i].strengthening);
							expect(this.children[i].weakening).toBe(this.childrenMock[i].weakening);
						}
					});

					it("calls the genetics.mate method 1 time", function () {
						expect(this.geneticsMock.mate.calls.count()).toEqual(1);
					});

					it("calls the genetics.mate method with the sot, the partner and a callback function", function () {
						expect(this.geneticsMock.mate).toHaveBeenCalledWith(this.sut, this.partner, jasmine.any(Function));
					});
				});
			});
		});
	});
});