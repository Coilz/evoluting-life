define(['genetics/options', 'genetics/sensorGene'], function (geneticsOptions, SensorGene) {
    "use strict";

    describe("genetics.SensorGene", function () {
        beforeEach(function () {
            geneticsOptions.reset();
            geneticsOptions.sensor.viewDistanceReplacementRate.set(0.0);
            geneticsOptions.sensor.fieldOfViewReplacementRate.set(0.0);

            var that = this;

            // Create a geneticsMock
            this.childrenMock = [{
                viewDistance: "viewDistance1",
                fieldOfView: "fieldOfView1"
            },
            {
                viewDistance: "viewDistance2",
                fieldOfView: "fieldOfView2"
            }];

            this.geneticsMock = {
                mate: function (a, b, createCallback) {
                    var children = [];
                    for (var i = 0; i < 2; i++) {
                        children.push(createCallback(that.childrenMock[i]));
                    }
                    return children;
                }
            };

            define('geneticsMock', [], function () {
                return that.geneticsMock;
            });

            require.undef('genetics/sensorGene');
            require.config({
                map: {
                    'genetics/sensorGene': {
                        'genetics/genetics': 'geneticsMock'
                    }
                }
            });
        });

        afterEach(function () {
            require.undef('geneticsMock');
            require.config({
                map: {
                    'genetics/sensorGene': {
                        'genetics/genetics': 'genetics/genetics'
                    }
                }
            });
        });

        describe("when minViewDistance is 30, maxViewDistance is 70, minFieldOfView is 1, maxFieldOfView is 7", function () {
            beforeEach(function () {
                geneticsOptions.sensor.minViewDistance.set(30);
                geneticsOptions.sensor.maxViewDistance.set(70);
                geneticsOptions.sensor.minFieldOfView.set(1);
                geneticsOptions.sensor.maxFieldOfView.set(7);
            });

            describe("when a SensorGene is created", function () {
                beforeEach(function () {
                    this.sut = new SensorGene();
                });

                describe("when Math.random always returns 0.5", function () {
                    beforeEach(function () {
                        this.rnd = Math.random;
                        Math.random = function () { return 0.5; };
                    });

                    afterEach(function () {
                        Math.random = this.rnd;
                    });

                    describe("when viewDistanceReplacementRate is 1.0", function () {
                        beforeEach(function () {
                            geneticsOptions.sensor.viewDistanceReplacementRate.set(1.0);

                            this.sut.viewDistance = 31;
                            this.sut.mutate();
                        });

                        it("mutate changes viewDistance to 50", function () {
                            expect(this.sut.viewDistance).toEqual(50);
                        });
                    });

                    describe("when fieldOfViewReplacementRate is 1.0", function () {
                        beforeEach(function () {
                            geneticsOptions.sensor.fieldOfViewReplacementRate.set(1.0);

                            this.sut.fieldOfView = 1;
                            this.sut.mutate();
                        });

                        it("mutate changes fieldOfView to 4", function () {
                            expect(this.sut.fieldOfView).toEqual(4);
                        });
                    });
                });
            });

            describe("when the state of the SensorGene is requested", function () {
                beforeEach(function () {
                    this.sut = new SensorGene();
                    this.geneState = this.sut.getState();
                });

                it("should return a defined representation of the SensorGene", function () {
                    expect(this.geneState).toBeDefined();
                });

                it("should return a representation of the SensorGene that contains the viewDistance", function () {
                    expect(this.geneState.viewDistance).toEqual(this.sut.viewDistance);
                });

                it("should return a representation of the SensorGene that contains the fieldOfView", function () {
                    expect(this.geneState.fieldOfView).toEqual(this.sut.fieldOfView);
                });
            });

            describe("when Math.random always returns 0.5", function () {
                beforeEach(function () {
                    this.rnd = Math.random;
                    Math.random = function () { return 0.5; };
                });

                afterEach(function () {
                    Math.random = this.rnd;
                });

                describe("when a SensorGene is created", function () {
                    beforeEach(function () {
                        this.sut = new SensorGene();
                    });

                    it("the viewDistance is 50", function () {
                        expect(this.sut.viewDistance).toEqual(50);
                    });

                    it("the fieldOfView is 4", function () {
                        expect(this.sut.fieldOfView).toEqual(4);
                    });

                    describe("when mutationFraction is 0.1, viewDistanceMutationRate is 1.0, fieldOfViewMutationRate is 1.0", function () {
                        beforeEach(function () {
                            geneticsOptions.evolution.mutationFraction.set(0.1);

                            geneticsOptions.sensor.viewDistanceMutationRate.set(1.0);
                            geneticsOptions.sensor.fieldOfViewMutationRate.set(1.0);
                            this.sut.mutate();
                        });

                        it("mutate does not change viewDistance", function () {
                            expect(this.sut.viewDistance).toEqual(50);
                        });

                        it("mutate does not change fieldOfView", function () {
                            expect(this.sut.fieldOfView).toEqual(4);
                        });
                    });

                    describe("when viewDistanceMutationRate, fieldOfViewMutationRate are less than the result of Math.random", function () {
                        beforeEach(function () {
                            geneticsOptions.sensor.viewDistanceMutationRate.set(0.49);
                            geneticsOptions.sensor.fieldOfViewMutationRate.set(0.49);
                            this.sut.mutate();
                        });

                        it("mutate does not change viewDistance", function () {
                            expect(this.sut.viewDistance).toEqual(50);
                        });

                        it("mutate does not change fieldOfView", function () {
                            expect(this.sut.fieldOfView).toEqual(4);
                        });
                    });
                });
            });

            describe("when Math.random always returns 0.25", function () {
                beforeEach(function () {
                    this.rnd = Math.random;
                    Math.random = function () { return 0.25; };
                });

                afterEach(function () {
                    Math.random = this.rnd;
                });

                describe("when a SensorGene is created", function () {
                    beforeEach(function () {
                        this.sut = new SensorGene();
                    });

                    it("the viewDistance is 40", function () {
                        expect(this.sut.viewDistance).toEqual(40);
                    });

                    it("the fieldOfView is 2.5", function () {
                        expect(this.sut.fieldOfView).toEqual(2.5);
                    });

                    describe("when mutationFraction is 0.1, viewDistanceMutationRate is 1.0, fieldOfViewMutationRate is 1.0", function () {
                        beforeEach(function () {
                            geneticsOptions.evolution.mutationFraction.set(0.1);

                            geneticsOptions.sensor.viewDistanceMutationRate.set(1.0);
                            geneticsOptions.sensor.fieldOfViewMutationRate.set(1.0);
                            this.sut.mutate();
                        });

                        it("mutate changes viewDistance to 38", function () {
                            expect(this.sut.viewDistance).toEqual(38);
                        });

                        it("mutate changes fieldOfView to 2.2", function () {
                            expect(this.sut.fieldOfView).toEqual(2.2);
                        });
                    });

                    describe("when viewDistanceMutationRate, fieldOfViewMutationRate are less than the result of Math.random", function () {
                        beforeEach(function () {
                            geneticsOptions.sensor.viewDistanceMutationRate.set(0.24);
                            geneticsOptions.sensor.fieldOfViewMutationRate.set(0.24);
                            this.sut.mutate();
                        });

                        it("mutate does not change viewDistance", function () {
                            expect(this.sut.viewDistance).toEqual(40);
                        });

                        it("mutate does not change fieldOfView", function () {
                            expect(this.sut.fieldOfView).toEqual(2.5);
                        });
                    });
                });
            });

            describe("when Math.random always returns 0.75", function () {
                beforeEach(function () {
                    this.rnd = Math.random;
                    Math.random = function () { return 0.75; };
                });

                afterEach(function () {
                    Math.random = this.rnd;
                });

                describe("when a SensorGene is created", function () {
                    beforeEach(function () {
                        this.sut = new SensorGene();
                    });

                    it("the viewDistance is 60", function () {
                        expect(this.sut.viewDistance).toEqual(60);
                    });

                    it("the fieldOfView is 5.5", function () {
                        expect(this.sut.fieldOfView).toEqual(5.5);
                    });

                    describe("when mutationFraction is 0.1, viewDistanceMutationRate is 1.0, fieldOfViewMutationRate is 1.0", function () {
                        beforeEach(function () {
                            geneticsOptions.evolution.mutationFraction.set(0.1);

                            geneticsOptions.sensor.viewDistanceMutationRate.set(1.0);
                            geneticsOptions.sensor.fieldOfViewMutationRate.set(1.0);
                            this.sut.mutate();
                        });

                        it("mutate changes viewDistance to 62", function () {
                            expect(this.sut.viewDistance).toEqual(62);
                        });

                        it("mutate changes fieldOfView to 5.8", function () {
                            expect(this.sut.fieldOfView).toEqual(5.8);
                        });
                    });

                    describe("when viewDistanceMutationRate, fieldOfViewMutationRate are less than the result of Math.random", function () {
                        beforeEach(function () {
                            geneticsOptions.sensor.viewDistanceMutationRate.set(0.74);
                            geneticsOptions.sensor.fieldOfViewMutationRate.set(0.74);
                            this.sut.mutate();
                        });

                        it("mutate does not change viewDistance", function () {
                            expect(this.sut.viewDistance).toEqual(60);
                        });

                        it("mutate does not change fieldOfView", function () {
                            expect(this.sut.fieldOfView).toEqual(5.5);
                        });
                    });
                });
            });

            describe("when the sensor is cloned", function () {
                beforeEach(function () {
                    this.sut = new SensorGene();
                    this.clone = this.sut.clone();
                });

                it("the clone should not be the same object as the source", function () {
                    expect(this.clone).not.toBe(this.sut);
                });

                it("the clone should have the same viewDistance", function () {
                    expect(this.clone.viewDistance).toEqual(this.sut.viewDistance);
                });

                it("the clone should have the same fieldOfView", function () {
                    expect(this.clone.fieldOfView).toEqual(this.sut.fieldOfView);
                });

                describe("when the properties of the clone are changed", function () {
                    beforeEach(function () {
                        this.clone.viewDistance += 1;
                        this.clone.fieldOfView += 0.1;
                    });

                    it("the change doesn't influence the original", function () {
                        expect(this.sut.viewDistance).not.toBe(this.clone.viewDistance);
                        expect(this.sut.viewDistance).not.toEqual(this.clone.viewDistance);

                        expect(this.sut.fieldOfView).not.toBe(this.clone.fieldOfView);
                        expect(this.sut.fieldOfView).not.toEqual(this.clone.fieldOfView);
                    });
                });
            });

            describe("when two SensorGenes are created", function () {
                beforeEach(function (done) {
                    var that = this;
                    require(['genetics/sensorGene'], function (SensorGene) {
                        that.sut = new SensorGene();
                        that.partner = new SensorGene();
                        done();
                    });
                });

                describe("when they mate with eachother", function () {
                    beforeEach(function () {
                        spyOn(this.geneticsMock, 'mate').and.callThrough();
                        this.children = this.sut.mate(this.partner);
                    });

                    it("should produce 2 children that are not the parents", function () {
                        expect(this.children.length).toEqual(2);

                        for (var i = 0; i < 2; i++) {
                            expect(this.children[i]).not.toBe(this.sut);
                            expect(this.children[i]).not.toBe(this.partner);
                        }
                    });

                    it("should return the children that were created by genetics.mate", function () {
						for (var i=0; i<2; i++) {
                            expect(this.children[i].viewDistance).toBe(this.childrenMock[i].viewDistance);
                            expect(this.children[i].fieldOfView).toBe(this.childrenMock[i].fieldOfView);
                        }
                    });

                    it("calls the genetics.mate method 1 time", function () {
                        expect(this.geneticsMock.mate.calls.count()).toEqual(1);
                    });

                    it("calls the genetics.mate method with the sot, the partner and a callback function", function () {
                        expect(this.geneticsMock.mate).toHaveBeenCalledWith(this.sut, this.partner, jasmine.any(Function));
                    });
                });
            });
        });
    });
});
