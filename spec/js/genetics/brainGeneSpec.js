define(['genetics/options', 'genetics/brainGene'], function(geneticsOptions, BrainGene) {
	"use strict";

	describe("genetics.BrainGene", function() {
		beforeEach(function() {
			geneticsOptions.reset();

			this.inputCount = 3;
			this.outputCount = 4;

			this.childrenMock = [{}, {}];

			var that = this;
			var Mock = function () {
				return {
					mutate: function () {},
					clone: function () {},
					mate: function (partner) {
						return that.childrenMock;
					},
					getState: function () {
						return {};
					}
				};
			};
			this.neuronGeneMock = new Mock();
			this.neuronGeneCtor = function() {
				return that.neuronGeneMock;
			};
			define('neuronGeneMock', [], function () {
				return that.neuronGeneCtor;
			});

			require.undef('genetics/brainGene');
			require.config({
				map: {
					'genetics/brainGene': {
						'genetics/neuronGene': 'neuronGeneMock'
					}
				}
			});
		});

		afterEach(function() {
			require.undef('neuronGeneMock');
			require.config({
				map: {
					'genetics/brainGene': {
						'genetics/neuronGene': 'genetics/neuronGene'
					}
				}
			});
		});

		describe("when a brainGene is created with inputCount and outputCount", function() {
			beforeEach(function(done) {
				spyOn(this, 'neuronGeneCtor').and.callThrough();

				var that = this;
				require(['genetics/brainGene'], function(BrainGene) {
					that.sut = new BrainGene(that.inputCount, that.outputCount);
					done();
				});
			});

			it("should have created at least the 2 layers of genes for all input and output names", function() {
				expect(this.sut.getLayers().length).not.toBeLessThan(2);
			});

			it("should have created the correct number of genes for the input layer", function() {
				expect(this.sut.getLayers()[this.sut.getLayers().length-1].length).toEqual(this.inputCount);
			});

			it("should have created the correct number of genes for the output layer", function() {
				expect(this.sut.getLayers()[0].length).toEqual(this.outputCount);
			});

			describe("when the state of the brainGene is requested", function () {
				beforeEach(function() {
					this.geneState = this.sut.getState();
				});

				it("should return a defined representation of the brainGene", function () {
					expect(this.geneState).toBeDefined();
				});

				it("should return a representation of the brainGene that contains the layers", function () {
					expect(this.geneState.layers.length).toEqual(this.sut.getLayers().length);
					for (var i=0; i<this.sut.getLayers().length; i++) {
						expect(this.geneState.layers[i].length).toEqual(this.sut.getLayers()[i].length);
						for (var j=0; j<this.sut.getLayers()[i].length; j++) {
							expect(this.geneState.layers[i][j]).toEqual(this.sut.getLayers()[i][j].getState());
						}
					}
				});
			});

			describe("when the maxHiddenLayers is set to zero", function() {
				beforeEach(function(done) {
					this.neuronGeneCtor.calls.reset();

					geneticsOptions.brain.minHiddenLayers.set(0);
					geneticsOptions.brain.maxHiddenLayers.set(0);

					var that = this;
					require(['genetics/brainGene'], function(BrainGene) {
						that.sut = new BrainGene(that.inputCount, that.outputCount);
						done();
					});
				});

				it("should have created at most the number of layers for the input and output names", function() {
					expect(this.sut.getLayers().length).not.toBeGreaterThan(2);
				});

				it("calls the createNeuronGene method this.outputCount times", function() {
					expect(this.neuronGeneCtor.calls.count()).toEqual(this.inputCount + this.outputCount);
				});

				it("calls the createNeuronGene method with maxOutputs as parameter", function() {
					expect(this.neuronGeneCtor.calls.allArgs()).toEqual([[0],[0],[0],[0],[4],[4],[4]]);
				});
			});

			describe("when the minHiddenLayers is set to the value of maxHiddenLayers (1)", function() {
				beforeEach(function(done) {
					geneticsOptions.brain.minHiddenLayers.set(1);
					geneticsOptions.brain.maxHiddenLayers.set(1);

					var that = this;
					require(['genetics/brainGene'], function(BrainGene) {
						that.sut = new BrainGene(that.inputCount, that.outputCount);
						done();
					});
				});

				it("should have created 1 hidden layer", function() {
					expect(this.sut.getLayers().length).toEqual(3);
				});

				it("should have created at least the max number of genes of the outer layers for each hidden layer", function() {
					expect(this.sut.getLayers()[1].length).not.toBeLessThan(this.inputCount);
					expect(this.sut.getLayers()[1].length).not.toBeLessThan(this.outputCount);
				});

				it("should have created at most the number of genes for all input and output names", function() {
					var maximum = geneticsOptions.brain.maxNeuronsPerLayer.get();
					expect(this.sut.getLayers()[1].length).not.toBeGreaterThan(maximum);
				});

				describe("when maxNeuronsPerLayer is 8 and Math.random always returns 0.5", function() {
					beforeEach(function(done) {
						this.neuronGeneCtor.calls.reset();

						geneticsOptions.brain.minHiddenLayers.set(1);
						geneticsOptions.brain.maxHiddenLayers.set(1);
						geneticsOptions.brain.maxNeuronsPerLayer.set(10);

						this.rnd = Math.random;
						Math.random = function() {return 0.5;};

						var that = this;
						require(['genetics/brainGene'], function(BrainGene) {
							that.sut = new BrainGene(that.inputCount, that.outputCount);
							done();
						});
					});

					afterEach(function() {
						Math.random = this.rnd;
					});

					it("calls the createNeuronGene method 3 + 7 + 4 times", function() {
						expect(this.neuronGeneCtor.calls.count()).toEqual(this.inputCount + 7 + this.outputCount);
					});

					it("calls the createNeuronGene method with the parameters", function() {
						expect(this.neuronGeneCtor.calls.allArgs()).toEqual([[0],[0],[0],[0], [4],[4],[4],[4], [4],[4],[4], [7],[7],[7]]);
					});
				});
			});

			describe("when minHiddenLayers is 1, maxHiddenLayers is 5 and maxNeuronsPerLayer is 8", function() {
				beforeEach(function(done) {
					geneticsOptions.brain.minHiddenLayers.set(1);
					geneticsOptions.brain.maxHiddenLayers.set(5);
					geneticsOptions.brain.maxNeuronsPerLayer.set(8);

					var that = this;
					require(['genetics/brainGene'], function(BrainGene) {
						that.sut = new BrainGene(that.inputCount, that.outputCount);
						done();
					});
				});

				describe("when neuronReplacementRate is 1.0 and the brainGene mutates", function() {
					beforeEach(function() {
						geneticsOptions.brain.neuronReplacementRate.set(1.0);
						this.neuronGeneCtor.calls.reset();
						this.geneCount = 0;

						for (var i=0; i<this.sut.getLayers().length; i++) {
							this.geneCount += this.sut.getLayers()[i].length;
						}

						this.sut.mutate();
					});

					it("calls the createNeuronGene method for each gene in the brainGene", function() {
						expect(this.neuronGeneCtor.calls.count()).toEqual(this.geneCount);
					});

					it("calls the createNeuronGene method with the parameters", function() {
						var parameters = [];
						var lastValue = 0;
						for (var i=0; i<this.sut.getLayers().length; i++) {
							var l=this.sut.getLayers()[i].length;
							for (var j=0; j<l; j++) {
								parameters.push([lastValue]);
							}
							lastValue = l;
						}

						expect(this.neuronGeneCtor.calls.allArgs()).toEqual(parameters);
					});
				});

				describe("when neuronReplacementRate is 1.0, neuronMutationRate is 1.0 and the brainGene mutates", function() {
					beforeEach(function() {
						geneticsOptions.brain.neuronReplacementRate.set(1.0);
						geneticsOptions.brain.neuronMutationRate.set(1.0);
						this.neuronGeneCtor.calls.reset();
						spyOn(this.neuronGeneMock, 'mutate');
						this.geneCount = 0;

						for (var i=0; i<this.sut.getLayers().length; i++) {
							this.geneCount += this.sut.getLayers()[i].length;
						}

						this.sut.mutate();
					});

					it("does not call the gene.mutate", function() {
						expect(this.neuronGeneMock.mutate.calls.count()).toEqual(0);
					});

					it("calls the createNeuronGene method for each gene in the brainGene", function() {
						expect(this.neuronGeneCtor.calls.count()).toEqual(this.geneCount);
					});

					it("calls the createNeuronGene method with the parameters", function() {
						var parameters = [];
						var lastValue = 0;
						for (var i=0; i<this.sut.getLayers().length; i++) {
							var l=this.sut.getLayers()[i].length;
							for (var j=0; j<l; j++) {
								parameters.push([lastValue]);
							}
							lastValue = l;
						}

						expect(this.neuronGeneCtor.calls.allArgs()).toEqual(parameters);
					});
				});

				describe("when neuronReplacementRate is 0.0, neuronMutationRate is 1.0 and the brainGene mutates", function() {
					beforeEach(function() {
						geneticsOptions.brain.neuronReplacementRate.set(0.0);
						geneticsOptions.brain.neuronMutationRate.set(1.0);
						spyOn(this.neuronGeneMock, 'mutate');
						this.geneCount = 0;

						for (var i=0; i<this.sut.getLayers().length; i++) {
							this.geneCount += this.sut.getLayers()[i].length;
						}

						this.sut.mutate();
					});

					it("calls the gene.mutate method for each gene in the brainGene", function() {
						expect(this.neuronGeneMock.mutate.calls.count()).toEqual(this.geneCount);
					});
				});

				describe("when Math.random always returns 0.5", function() {
					beforeEach(function(done) {
						this.neuronGeneCtor.calls.reset();

						this.rnd = Math.random;
						Math.random = function() {return 0.5;};

						var that = this;
						require(['genetics/brainGene'], function(BrainGene) {
							that.sut = new BrainGene(that.inputCount, that.outputCount);
							done();
						});
					});

					afterEach(function() {
						Math.random = this.rnd;
					});

					it("should have created 5 layers", function() {
						expect(this.sut.getLayers().length).toEqual(5);
					});

					it("should have created 6 genes for each of the 3 hidden layers", function() {
						expect(this.sut.getLayers()[1].length).toEqual(6);
						expect(this.sut.getLayers()[2].length).toEqual(6);
						expect(this.sut.getLayers()[3].length).toEqual(6);
					});

					it("calls the createNeuronGene method 3 + 6*3 + 4 times", function() {
						expect(this.neuronGeneCtor.calls.count()).toEqual(this.inputCount + 6*3 + this.outputCount);
					});

					it("calls the createNeuronGene method with the parameters", function() {
						expect(this.neuronGeneCtor.calls.allArgs()).toEqual([[0],[0],[0],[0], [4],[4],[4],[4],[4],[4], [6],[6],[6],[6],[6],[6], [6],[6],[6],[6],[6],[6], [6],[6],[6]]);
					});
				});

				describe("when Math.random always returns 0.25", function() {
					beforeEach(function(done) {
						this.rnd = Math.random;
						Math.random = function() {return 0.25;};

						var that = this;
						require(['genetics/brainGene'], function(BrainGene) {
							that.sut = new BrainGene(that.inputCount, that.outputCount);
							done();
						});
					});

					afterEach(function() {
						Math.random = this.rnd;
					});

					it("should have created 4 layers", function() {
						expect(this.sut.getLayers().length).toEqual(4);
					});

					it("should have created 5 genes for each of the 2 hidden layers", function() {
						expect(this.sut.getLayers()[1].length).toEqual(5);
						expect(this.sut.getLayers()[2].length).toEqual(5);
					});
				});
			});

			describe("when the brainGene is cloned", function() {
				beforeEach(function() {
					this.geneCount = 0;

					for (var i=0; i<this.sut.getLayers().length; i++) {
						this.geneCount += this.sut.getLayers()[i].length;
					}

					this.clone = this.sut.clone();
				});

				it("the clone should not be the same object as the source", function() {
					expect(this.clone).not.toBe(this.sut);
				});

				it("should create a clone with the same number of layers", function() {
					expect(this.clone.getLayers().length).toEqual(this.sut.getLayers().length);
				});

				it("should create a clone with the same number of genes for each layer", function() {
					var sotLayers = this.sut.getLayers();
					var cloneLayers = this.clone.getLayers();

					for (var i=0; i<sotLayers.length; i++) {
						expect(cloneLayers[i].length).toEqual(sotLayers[i].length);
					}
				});

				it("calls the NeuronGene constructor method for all neuronGenes", function() {
					expect(this.neuronGeneCtor.calls.count()).toEqual(this.geneCount * 2); // Once for the original and once for the clone
				});

				it("should create layers that are different objects", function() {
					var sutLayers = this.sut.getLayers();
					var cloneLayers = this.clone.getLayers();

					expect(cloneLayers).not.toBe(sutLayers);

					for (var i1=0; i1<sutLayers.length; i1++) {
						for (var i2=0; i2<cloneLayers.length; i2++) {
								expect(cloneLayers[i2]).not.toBe(sutLayers[i1]);
						}
					}
				});
			});
		});

		describe("when two BrainGenes with the same dimensions are created", function() {
			beforeEach(function(done) {
				geneticsOptions.brain.minHiddenLayers.set(3);
				geneticsOptions.brain.maxHiddenLayers.set(3);
				geneticsOptions.brain.maxNeuronsPerLayer.set(Math.max(this.inputCount, this.outputCount));

				var that = this;
				require(['genetics/brainGene'], function(BrainGene) {
					that.sut = new BrainGene(that.inputCount, that.outputCount);
					that.partner = new BrainGene(that.inputCount, that.outputCount);
					done();
				});
			});

			describe("when the genes mate", function() {
				beforeEach(function() {
					this.geneCount = 0;

					for (var i=0; i<this.sut.getLayers().length; i++) {
						this.geneCount += this.sut.getLayers()[i].length;
					}

					spyOn(this.neuronGeneMock, 'mate').and.callThrough();
					this.children = this.sut.mate(this.partner);
				});

				it("should produce 2 children that are not the parents", function() {
					expect(this.children.length).toEqual(2);

					for (var i=0; i<2; i++) {
						expect(this.children[i]).not.toBe(this.sut);
						expect(this.children[i]).not.toBe(this.partner);
					}
				});

				it("should produces children that have layers containing the children of the neurons", function() {
					for (var i=0; i<2; i++) {
						expect(this.children[i].getLayers().length).toEqual(this.sut.getLayers().length);
						for (var j=0; j<this.children[i].getLayers().length; j++) {
							expect(this.children[i].getLayers()[j].length).toEqual(this.sut.getLayers()[j].length);
							for (var k=0; k<this.children[i].getLayers()[j].length; k++) {
								expect(this.children[i].getLayers()[j][k]).toBe(this.childrenMock[i]);
							}
						}
					}
				});

				it("calls the genetics.mate method this.geneCount times", function() {
					expect(this.neuronGeneMock.mate.calls.count()).toEqual(this.geneCount);
				});
			});
		});

		describe("when two BrainGenes with different dimensions are created", function() {
			beforeEach(function(done) {
				var that = this;
				require(['genetics/brainGene'], function(BrainGene) {
					geneticsOptions.brain.minHiddenLayers.set(3);
					geneticsOptions.brain.maxHiddenLayers.set(3);
					geneticsOptions.brain.maxNeuronsPerLayer.set(Math.max(that.inputCount, that.outputCount));
					that.sut = new BrainGene(that.inputCount, that.outputCount);

					geneticsOptions.brain.minHiddenLayers.set(2);
					geneticsOptions.brain.maxHiddenLayers.set(2);
					geneticsOptions.brain.maxNeuronsPerLayer.set(Math.max(that.inputCount, that.outputCount) + 1);
					that.partner = new BrainGene(that.inputCount, that.outputCount);

					done();
				});
			});

			describe("when Math.random always returns 0.0", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.0;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when the genes mate", function() {
					beforeEach(function() {
						this.sutClone = {};
						this.partnerClone = {};

						spyOn(this.sut, 'clone').and.returnValue(this.sutClone);
						spyOn(this.partner, 'clone').and.returnValue(this.partnerClone);

						this.children = this.sut.mate(this.partner);
					});

					it("should produce 2 children that are not the parents", function() {
						expect(this.children.length).toEqual(2);

						for (var i=0; i<2; i++) {
							expect(this.children[i]).not.toBe(this.sut);
							expect(this.children[i]).not.toBe(this.partner);
						}
					});

					it("should create clones of themselves as children", function() {
						expect(this.sut.clone.calls.count()).toEqual(1);
						expect(this.partner.clone.calls.count()).toEqual(1);

						expect(this.children[0]).toBe(this.sutClone);
						expect(this.children[1]).toBe(this.partnerClone);
					});
				});
			});

			describe("when Math.random always returns 0.999", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.999;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when the genes mate", function() {
					beforeEach(function() {
						this.sutClone = {};
						this.partnerClone = {};

						spyOn(this.sut, 'clone').and.returnValue(this.sutClone);
						spyOn(this.partner, 'clone').and.returnValue(this.partnerClone);

						this.children = this.sut.mate(this.partner);
					});

					it("should produce 2 children that are not the parents", function() {
						expect(this.children.length).toEqual(2);

						for (var i=0; i<2; i++) {
							expect(this.children[i]).not.toBe(this.sut);
							expect(this.children[i]).not.toBe(this.partner);
						}
					});

					it("should create clones of themselves as children", function() {
						expect(this.sut.clone.calls.count()).toEqual(1);
						expect(this.partner.clone.calls.count()).toEqual(1);

						expect(this.children[0]).toBe(this.partnerClone);
						expect(this.children[1]).toBe(this.sutClone);
					});
				});
			});
		});
	});
});
