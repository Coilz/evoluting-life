define(['genetics/options', 'genetics/movementGene'], function (geneticsOptions, MovementGene) {
	"use strict";

	describe("genetics.MovementGene", function () {
		beforeEach(function () {
			geneticsOptions.reset();
			geneticsOptions.movement.angularForceReplacementRate.set(0.0);
			geneticsOptions.movement.linearForceReplacementRate.set(0.0);

			var that = this;

			// Create a geneticsMock
			this.childrenMock = [{
				angularForce: "angularForce1",
				linearForce: "linearForce1"
			},
			{
				angularForce: "angularForce2",
				linearForce: "linearForce2"
			}];

			this.geneticsMock = {
				mate: function (a, b, createCallback) {
					var children = [];
					for (var i = 0; i < 2; i++) {
						children.push(createCallback(that.childrenMock[i]));
					}
					return children;
				}
			};

			define('geneticsMock', [], function () {
				return that.geneticsMock;
			});

			require.undef('genetics/movementGene');
			require.config({
				map: {
					'genetics/movementGene': {
						'genetics/genetics': 'geneticsMock'
					}
				}
			});
		});

		afterEach(function () {
			require.undef('geneticsMock');
			require.config({
				map: {
					'genetics/movementGene': {
						'genetics/genetics': 'genetics/genetics'
					}
				}
			});
		});

		describe("when minAngularForce is 30, maxAngularForce is 70, minLinearForce is 1, maxLinearForce is 7", function () {
			beforeEach(function () {
				geneticsOptions.movement.minAngularForce.set(30);
				geneticsOptions.movement.maxAngularForce.set(70);
				geneticsOptions.movement.minLinearForce.set(1);
				geneticsOptions.movement.maxLinearForce.set(7);
			});

			describe("when a MovementGene is created", function () {
					beforeEach(function () {
							this.sut = new MovementGene();
					});

					describe("when Math.random always returns 0.5", function () {
							beforeEach(function () {
									this.rnd = Math.random;
									Math.random = function () { return 0.5; };
							});

							afterEach(function () {
									Math.random = this.rnd;
							});

							describe("when angularForceReplacementRate is 1.0", function () {
									beforeEach(function () {
											geneticsOptions.movement.angularForceReplacementRate.set(1.0);

											this.sut.angularForce = 31;
											this.sut.mutate();
									});

									it("mutate changes angularForce to 50", function () {
											expect(this.sut.angularForce).toEqual(50);
									});
							});

							describe("when linearForceReplacementRate is 1.0", function () {
									beforeEach(function () {
											geneticsOptions.movement.linearForceReplacementRate.set(1.0);

											this.sut.linearForce = 1;
											this.sut.mutate();
									});

									it("mutate changes linearForce to 4", function () {
											expect(this.sut.linearForce).toEqual(4);
									});
							});
					});
			});

			describe("when the state of the MovementGene is requested", function () {
				beforeEach(function () {
					this.sut = new MovementGene();
					this.geneState = this.sut.getState();
				});

				it("should return a defined representation of the MovementGene", function () {
					expect(this.geneState).toBeDefined();
				});

				it("should return a representation of the MovementGene that contains the angularForce", function () {
					expect(this.geneState.angularForce).toEqual(this.sut.angularForce);
				});

				it("should return a representation of the MovementGene that contains the linearForce", function () {
					expect(this.geneState.linearForce).toEqual(this.sut.linearForce);
				});
			});

			describe("when Math.random always returns 0.5", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.5; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when a MovementGene is created", function () {
					beforeEach(function () {
						this.sut = new MovementGene();
					});

					it("the angularForce is 50", function () {
						expect(this.sut.angularForce).toEqual(50);
					});

					it("the linearForce is 4", function () {
						expect(this.sut.linearForce).toEqual(4);
					});

					describe("when mutationFraction is 0.1, angularForceMutationRate is 1.0, linearForceMutationRate is 1.0", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);

							geneticsOptions.movement.angularForceMutationRate.set(1.0);
							geneticsOptions.movement.linearForceMutationRate.set(1.0);
							this.sut.mutate();
						});

						it("mutate does not change angularForce", function () {
							expect(this.sut.angularForce).toEqual(50);
						});

						it("mutate does not change linearForce", function () {
							expect(this.sut.linearForce).toEqual(4);
						});
					});

					describe("when angularForceMutationRate, linearForceMutationRate are less than the result of Math.random", function () {
						beforeEach(function () {
							geneticsOptions.movement.angularForceMutationRate.set(0.49);
							geneticsOptions.movement.linearForceMutationRate.set(0.49);
							this.sut.mutate();
						});

						it("mutate does not change angularForce", function () {
							expect(this.sut.angularForce).toEqual(50);
						});

						it("mutate does not change linearForce", function () {
							expect(this.sut.linearForce).toEqual(4);
						});
					});
				});
			});

			describe("when Math.random always returns 0.25", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.25; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when a MovementGene is created", function () {
					beforeEach(function () {
						this.sut = new MovementGene();
					});

					it("the angularForce is 40", function () {
						expect(this.sut.angularForce).toEqual(40);
					});

					it("the linearForce is 2.5", function () {
						expect(this.sut.linearForce).toEqual(2.5);
					});

					describe("when mutationFraction is 0.1, angularForceMutationRate is 1.0, linearForceMutationRate is 1.0", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);

							geneticsOptions.movement.angularForceMutationRate.set(1.0);
							geneticsOptions.movement.linearForceMutationRate.set(1.0);
							this.sut.mutate();
						});

						it("mutate changes angularForce to 38", function () {
							expect(this.sut.angularForce).toEqual(38);
						});

						it("mutate changes linearForce to 2.2", function () {
							expect(this.sut.linearForce).toEqual(2.2);
						});
					});

					describe("when angularForceMutationRate, linearForceMutationRate are less than the result of Math.random", function () {
						beforeEach(function () {
							geneticsOptions.movement.angularForceMutationRate.set(0.24);
							geneticsOptions.movement.linearForceMutationRate.set(0.24);
							this.sut.mutate();
						});

						it("mutate does not change angularForce", function () {
							expect(this.sut.angularForce).toEqual(40);
						});

						it("mutate does not change linearForce", function () {
							expect(this.sut.linearForce).toEqual(2.5);
						});
					});
				});
			});

			describe("when Math.random always returns 0.75", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.75; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when a MovementGene is created", function () {
					beforeEach(function () {
						this.sut = new MovementGene();
					});

					it("the angularForce is 60", function () {
						expect(this.sut.angularForce).toEqual(60);
					});

					it("the linearForce is 5.5", function () {
						expect(this.sut.linearForce).toEqual(5.5);
					});

					describe("when mutationFraction is 0.1, angularForceMutationRate is 1.0, linearForceMutationRate is 1.0", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);

							geneticsOptions.movement.angularForceMutationRate.set(1.0);
							geneticsOptions.movement.linearForceMutationRate.set(1.0);
							this.sut.mutate();
						});

						it("mutate changes angularForce to 62", function () {
							expect(this.sut.angularForce).toEqual(62);
						});

						it("mutate changes linearForce to 5.8", function () {
							expect(this.sut.linearForce).toEqual(5.8);
						});
					});

					describe("when angularForceMutationRate, linearForceMutationRate are less than the result of Math.random", function () {
						beforeEach(function () {
							geneticsOptions.movement.angularForceMutationRate.set(0.74);
							geneticsOptions.movement.linearForceMutationRate.set(0.74);
							this.sut.mutate();
						});

						it("mutate does not change angularForce", function () {
							expect(this.sut.angularForce).toEqual(60);
						});

						it("mutate does not change linearForce", function () {
							expect(this.sut.linearForce).toEqual(5.5);
						});
					});
				});
			});

			describe("when the movement is cloned", function () {
				beforeEach(function () {
					this.sut = new MovementGene();
					this.clone = this.sut.clone();
				});

				it("the clone should not be the same object as the source", function () {
					expect(this.clone).not.toBe(this.sut);
				});

				it("the clone should have the same angularForce", function () {
					expect(this.clone.angularForce).toEqual(this.sut.angularForce);
				});

				it("the clone should have the same linearForce", function () {
					expect(this.clone.linearForce).toEqual(this.sut.linearForce);
				});

				describe("when the properties of the clone are changed", function () {
					beforeEach(function () {
						this.clone.angularForce += 1;
						this.clone.linearForce += 1;
					});

					it("the change doesn't influence the original", function () {
						expect(this.sut.angularForce).not.toBe(this.clone.angularForce);
						expect(this.sut.angularForce).not.toEqual(this.clone.angularForce);

						expect(this.sut.linearForce).not.toBe(this.clone.linearForce);
						expect(this.sut.linearForce).not.toEqual(this.clone.linearForce);
					});
				});
			});

			describe("when two MovementGenes are created", function () {
				beforeEach(function (done) {
					var that = this;
					require(['genetics/movementGene'], function (MovementGene) {
						that.sut = new MovementGene();
						that.partner = new MovementGene();
						done();
					});
				});

				describe("when they mate with eachother", function () {
					beforeEach(function () {
						spyOn(this.geneticsMock, 'mate').and.callThrough();
						this.children = this.sut.mate(this.partner);
					});

					it("should produce 2 children that are not the parents", function () {
						expect(this.children.length).toEqual(2);

						for (var i = 0; i < 2; i++) {
							expect(this.children[i]).not.toBe(this.sut);
							expect(this.children[i]).not.toBe(this.partner);
						}
					});

					it("should return the children that were created by genetics.mate", function () {
						for (var i = 0; i < 2; i++) {
							expect(this.children[i].angularForce).toBe(this.childrenMock[i].angularForce);
							expect(this.children[i].linearForce).toBe(this.childrenMock[i].linearForce);
						}
					});

					it("calls the genetics.mate method 1 time", function () {
						expect(this.geneticsMock.mate.calls.count()).toEqual(1);
					});

					it("calls the genetics.mate method with the sot, the partner and a callback function", function () {
						expect(this.geneticsMock.mate).toHaveBeenCalledWith(this.sut, this.partner, jasmine.any(Function));
					});
				});
			});
		});
	});
});