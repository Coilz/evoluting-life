define(['genetics/options', 'genetics/lifeGene'], function(geneticsOptions, LifeGene) {
	"use strict";

	describe("genetics.LifeGene", function() {
		beforeEach(function() {
			geneticsOptions.reset();

			var that = this;

			// Create a geneticsMock
			this.childrenMock = [{
				initialEnergy: "initialEnergy1",
				oldAge: "oldAge1",
				matureAge: "matureAge1",
				nutrition: "nutrition1"
			},
			{
				initialEnergy: "initialEnergy2",
				oldAge: "oldAge2",
				matureAge: "matureAge1",
				nutrition: "nutrition1"
			}];

			this.geneticsMock = {
				mate: function (a, b, createCallback) {
					var children = [];
					for (var i = 0; i < 2; i++) {
						children.push(createCallback(that.childrenMock[i]));
					}
					return children;
				}
			};

			define('geneticsMock', [], function () {
				return that.geneticsMock;
			});

			require.undef('genetics/lifeGene');
			require.config({
				map: {
					'genetics/lifeGene': {
						'genetics/genetics': 'geneticsMock'
					}
				}
			});
		});

		afterEach(function() {
			require.undef('geneticsMock');
			require.config({
				map: {
					'genetics/lifeGene': {
						'genetics/genetics': 'genetics/genetics'
					}
				}
			});
		});

		describe("when minimum and maximum values for properties are known", function() {
			beforeEach(function() {
				geneticsOptions.life.minInitialEnergy.set(1);
				geneticsOptions.life.maxInitialEnergy.set(9);
				geneticsOptions.life.minOldAge.set(30);
				geneticsOptions.life.maxOldAge.set(70);
				geneticsOptions.life.minMatureAge.set(1);
				geneticsOptions.life.maxMatureAge.set(5);
				geneticsOptions.life.minNutrition.set(1);
				geneticsOptions.life.maxNutrition.set(7);
			});

			describe("when the state of the LifeGene is requested", function () {
				beforeEach(function() {
					this.sut = new LifeGene();
					this.geneState = this.sut.getState();
				});

				it("should return a defined representation of the LifeGene", function () {
					expect(this.geneState).toBeDefined();
				});

				it("should return a representation of the LifeGene that contains the initialEnergy", function () {
					expect(this.geneState.initialEnergy).toEqual(this.sut.initialEnergy);
				});

				it("should return a representation of the LifeGene that contains the oldAge", function () {
					expect(this.geneState.oldAge).toEqual(this.sut.oldAge);
				});

				it("should return a representation of the LifeGene that contains the matureAge", function () {
					expect(this.geneState.matureAge).toEqual(this.sut.matureAge);
				});

				it("should return a representation of the LifeGene that contains the nutrition", function () {
					expect(this.geneState.nutrition).toEqual(this.sut.nutrition);
				});
			});

			describe("when Math.random always returns 0.5", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.5;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when a LifeGene is created", function() {
					beforeEach(function() {
						this.sut = new LifeGene();
					});

					it("the initialEnergy is 5", function() {
						expect(this.sut.initialEnergy).toEqual(5);
					});

					it("the oldAge is 50", function() {
						expect(this.sut.oldAge).toEqual(50);
					});

					it("the matureAge is 3", function() {
						expect(this.sut.matureAge).toEqual(3);
					});

					it("the nutrition is 4", function() {
						expect(this.sut.nutrition).toEqual(4);
					});

					describe("when mutationFraction is 0.1 and all MutationRates are", function() {
						beforeEach(function() {
							geneticsOptions.evolution.mutationFraction.set(0.1);

							geneticsOptions.life.initialEnergyMutationRate.set(1.0);
							geneticsOptions.life.oldAgeMutationRate.set(1.0);
							geneticsOptions.life.matureAgeMutationRate.set(1.0);
							geneticsOptions.life.nutritionMutationRate.set(1.0);
							this.sut.mutate();
						});

						it("mutate does not change initialEnergy", function() {
							expect(this.sut.initialEnergy).toEqual(5);
						});

						it("mutate does not change oldAge", function() {
							expect(this.sut.oldAge).toEqual(50);
						});

						it("mutate does not change matureAge", function() {
							expect(this.sut.matureAge).toEqual(3);
						});

						it("mutate does not change nutrition", function() {
							expect(this.sut.nutrition).toEqual(4);
						});
					});

					describe("when the MutationRates are all less than the result of Math.random", function() {
						beforeEach(function() {
							geneticsOptions.life.oldAgeMutationRate.set(0.49);
							geneticsOptions.life.oldAgeMutationRate.set(0.49);
							geneticsOptions.life.matureAgeMutationRate.set(0.49);
							geneticsOptions.life.nutritionMutationRate.set(0.49);
							this.sut.mutate();
						});

						it("mutate does not change initialEnergy", function() {
							expect(this.sut.initialEnergy).toEqual(5);
						});

						it("mutate does not change oldAge", function() {
							expect(this.sut.oldAge).toEqual(50);
						});

						it("mutate does not change matureAge", function() {
							expect(this.sut.matureAge).toEqual(3);
						});

						it("mutate does not change nutrition", function() {
							expect(this.sut.nutrition).toEqual(4);
						});
					});
				});
			});

			describe("when Math.random always returns 0.25", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.25;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when a LifeGene is created", function() {
					beforeEach(function() {
						this.sut = new LifeGene();
					});

					it("the initialEnergy is 3", function() {
						expect(this.sut.initialEnergy).toEqual(3);
					});

					it("the oldAge is 40", function() {
						expect(this.sut.oldAge).toEqual(40);
					});

					it("the matureAge is 0.2", function() {
						expect(this.sut.matureAge).toEqual(2);
					});

					it("the nutrition is 2.5", function() {
						expect(this.sut.nutrition).toEqual(2.5);
					});

					describe("when mutationFraction is 0.1 and all MutationRates are 1.0", function() {
						beforeEach(function() {
							geneticsOptions.evolution.mutationFraction.set(0.1);

							geneticsOptions.life.initialEnergyMutationRate.set(1.0);
							geneticsOptions.life.oldAgeMutationRate.set(1.0);
							geneticsOptions.life.matureAgeMutationRate.set(1.0);
							geneticsOptions.life.nutritionMutationRate.set(1.0);
							this.sut.mutate();
						});

						it("mutate changes initialEnergy to 2.6", function() {
							expect(this.sut.initialEnergy).toEqual(2.6);
						});

						it("mutate changes oldAge to 38", function() {
							expect(this.sut.oldAge).toEqual(38);
						});

						it("mutate changes matureAge to 1.8", function() {
							expect(this.sut.matureAge).toEqual(1.8);
						});

						it("mutate changes nutrition to 2.2", function() {
							expect(this.sut.nutrition).toEqual(2.2);
						});
					});

					describe("when all MutationRates are less than the result of Math.random", function() {
						beforeEach(function() {
							geneticsOptions.life.initialEnergyMutationRate.set(0.24);
							geneticsOptions.life.oldAgeMutationRate.set(0.24);
							geneticsOptions.life.matureAgeMutationRate.set(0.24);
							geneticsOptions.life.nutritionMutationRate.set(0.24);
							this.sut.mutate();
						});

						it("mutate does not change initialEnergy", function() {
							expect(this.sut.initialEnergy).toEqual(3);
						});

						it("mutate does not change oldAge", function() {
							expect(this.sut.oldAge).toEqual(40);
						});

						it("mutate does not change matureAge", function() {
							expect(this.sut.matureAge).toEqual(2);
						});

						it("mutate does not change nutrition", function() {
							expect(this.sut.nutrition).toEqual(2.5);
						});
					});
				});
			});

			describe("when Math.random always returns 0.75", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.75;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when a LifeGene is created", function() {
					beforeEach(function() {
						this.sut = new LifeGene();
					});

					it("the initialEnergy is 7", function() {
						expect(this.sut.initialEnergy).toEqual(7);
					});

					it("the oldAge is 60", function() {
						expect(this.sut.oldAge).toEqual(60);
					});

					it("the matureAge is 4", function() {
						expect(this.sut.matureAge).toEqual(4);
					});

					it("the nutrition is 5.5", function() {
						expect(this.sut.nutrition).toEqual(5.5);
					});

					describe("when mutationFraction is 0.1 and all MutationRates are 1.0", function() {
						beforeEach(function() {
							geneticsOptions.evolution.mutationFraction.set(0.1);

							geneticsOptions.life.initialEnergyMutationRate.set(1.0);
							geneticsOptions.life.oldAgeMutationRate.set(1.0);
							geneticsOptions.life.matureAgeMutationRate.set(1.0);
							geneticsOptions.life.nutritionMutationRate.set(1.0);
							this.sut.mutate();
						});

						it("mutate changes initialEnergy to 7.4", function() {
							expect(this.sut.initialEnergy).toEqual(7.4);
						});

						it("mutate changes oldAge to 62", function() {
							expect(this.sut.oldAge).toEqual(62);
						});

						it("mutate changes matureAge to 4.2", function() {
							expect(this.sut.matureAge).toEqual(4.2);
						});

						it("mutate changes nutrition to 5.8", function() {
							expect(this.sut.nutrition).toEqual(5.8);
						});
					});

					describe("when all MutationRates are less than the result of Math.random", function() {
						beforeEach(function() {
							geneticsOptions.life.initialEnergyMutationRate.set(0.74);
							geneticsOptions.life.oldAgeMutationRate.set(0.74);
							geneticsOptions.life.matureAgeMutationRate.set(0.74);
							geneticsOptions.life.nutritionMutationRate.set(0.74);
							this.sut.mutate();
						});

						it("mutate does not change initialEnergy", function() {
							expect(this.sut.initialEnergy).toEqual(7);
						});

						it("mutate does not change oldAge", function() {
							expect(this.sut.oldAge).toEqual(60);
						});

						it("mutate does not change matureAge", function() {
							expect(this.sut.matureAge).toEqual(4);
						});

						it("mutate does not change nutrition", function() {
							expect(this.sut.nutrition).toEqual(5.5);
						});
					});
				});
			});

			describe("when the life is cloned", function() {
				beforeEach(function() {
					this.sut = new LifeGene();
					this.clone = this.sut.clone();
				});

				it("the clone should not be the same object as the source", function() {
					expect(this.clone).not.toBe(this.sut);
				});

				it("the clone should have the same initialEnergy", function() {
					expect(this.clone.initialEnergy).toEqual(this.sut.initialEnergy);
				});

				it("the clone should have the same oldAge", function() {
					expect(this.clone.oldAge).toEqual(this.sut.oldAge);
				});

				it("the clone should have the same matureAge", function() {
					expect(this.clone.matureAge).toEqual(this.sut.matureAge);
				});

				it("the clone should have the same nutrition", function() {
					expect(this.clone.nutrition).toEqual(this.sut.nutrition);
				});

				describe("when the properties of the clone are changed", function () {
					beforeEach(function () {
						this.clone.initialEnergy += 1;
						this.clone.oldAge += 1;
						this.clone.matureAge += 1;
						this.clone.nutrition += 1;
					});

					it("the change doesn't influence the original", function () {
						expect(this.sut.initialEnergy).not.toBe(this.clone.initialEnergy);
						expect(this.sut.initialEnergy).not.toEqual(this.clone.initialEnergy);

						expect(this.sut.oldAge).not.toBe(this.clone.oldAge);
						expect(this.sut.oldAge).not.toEqual(this.clone.oldAge);

						expect(this.sut.matureAge).not.toBe(this.clone.matureAge);
						expect(this.sut.matureAge).not.toEqual(this.clone.matureAge);

						expect(this.sut.nutrition).not.toBe(this.clone.nutrition);
						expect(this.sut.nutrition).not.toEqual(this.clone.nutrition);
					});
				});
			});

			describe("when two LifeGenes are created", function() {
				beforeEach(function(done) {
					var that = this;
					require(['genetics/lifeGene'], function(LifeGene) {
						that.sut = new LifeGene();
						that.partner = new LifeGene();
						done();
					});
				});

				describe("when they mate with eachother", function() {
					beforeEach(function() {
						spyOn(this.geneticsMock, 'mate').and.callThrough();
						this.children = this.sut.mate(this.partner);
					});

					it("should produce 2 children that are not the parents", function() {
						expect(this.children.length).toEqual(2);

						for (var i=0; i<2; i++) {
							expect(this.children[i]).not.toBe(this.sut);
							expect(this.children[i]).not.toBe(this.partner);
						}
					});

					it("should return the children that were created by genetics.mate", function() {
						for (var i = 0; i < 2; i++) {
							expect(this.children[i].initialEnergy).toBe(this.childrenMock[i].initialEnergy);
							expect(this.children[i].oldAge).toBe(this.childrenMock[i].oldAge);
							expect(this.children[i].matureAge).toBe(this.childrenMock[i].matureAge);
							expect(this.children[i].nutrition).toBe(this.childrenMock[i].nutrition);
						}
					});

					it("calls the genetics.mate method 1 time", function() {
						expect(this.geneticsMock.mate.calls.count()).toEqual(1);
					});

					it("calls the genetics.mate method with the sot, the partner and a callback function", function() {
						expect(this.geneticsMock.mate).toHaveBeenCalledWith(this.sut, this.partner, jasmine.any(Function));
					});
				});
			});
		});
	});
});