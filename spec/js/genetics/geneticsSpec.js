define(['genetics/genetics'], function(genetics) {
	"use strict";

	describe("genetics.genetics", function() {
		beforeEach(function() {
			this.someThing = function (mateState) {
				this.getMateState = function () {
					return mateState;
				};
			};

			this.a = new this.someThing({one: "a1", two: "a2", three: "a3", four: "a4"});
			this.b = new this.someThing({one: "b1", two: "b2", three: "b3", four: "b4"});
		});

		describe("when mate is called with a and b and a createCallback", function() {
			beforeEach(function() {
				this.children = genetics.mate(this.a, this.b, function (mateState) {
					return mateState;
				});
			});

			it("should produce 2 children that are not the parents", function() {
				expect(this.children.length).toEqual(2);

				for (var i=0; i<2; i++) {
					expect(this.children[i]).not.toBe(this.a);
					expect(this.children[i]).not.toBe(this.b);
				}
			});

			it("should produce 2 children with each 4 properties", function() {
				expect(Object.keys(this.children[0]).length).toEqual(4);
				expect(Object.keys(this.children[1]).length).toEqual(4);
			});

			it("should mix the properties of the parents", function() {
				var propertyName;

				var propertyNames = [];
	            for (propertyName in this.children[0]) {
	                propertyNames.push( propertyName );
	            }

				for (var i=0; i<propertyNames.length; i++) {
					propertyName = propertyNames[i];
					var child0Property = this.children[0][propertyName];
					var child1Property = this.children[1][propertyName];
					expect(child0Property).not.toEqual(child1Property);
				}
			});

			describe("when properties of the children are changed", function() {
				beforeEach(function() {
					this.children = genetics.mate(this.a, this.b, function (mateState) {
						return mateState;
					});
				});

				it("should not affect their parents", function() {
					this.children[0].one = "child0Property1";
					this.children[1].one = "child1Property1";

					expect(this.a.getMateState().one).toEqual("a1");
					expect(this.b.getMateState().one).toEqual("b1");
				});

				it("should not affect their siblings", function() {
					var aNewValue = "aNewValue";
					this.children[0].one = aNewValue;

					expect(this.children[1].one).not.toEqual(aNewValue);
					expect(this.children[1].two).not.toEqual(aNewValue);
					expect(this.children[1].three).not.toEqual(aNewValue);
					expect(this.children[1].four).not.toEqual(aNewValue);
				});
			});
		});
	});
});
