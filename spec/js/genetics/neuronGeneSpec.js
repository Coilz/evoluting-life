define(['genetics/options', 'genetics/neuronGene'], function (geneticsOptions, NeuronGene) {
	"use strict";

	describe("genetics.NeuronGene", function () {
		beforeEach(function () {
			geneticsOptions.reset();
			geneticsOptions.neuron.thresholdReplacementRate.set(0.0);
			geneticsOptions.neuron.relaxationReplacementRate.set(0.0);

			var that = this;

			// Create a geneticsMock
			this.childrenMock = [{
				threshold: "threshold1",
				relaxation: "relaxation1",
				axons: []
			},
			{
				threshold: "threshold2",
				relaxation: "relaxation2",
				axons: []
			}];

			this.geneticsMock = {
				mate: function (a, b, createCallback) {
					var children = [];
					for (var i = 0; i < 2; i++) {
						children.push(createCallback(that.childrenMock[i]));
					}
					return children;
				}
			};

			define('geneticsMock', [], function () {
				return that.geneticsMock;
			});

			// Create an axonMock
			this.mock = {
				mutate: function () { },
				clone: function () { },
				mate: function (partner) {
					return [{}, {}];
				},
				getState: function () {
					return {};
				}
			};
			this.axonMock = function () { return that.mock; };

			define('axonMock', [], function () {
				return that.axonMock;
			});

			require.undef('genetics/neuronGene');
			require.config({
				map: {
					'genetics/neuronGene': {
						'genetics/genetics': 'geneticsMock',
						'genetics/axonGene': 'axonMock'
					}
				}
			});
		});

		afterEach(function () {
			require.undef('axonMock');
			require.undef('geneticsMock');
			require.config({
				map: {
					'genetics/neuronGene': {
						'genetics/genetics': 'genetics/genetics',
						'genetics/axonGene': 'genetics/AxonGene'
					}
				}
			});
		});

		describe("when a gene is created", function () {
			beforeEach(function () {
				this.sut = new NeuronGene(0);
			});

			it("the number of axons is zero", function () {
				expect(this.sut.axons.length).toEqual(0);
			});

			describe("when Math.random always returns 0.5", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.5; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when thresholdReplacementRate is 1.0 and minThreshold is 1 and maxThreshold is 5", function () {
					beforeEach(function () {
						geneticsOptions.neuron.minThreshold.set(1);
						geneticsOptions.neuron.maxThreshold.set(5);
						geneticsOptions.neuron.thresholdReplacementRate.set(1.0);

						this.sut.threshold = 1;
						this.sut.mutate();
					});

					it("mutate changes the threshold to 3", function () {
						expect(this.sut.threshold).toEqual(3);
					});
				});

				describe("when relaxationReplacementRate is 1.0 and maxRelaxation is 20", function () {
					beforeEach(function () {
						geneticsOptions.neuron.maxRelaxation.set(20);
						geneticsOptions.neuron.relaxationReplacementRate.set(1.0);

						this.sut.relaxation = 1;
						this.sut.mutate();
					});

					it("mutate changes the relaxation to 0.1", function () {
						expect(this.sut.relaxation).toEqual(0.1);
					});
				});
			});

			describe("when the state of the NeuronGene is requested", function () {
				beforeEach(function () {
					this.sut = new NeuronGene(3);
					this.geneState = this.sut.getState();
				});

				it("should return a defined representation of the NeuronGene", function () {
					expect(this.geneState).toBeDefined();
				});

				it("should return a representation of the NeuronGene that contains the threshold", function () {
					expect(this.geneState.threshold).toBeDefined();
					expect(this.geneState.threshold).toEqual(this.sut.threshold);
				});

				it("should return a representation of the NeuronGene that contains the relaxation", function () {
					expect(this.geneState.relaxation).toBeDefined();
					expect(this.geneState.relaxation).toEqual(this.sut.relaxation);
				});

				it("should return a representation of the NeuronGene that contains the axons", function () {
					expect(this.geneState.axons).toBeDefined();
					expect(this.geneState.axons.length).toEqual(this.sut.axons.length);
					for (var i = 0; i < this.sut.axons.length; i++) {
						expect(this.geneState.axons[i]).toEqual(this.sut.axons[i].getState());
					}
				});
			});

			describe("when the minThreshold and maxThreshold have the same value", function () {
				beforeEach(function () {
					geneticsOptions.neuron.minThreshold.set(5);
					geneticsOptions.neuron.maxThreshold.set(5);

					this.sut = new NeuronGene(0);
				});

				it("threshold should have that value", function () {
					expect(this.sut.threshold).toEqual(5);
				});
			});

			describe("when minThreshold is 1 and maxThreshold is 5", function () {
				beforeEach(function () {
					geneticsOptions.neuron.minThreshold.set(1);
					geneticsOptions.neuron.maxThreshold.set(5);
				});

				describe("when Math.random always returns 0.5", function () {
					beforeEach(function () {
						this.rnd = Math.random;
						Math.random = function () { return 0.5; };

						this.sut = new NeuronGene(0);
					});

					afterEach(function () {
						Math.random = this.rnd;
					});

					it("threshold should have value 3", function () {
						expect(this.sut.threshold).toEqual(3);
					});

					describe("when mutationFraction is 0.1", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);
							this.sut.mutate();
						});

						it("mutate does not change the threshold", function () {
							expect(this.sut.threshold).toEqual(3);
						});
					});
				});

				describe("when Math.random always returns 0.25", function () {
					beforeEach(function () {
						this.rnd = Math.random;
						Math.random = function () { return 0.25; };

						this.sut = new NeuronGene(0);
					});

					afterEach(function () {
						Math.random = this.rnd;
					});

					it("threshold should have value 2", function () {
						expect(this.sut.threshold).toEqual(2);
					});

					describe("when mutationFraction is 0.1", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);
						});

						describe("when thresholdMutationRate is 1.0", function () {
							beforeEach(function () {
								geneticsOptions.neuron.thresholdMutationRate.set(1.0);
								this.sut.mutate();
							});

							it("mutate changes the threshold to 1.80", function () {
								expect(this.sut.threshold).toEqual(1.80);
							});
						});

						describe("when thresholdMutationRate is less than the result of Math.random", function () {
							beforeEach(function () {
								geneticsOptions.neuron.thresholdMutationRate.set(0.24);
								this.sut.mutate();
							});

							it("mutate does not change the threshold", function () {
								expect(this.sut.threshold).toEqual(2);
							});
						});
					});
				});

				describe("when Math.random always returns 0.75", function () {
					beforeEach(function () {
						this.rnd = Math.random;
						Math.random = function () { return 0.75; };

						this.sut = new NeuronGene(0);
					});

					afterEach(function () {
						Math.random = this.rnd;
					});

					it("threshold should have value 4", function () {
						expect(this.sut.threshold).toEqual(4);
					});

					describe("when mutationFraction is 0.1", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);
						});

						describe("when thresholdMutationRate is 1.0", function () {
							beforeEach(function () {
								geneticsOptions.neuron.thresholdMutationRate.set(1.0);
								this.sut.mutate();
							});

							it("mutate changes the threshold to 4.20", function () {
								expect(this.sut.threshold).toEqual(4.20);
							});
						});

						describe("when thresholdMutationRate is less than the result of Math.random", function () {
							beforeEach(function () {
								geneticsOptions.neuron.thresholdMutationRate.set(0.74);
								this.sut.mutate();
							});

							it("mutate does not change the threshold", function () {
								expect(this.sut.threshold).toEqual(4);
							});
						});
					});
				});
			});

			describe("when maxRelaxation is 60", function () {
				beforeEach(function () {
					geneticsOptions.neuron.maxRelaxation.set(60);
				});

				describe("when Math.random always returns 0.5", function () {
					beforeEach(function () {
						this.rnd = Math.random;
						Math.random = function () { return 0.5; };

						this.sut = new NeuronGene(0);
					});

					afterEach(function () {
						Math.random = this.rnd;
					});

					it("relaxation should have value 0.3", function () {
						expect(this.sut.relaxation).toEqual(0.3);
					});
				});

				describe("when Math.random always returns 0.25", function () {
					beforeEach(function () {
						this.rnd = Math.random;
						Math.random = function () { return 0.25; };

						this.sut = new NeuronGene(0);
					});

					afterEach(function () {
						Math.random = this.rnd;
					});

					it("relaxation should have value 0.15", function () {
						expect(this.sut.relaxation).toEqual(0.15);
					});

					describe("when mutationFraction is 0.1", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);
						});

						describe("when relaxationMutationRate is 1.0", function () {
							beforeEach(function () {
								geneticsOptions.neuron.relaxationMutationRate.set(1.0);
								this.sut.mutate();
							});

							it("mutate changes the relaxation to 0.12", function () {
								expect(this.sut.relaxation).toEqual(0.12);
							});
						});

						describe("when relaxationMutationRate is less than the result of Math.random", function () {
							beforeEach(function () {
								geneticsOptions.neuron.relaxationMutationRate.set(0.24);
								this.sut.mutate();
							});

							it("mutate does not change the relaxation", function () {
								expect(this.sut.relaxation).toEqual(0.15);
							});
						});
					});
				});

				describe("when Math.random always returns 0.75", function () {
					beforeEach(function () {
						this.rnd = Math.random;
						Math.random = function () { return 0.75; };

						this.sut = new NeuronGene(0);
					});

					afterEach(function () {
						Math.random = this.rnd;
					});

					it("relaxation should have value 0.45", function () {
						expect(this.sut.relaxation).toEqual(0.45);
					});

					describe("when mutationFraction is 0.1", function () {
						beforeEach(function () {
							geneticsOptions.evolution.mutationFraction.set(0.1);
						});

						describe("when relaxationMutationRate is 1.0", function () {
							beforeEach(function () {
								geneticsOptions.neuron.relaxationMutationRate.set(1.0);
								this.sut.mutate();
							});

							it("mutate changes the relaxation to 0.48", function () {
								expect(this.sut.relaxation).toEqual(0.48);
							});
						});

						describe("when relaxationMutationRate is less than the result of Math.random", function () {
							beforeEach(function () {
								geneticsOptions.neuron.relaxationMutationRate.set(0.74);
								this.sut.mutate();
							});

							it("mutate does not change the relaxation", function () {
								expect(this.sut.relaxation).toEqual(0.45);
							});
						});
					});
				});
			});
		});

		describe("when a gene is created with value 3 for maxOutputs", function () {
			beforeEach(function (done) {
				spyOn(this, 'axonMock').and.callThrough();

				this.maxOutputs = 3;

				var that = this;
				require(['genetics/neuronGene'], function (NeuronGene) {
					that.sut = new NeuronGene(that.maxOutputs);
					done();
				});
			});

			it("the number of axons is equal to maxOutputs", function () {
				expect(this.sut.axons.length).toEqual(this.maxOutputs);
			});

			it("calls the constructor of AxonGene maxOutputs times", function () {
				expect(this.axonMock.calls.count()).toEqual(this.maxOutputs);
			});

			describe("when the gene mutates", function () {
				beforeEach(function () {
					geneticsOptions.neuron.axonGeneReplacementRate.set(0.0);

					spyOn(this.mock, 'mutate');
					this.sut.mutate();
				});

				it("calls the axon.mutate method maxOutputs times", function () {
					expect(this.mock.mutate.calls.count()).toEqual(this.maxOutputs);
				});
			});

			describe("when the gene mutates and axonGeneReplacementRate is 1.0", function () {
				beforeEach(function () {
					geneticsOptions.neuron.axonGeneReplacementRate.set(1.0);

					this.sut.mutate();
				});

				it("calls the createAxonGene method maxOutputs times for the second time", function () {
					expect(this.axonMock.calls.count()).toEqual(2 * this.maxOutputs);
				});
			});

			describe("when the gene is cloned", function () {
				beforeEach(function () {
					this.clone = this.sut.clone();
				});

				it("the clone should not be the same object as the source", function () {
					expect(this.clone).not.toBe(this.sut);
				});

				it("the clone should have the same threshold", function () {
					expect(this.clone.threshold).toEqual(this.sut.threshold);
				});

				it("the clone should have the same relaxation", function () {
					expect(this.clone.relaxation).toEqual(this.sut.relaxation);
				});

				it("the clone should have the same number of axons", function () {
					expect(this.clone.axons.length).toEqual(this.sut.axons.length);
				});

				it("the axons of the clone should not be the same object as the axons of the source", function () {
					expect(this.clone.axons).not.toBe(this.sut.axons);

					for (var i = 0; i < this.sut.axons.length; i++) {
						for (var j = 0; j < this.sut.clone.length; j++) {
							expect(this.clone.axons[j]).not.toBe(this.sut.axons[i]);
						}
					}
				});

				describe("when the properties of the clone are changed", function () {
					beforeEach(function () {
						this.clone.threshold += 1;
						this.clone.relaxation += 0.1;
					});

					it("the change doesn't influence the original", function () {
						expect(this.sut.threshold).not.toBe(this.clone.threshold);
						expect(this.sut.threshold).not.toEqual(this.clone.threshold);

						expect(this.sut.relaxation).not.toBe(this.clone.relaxation);
						expect(this.sut.relaxation).not.toEqual(this.clone.relaxation);
					});
				});
			});

			describe("when two NeuronGenes are created", function () {
				beforeEach(function (done) {
					var that = this;
					require(['genetics/neuronGene'], function (NeuronGene) {
						that.sut = new NeuronGene(that.maxOutputs);
						that.partner = new NeuronGene(that.maxOutputs);
						done();
					});
				});

				describe("when they mate with eachother", function () {
					beforeEach(function () {
						spyOn(this.geneticsMock, 'mate').and.callThrough();
						spyOn(this.mock, 'mate').and.callThrough();

						this.children = this.sut.mate(this.partner);
					});

					it("should produce 2 children that are not the parents", function () {
						expect(this.children.length).toEqual(2);

						for (var i = 0; i < 2; i++) {
							expect(this.children[i]).not.toBe(this.sut);
							expect(this.children[i]).not.toBe(this.partner);
						}
					});

					it("should return the children that were created by genetics.mate", function () {
						for (var i=0; i<2; i++) {
							expect(this.children[i].threshold).toBe(this.childrenMock[i].threshold);
							expect(this.children[i].relaxation).toBe(this.childrenMock[i].relaxation);
							expect(this.children[i].axons.length).toBe(this.sut.axons.length);
						}
					});

					it("calls the genetics.mate method 1 time", function () {
						expect(this.geneticsMock.mate.calls.count()).toEqual(1);
					});

					it("calls the genetics.mate method with the sot, the partner and a callback function", function () {
						expect(this.geneticsMock.mate).toHaveBeenCalledWith(this.sut, this.partner, jasmine.any(Function));
					});

					it("calls the axon.mate method maxOutputs times", function () {
						expect(this.mock.mate.calls.count()).toEqual(this.maxOutputs);
					});
				});
			});
		});
	});
});