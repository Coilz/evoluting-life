define(['brains/options', 'brains/axon'], function(brainsOptions, Axon) {
	"use strict";

	describe("brains.Axon", function() {
		beforeEach(function() {
			brainsOptions.reset();

			this.axonGeneMock = {
				strength: Math.random()
			};
			this.neuronMock = {
				excite: function(input) {}
			};

			require.undef('brains/axon');
		});

		afterEach(function() {
		});

		describe("when an axon is created with a axonGene", function() {
			beforeEach(function() {
				spyOn(this.neuronMock, 'excite');
				this.sut = new Axon(this.axonGeneMock, this.neuronMock);
			});

			it("should have the same strength as the axonGeneMock", function() {
				expect(this.sut.getStrength()).toEqual(this.axonGeneMock.strength);
			});

			it("should not have exited the target neuron", function() {
				expect(this.neuronMock.excite.calls.count()).toEqual(0);
			});

			describe("when the axon transmits", function() {
				beforeEach(function() {
					this.sut.transmit();
				});

				it("should have exited the target neuron once", function() {
					expect(this.neuronMock.excite.calls.count()).toEqual(1);
				});

				it("should exite the targetNeuron with the value of the axon strength", function() {
					expect(this.neuronMock.excite).toHaveBeenCalledWith(this.sut.getStrength());
				});
			});
		});
	});
});