define(['brains/options'], function(brainsOptions) {
	"use strict";

	describe("brains.Brain", function() {
		beforeEach(function() {
			brainsOptions.reset();

			this.brainGeneMock = function () {
				var layers = new Array(2 + Math.floor(Math.random() * 12));
				return {
					getLayers: function () {
						return layers;
					}
				};
			};

			var that = this;
			var Mock = function () {
				return {
					sense: function(input) {},
					transmit: function() {},
				};
			};

			this.inputLayerMock = new Mock();
			this.layerMock = new Mock();
			this.inputLayerCtor = function() {
				return that.inputLayerMock;
			};
			this.layerCtor = function() {
				return that.layerMock;
			};

			define('inputLayerMock', [], function () {
				return that.inputLayerCtor;
			});
			define('layerMock', [], function () {
				return that.layerCtor;
			});

			require.undef('brains/brain');
			require.config({
				map: {
					'brains/brain': {
						'brains/inputLayer': 'inputLayerMock',
						'brains/layer': 'layerMock'
					}
				}
			});
		});

		afterEach(function() {
			require.undef('inputLayerMock');
			require.undef('layerMock');
			require.config({
				map: {
					'brains/brain': {
						'brains/inputLayer': 'brains/inputLayer',
						'brains/layer': 'brains/layer'
					}
				}
			});
		});

		describe("when a brain is created with a brainGene", function() {
			beforeEach(function(done) {
				spyOn(this, 'layerCtor').and.callThrough();
				spyOn(this, 'inputLayerCtor').and.callThrough();

				this.brainGene = new this.brainGeneMock();

				var that = this;
				require(['brains/brain'], function(Brain) {
					that.sut = new Brain(that.brainGene);
					done();
				});
			});

			it("should be defined", function() {
				expect(this.sut).toBeDefined();
			});

			it("should have the same number of layers as the brainGene", function() {
				expect(this.sut.getLayers().length).toEqual(this.brainGene.getLayers().length);
			});

			it("calls the createLayer method for each layer in the brainGene", function() {
				expect(this.layerCtor.calls.count()).toEqual(this.brainGene.getLayers().length-1);
			});

			it("calls the createInputLayer method 1 time", function() {
				expect(this.inputLayerCtor.calls.count()).toEqual(1);
			});

			it("creates the first layer from the first brainGene layer", function() {
				expect(this.layerCtor.calls.argsFor(0)).toEqual([this.brainGene.getLayers()[0]]);
			});

			it("creates the last layer from the last brainGene layer and the previous layer as target layer", function() {
				var brainGeneLayers = this.brainGene.getLayers();
				var targetLayer = this.sut.getLayers()[this.sut.getLayers().length-2];
				expect(this.inputLayerCtor.calls.argsFor(0)).toEqual([brainGeneLayers[brainGeneLayers.length-1], targetLayer]);
			});

			describe("when the brain thinks", function() {
				beforeEach(function() {
					spyOn(this.layerMock, 'transmit');
					spyOn(this.inputLayerMock, 'transmit');
					spyOn(this.inputLayerMock, 'sense');

					this.inputValues = [13.3, 5.5, 17.7, 11.3];
					this.sut.think(this.inputValues);
				});

				it("excites the input neurons once", function() {
					expect(this.inputLayerMock.sense.calls.count()).toEqual(1);
				});

				it("excites the input layer with the input", function() {
					expect(this.inputLayerMock.sense.calls.argsFor(0)).toEqual([this.inputValues]);
				});

				it("transmits the to all layers", function() {
					expect(this.inputLayerMock.transmit.calls.count()).toEqual(1);
					expect(this.layerMock.transmit.calls.count()).toEqual(this.brainGene.getLayers().length-1);
				});
			});
		});
	});
});
