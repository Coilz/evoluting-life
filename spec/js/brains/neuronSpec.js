define(['brains/options'], function (brainsOptions, undefined) {
	"use strict";

	describe("brains.Neuron", function () {
		beforeEach(function () {
			brainsOptions.reset();

			this.neuronGeneMock = function () {
				var axonsMock = new Array(Math.floor(Math.random() * 12));
				return {
					axons: axonsMock,
					threshold: 0.2,
					relaxation: 0.1
				};
			};
			this.targetNeuronsMock = new Array(2 + Math.floor(Math.random() * 12));

			var that = this;
			var Mock = function () {
				return {
					transmit: function () { }
				};
			};
			this.axonMock = new Mock();
			this.axonCtor = function () { return that.axonMock; };

			define('axonMock', [], function () {
				return that.axonCtor;
			});

			require.undef('brains/neuron');
			require.config({
				map: {
					'brains/neuron': {
						'brains/axon': 'axonMock'
					}
				}
			});
		});

		afterEach(function () {
			require.undef('axonMock');
			require.config({
				map: {
					'brains/neuron': {
						'brains/axon': 'brains/axon'
					}
				}
			});
		});

		describe("when an output neuron is created with a gen", function () {
			beforeEach(function (done) {
				spyOn(this, 'axonCtor').and.callThrough();

				this.neuronGene = new this.neuronGeneMock();
				this.neuronGene.axons = new Array(0); // Output neurons don't have axons

				var that = this;
				require(['brains/neuron'], function (Neuron) {
					that.sut = new Neuron(that.neuronGene);
					done();
				});
			});

			it("should have the same number of axons as the neuronGene", function () {
				expect(this.sut.getAxons().length).toEqual(this.neuronGene.axons.length);
			});

			it("calls the createAxon the number times equal to the number of axons in the neuronGene", function () {
				expect(this.axonCtor.calls.count()).toEqual(this.neuronGene.axons.length);
			});

			it("should have the same value for threshold as the neuronGene", function () {
				expect(this.sut.getThreshold()).toEqual(this.neuronGene.threshold);
			});

			it("should have the same value for relaxation as the neuronGene", function () {
				expect(this.sut.getRelaxation()).toEqual(this.neuronGene.relaxation);
			});

			it("should have zero as excitation value", function () {
				expect(this.sut.excitation).toEqual(0);
			});

			describe("when a neuron gets excitated with a positive value", function () {
				beforeEach(function () {
					this.excitationDelta = this.sut.getThreshold() * 2 / 3;
					this.sut.excite(this.excitationDelta);
				});

				it("should have its excitation value increased by the value", function () {
					expect(this.sut.excitation).toEqual(this.excitationDelta);
				});

				describe("when the neuron transmits", function () {
					beforeEach(function () {
						this.expectedValue = this.sut.excitation * (1 - this.sut.getRelaxation());
						this.tramsmit = this.sut.transmit();
					});

					it("should not have transmitted", function () {
						expect(this.tramsmit).toBe(0);
					});

					it("should have its excitation value decreased by the relaxation factor", function () {
						expect(this.sut.excitation).toEqual(this.expectedValue);
					});

					it("should have a smaller excitation then before the relaxation", function () {
						expect(this.sut.excitation).not.toBeGreaterThan(this.excitationDelta);
					});
				});

				describe("when the neuron gets excitated with the value again", function () {
					beforeEach(function () {
						this.sut.excite(this.excitationDelta);
					});

					it("should have its excitation value increased by the value", function () {
						expect(this.sut.excitation).toEqual(this.excitationDelta * 2);
					});

					describe("when the neuron transmits", function () {
						beforeEach(function () {
							this.tramsmit = this.sut.transmit();
						});

						it("should have transmitted", function () {
							expect(this.tramsmit).toBeGreaterThan(0);
						});

						it("should have its excitation value set to zero", function () {
							expect(this.sut.excitation).toEqual(0);
						});
					});
				});
			});

			describe("when a neuron gets excitated with a negative value", function () {
				beforeEach(function () {
					this.sut.excitation = this.sut.getThreshold() * 1 / 2;
					this.excitationDelta = this.sut.getThreshold() * -3 / 8;
					this.expectedExcitation = this.sut.getThreshold() * 1 / 2 - this.sut.getThreshold() * 3 / 8;

					this.sut.excite(this.excitationDelta);
				});

				it("should have its excitation value decreased by the value", function () {
					expect(this.sut.excitation).toEqual(this.expectedExcitation);
				});

				describe("when the neuron transmits", function () {
					beforeEach(function () {
						this.expectedValue = this.sut.excitation * (1 - this.sut.getRelaxation());
						this.tramsmit = this.sut.transmit();
					});

					it("should not have transmitted", function () {
						expect(this.tramsmit).toBe(0);
					});

					it("should have its excitation value decreased by the relaxation factor", function () {
						expect(this.sut.excitation).toEqual(this.expectedValue);
					});

					it("should have a smaller excitation then before the relaxation", function () {
						expect(this.sut.excitation).not.toBeGreaterThan(this.expectedExcitation);
					});
				});

				describe("when the neuron gets excitated with the value again", function () {
					beforeEach(function () {
						this.newExpectedExcitation = this.expectedExcitation - this.sut.getThreshold() * 3 / 8;
						this.sut.excite(this.excitationDelta);
					});

					it("should have its excitation value is negative", function () {
						expect(this.sut.excitation).toEqual(this.newExpectedExcitation);
					});

					describe("when the neuron transmits again", function () {
						beforeEach(function () {
							this.tramsmit = this.sut.transmit();
						});

						it("should not have transmitted", function () {
							expect(this.tramsmit).toBe(0);
						});

						it("should have its excitation value is 0", function () {
							expect(this.sut.excitation).toEqual(0);
						});
					});
				});
			});
		});

		describe("when a neuron is created with a gen", function () {
			beforeEach(function (done) {
				spyOn(this, 'axonCtor').and.callThrough();

				this.neuronGene = new this.neuronGeneMock();

				var that = this;
				require(['brains/neuron'], function (Neuron) {
					that.sut = new Neuron(that.neuronGene);
					done();
				});
			});

			it("should have the same number of axons as the neuronGene", function () {
				expect(this.sut.getAxons().length).toEqual(this.neuronGene.axons.length);
			});

			it("calls the createAxon method for each axon in the neuronGene", function () {
				expect(this.axonCtor.calls.count()).toEqual(this.neuronGene.axons.length);
			});

			it("should have the same value for threshold as the neuronGene", function () {
				expect(this.sut.getThreshold()).toEqual(this.neuronGene.threshold);
			});

			it("should have the same value for relaxation as the neuronGene", function () {
				expect(this.sut.getRelaxation()).toEqual(this.neuronGene.relaxation);
			});

			it("should have zero as excitation value", function () {
				expect(this.sut.excitation).toEqual(0);
			});

			describe("when a neuron gets excitated with a value", function () {
				beforeEach(function () {
					this.excitationDelta = this.sut.getThreshold() * 2 / 3;
					this.sut.excite(this.excitationDelta);
				});

				it("should have its excitation value increased by the value", function () {
					expect(this.sut.excitation).toEqual(this.excitationDelta);
				});

				describe("when the neuron transmits", function () {
					beforeEach(function () {
						spyOn(this.axonMock, 'transmit');
						this.expectedValue = this.sut.excitation * (1 - this.sut.getRelaxation());
						this.tramsmit = this.sut.transmit();
					});

					it("should not have transmitted", function () {
						expect(this.tramsmit).toBe(0);
					});

					it("should have its excitation value decreased by the relaxation factor", function () {
						expect(this.sut.excitation).toEqual(this.expectedValue);
					});

					it("should have a smaller excitation then before the relaxation", function () {
						expect(this.sut.excitation).not.toBeGreaterThan(this.excitationDelta);
					});

					it("should not call transmit on any of tis axons", function () {
						expect(this.axonMock.transmit.calls.count()).toEqual(0);
					});
				});

				describe("when the neuron gets excitated with the value again", function () {
					beforeEach(function () {
						this.sut.excite(this.excitationDelta);
					});

					it("should have its excitation value increased by the value", function () {
						expect(this.sut.excitation).toEqual(this.excitationDelta * 2);
					});

					describe("when the neuron transmits", function () {
						beforeEach(function () {
							spyOn(this.axonMock, 'transmit');
							this.tramsmit = this.sut.transmit();
						});

						it("should have transmitted", function () {
							expect(this.tramsmit).toBeGreaterThan(0);
						});

						it("should have its excitation value set to zero", function () {
							expect(this.sut.excitation).toEqual(0);
						});

						it("should call transmit on each of tis axons", function () {
							expect(this.axonMock.transmit.calls.count()).toEqual(this.neuronGene.axons.length);
						});
					});
				});
			});
		});
		});
});