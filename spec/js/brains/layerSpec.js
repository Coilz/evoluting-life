define(['brains/options'], function(brainsOptions, undefined) {
	"use strict";

	describe("brains.Layer", function() {
		beforeEach(function() {
			brainsOptions.reset();

			this.inputCount = 4;
			this.outputCount = 3;

			var that = this;
			var Mock = function () {
				return {
					excite: function() {},
					transmit: function() {}
				};
			};
			this.neuronMock	= new Mock();
			this.neuronCtor	= function() { return that.neuronMock; };

			define('neuronMock', [], function () {
				return that.neuronCtor;
			});

			require.undef('brains/layer');
			require.config({
				map: {
					'brains/layer': {
						'brains/neuron': 'neuronMock'
					}
				}
			});
		});

		afterEach(function() {
			require.undef('neuronMock');
			require.config({
				map: {
					'brains/layer': {
						'brains/neuron': 'brains/neuron'
					}
				}
			});
		});

		describe("when a layer is created with a brainGene layer", function() {
			beforeEach(function(done) {
				spyOn(this, 'neuronCtor').and.callThrough();

				this.brainGeneLayerMock = new Array(4);

				var that = this;
				require(['brains/layer'], function(Layer) {
					that.sut = new Layer(that.brainGeneLayerMock);
					done();
				});
			});

			it("should have the same number of neurons as the brainGene layer", function() {
				expect(this.sut.getNeurons().length).toEqual(this.brainGeneLayerMock.length);
			});

			it("calls the createNeuron method for each layer in the brainGene", function() {
				expect(this.neuronCtor.calls.count()).toEqual(this.brainGeneLayerMock.length);
			});

			it("creates the first neuron from the first gene", function() {
				expect(this.neuronCtor.calls.argsFor(0)).toEqual([this.brainGeneLayerMock[0], undefined]);
			});

			describe("when the layer transmits", function() {
				beforeEach(function() {
					spyOn(this.neuronMock, 'transmit');
					this.excitations = this.sut.transmit();
				});

				it("calls transmit on each of its neurons", function() {
					expect(this.neuronMock.transmit.calls.count()).toEqual(this.brainGeneLayerMock.length);
				});

				it("returns an array of the excitation of its neurons", function() {
					expect(this.excitations.length).toEqual(this.brainGeneLayerMock.length);
				});
			});
		});

		describe("when a layer is created with a brainGene layer and a target layer", function() {
			beforeEach(function(done) {
				spyOn(this, 'neuronCtor').and.callThrough();

				this.brainGeneLayerMock = new Array(4);
				this.targetLayerMock = {
					getNeurons: function () {}
				};

				var that = this;
				require(['brains/layer'], function(Layer) {
					that.sut = new Layer(that.brainGeneLayerMock, that.targetLayerMock);
					done();
				});
			});

			it("should have the same number of neurons as the brainGene layer", function() {
				expect(this.sut.getNeurons().length).toEqual(this.brainGeneLayerMock.length);
			});

			it("calls the createNeuron method for each layer in the brainGene", function() {
				expect(this.neuronCtor.calls.count()).toEqual(this.brainGeneLayerMock.length);
			});

			it("creates the first neuron from the first gene and the neurons from the targetLayer", function() {
				expect(this.neuronCtor.calls.argsFor(0)).toEqual([this.brainGeneLayerMock[0], this.targetLayerMock.getNeurons()]);
			});
		});
	});
});
