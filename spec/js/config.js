(function () {
    require.config({
        baseUrl: "../app/js/",
    //    urlArgs: 'cb=' + Math.random(),
        paths: {
            'jasmine': '../../spec/libs/jasmine-2.4.1/jasmine',
            'jasmine-html': '../../spec/libs/jasmine-2.4.1/jasmine-html',
            'boot': '../../spec/libs/jasmine-2.4.1/boot'
        },
        shim: {
            'jasmine': {
                exports: 'window.jasmineRequire'
            },
            'jasmine-html': {
                deps: ['jasmine'],
                exports: 'window.jasmineRequire'
            },
            'boot': {
                deps: ['jasmine', 'jasmine-html'],
                exports: 'window.jasmineRequire'
            }
        }
    });

    var specBaseUrl = '../../spec/js/';
    var specs = [
        'brains/axonSpec',
        'brains/brainSpec',
        'brains/inputLayerSpec',
        'brains/layerSpec',
        'brains/neuronSpec',

        'display/countersCollectorSpec',
        'display/statsCollectorSpec',

        'evolution/rouletteWheelSelectionByFitnessSpec',

        'entities/plantSpec',

        'genetics/axonGeneSpec',
        'genetics/brainGeneSpec',
        'genetics/geneticsSpec',
        'genetics/genomeSpec',
        'genetics/lifeGeneSpec',
        'genetics/movementGeneSpec',
        'genetics/neuronGeneSpec',
        'genetics/sensorGeneSpec',

        'observe/observerListSpec',
        'observe/observerSpec',
        'observe/subjectSpec',

        'sensors/eyesSpec',

        'utils/cloningSpec',
        'utils/permutationsSpec',
        'utils/rangeSpec'
    ];

    for (var i=0; i<specs.length; i++) {
        specs[i] = specBaseUrl + specs[i];
    }

    require(['boot'], function () {
        require(specs, function () {
            window.onload();
        });
    });
})();
