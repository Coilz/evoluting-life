define(['sensors/options'], function(sensorsOptions) {
	"use strict";

	describe("sensors.Eyes", function() {
		beforeEach(function() {
			sensorsOptions.reset();
			this.positionMock = {
				x: 200,
				y: 200,
				a: Math.PI / 4
			};
			this.sensorGeneMock = {
				viewDistance: 100,
				fieldOfView: Math.PI
			};

			var Mock = function () {
				return {
					width: 400,
					height: 400
				};
			};

			define('worldCanvasMock', [], function () {
				return new Mock();
			});

			require.undef('sensors/eyes');
			require.config({
				map: {
					'sensors/eyes': {
						'worldCanvas': 'worldCanvasMock'
					}
				}
			});
		});

		afterEach(function() {
			require.undef('worldCanvasMock');

			require.config({
				map: {
					'sensors/eyes': {
						'worldCanvas': 'worldCanvas'
					}
				}
			});
		});

		describe("when an sensors.Eyes is created with a SensorGene", function() {
			beforeEach(function(done) {
				this.owner = {position: this.positionMock};

				var that = this;
				require(['sensors/eyes'], function(Eyes) {
					that.sut = new Eyes(that.owner, that.sensorGeneMock);
					done();
				});
			});

			it("should have a position that was given", function() {
				expect(this.sut.getPosition()).toBe(this.positionMock);
			});

			it("should have the viewDistance of the SensorGene", function() {
				expect(this.sut.viewDistance).toEqual(this.sensorGeneMock.viewDistance);
			});

			it("should have the fieldOfView of the SensorGene", function() {
				expect(this.sut.fieldOfView).toEqual(this.sensorGeneMock.fieldOfView);
			});

			describe("when no organisms are defined", function() {
				beforeEach(function() {
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(undefined, undefined);
					});

					it("should see nothing", function() {
						expect(this.sensed.animals).not.toBe(undefined);
						expect(this.sensed.plants).not.toBe(undefined);

						expect(this.sensed.animals.length).toEqual(0);
						expect(this.sensed.plants.length).toEqual(0);
					});
				});
			});

			describe("when there are no organisms", function() {
				beforeEach(function() {
					this.animals = [];
					this.plants = [];
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(this.animals, this.plants);
					});

					it("should see nothing", function() {
						expect(this.sensed.animals).not.toBe(undefined);
						expect(this.sensed.plants).not.toBe(undefined);

						expect(this.sensed.animals.length).toEqual(0);
						expect(this.sensed.plants.length).toEqual(0);
					});
				});
			});

			describe("when there are no organisms besides the owner", function() {
				beforeEach(function() {
					this.animals = [this.owner];
					this.plants = [];
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(this.animals, this.plants);
					});

					it("should see nothing", function() {
						expect(this.sensed.animals).not.toBe(undefined);
						expect(this.sensed.plants).not.toBe(undefined);

						expect(this.sensed.animals.length).toEqual(0);
						expect(this.sensed.plants.length).toEqual(0);
					});
				});
			});

			describe("when there are organisms past the viewDistance", function() {
				beforeEach(function() {
					this.animals = [{
						position: {x:100, y:300}
					}];
					this.plants = [{
						position: {x:300, y:100}
					}];
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(this.animals, this.plants);
					});

					it("should see nothing", function() {
						expect(this.sensed.animals).not.toBe(undefined);
						expect(this.sensed.plants).not.toBe(undefined);

						expect(this.sensed.animals.length).toEqual(0);
						expect(this.sensed.plants.length).toEqual(0);
					});
				});
			});

			describe("when there are organisms within the viewDistance, but out of the fieldOfView", function() {
				beforeEach(function() {
					this.animals = [{
						position: {x:149, y:250}
					}];
					this.plants = [{
						position: {x:250, y:149}
					}];
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(this.animals, this.plants);
					});

					it("should see nothing", function() {
						expect(this.sensed.animals).not.toBe(undefined);
						expect(this.sensed.plants).not.toBe(undefined);

						expect(this.sensed.animals.length).toEqual(0);
						expect(this.sensed.plants.length).toEqual(0);
					});
				});
			});

			describe("when there are organisms within the viewDistance and within the fieldOfView", function() {
				beforeEach(function() {
					var diff = Math.sqrt(this.sensorGeneMock.viewDistance*this.sensorGeneMock.viewDistance/2);

					this.animals = [{
						position: {
							x: this.positionMock.x - diff,
							y: this.positionMock.y + diff
						}
					}];
					this.plants = [{
						position: {
							x: this.positionMock.x + diff,
							y: this.positionMock.y - diff
						}
					}];
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(this.animals, this.plants);
					});

					it("should see all", function() {
						expect(this.sensed.animals).not.toBe(undefined);
						expect(this.sensed.plants).not.toBe(undefined);

						expect(this.sensed.animals.length).toEqual(1);
						expect(this.sensed.plants.length).toEqual(1);
					});

					it("should have angle PI/2 and -PI/2", function() {
						expect(this.sensed.animals[0].angle).toEqual(Math.PI/2);
						expect(this.sensed.plants[0].angle).toEqual(-1*Math.PI/2);
					});

					it("should have distance 100", function() {
						expect(this.sensed.animals[0].distance).toEqual(100);
						expect(this.sensed.plants[0].distance).toEqual(100);
					});
				});
			});

			describe("when there are multiple organisms within the viewDistance and within the fieldOfView", function() {
				beforeEach(function() {
					var diff = Math.sqrt(this.sensorGeneMock.viewDistance*this.sensorGeneMock.viewDistance/2);
					
					this.animals = [{
						name: "far",
						position: {
							x: this.positionMock.x - diff,
							y: this.positionMock.y + diff
						}
					},{
						name: "close",
						position: {
							x: this.positionMock.x - diff + 1,
							y: this.positionMock.y + diff - 1
						}
					}];
					this.plants = [{
						name: "far",
						position: {
							x: this.positionMock.x + diff,
							y: this.positionMock.y - diff
						}
					},{
						name: "close",
						position: {
							x: this.positionMock.x + diff - 1,
							y: this.positionMock.y - diff + 1
						}
					}];
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(this.animals, this.plants);
					});

					it("should see the closest organisms first", function() {
						expect(this.sensed.animals[0].organism.name).toEqual("close");
						expect(this.sensed.plants[0].organism.name).toEqual("close");

						expect(this.sensed.animals[1].organism.name).toEqual("far");
						expect(this.sensed.plants[1].organism.name).toEqual("far");
					});
				});
			});

			describe("when far from the wall and not facing it", function() {
				beforeEach(function() {
					this.positionMock.x = 200;
					this.positionMock.y = 200;
					this.positionMock.a = 0;
				});

				describe("when the eyes sense", function() {
					beforeEach(function() {
						this.sensed = this.sut.sense(undefined, undefined);
					});

					it("should have distance equal to the viewDistance", function() {
						expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
					});
				});
			});

			describe("when close to the left wall", function() {
				beforeEach(function() {
					this.positionMock.x = 2;
					this.positionMock.y = 200;
				});

				describe("when not facing it", function() {
					beforeEach(function() {
						this.positionMock.a = 0;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to the viewDistance", function() {
							expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
						});
					});
				});

				describe("when facing it", function() {
					beforeEach(function() {
						this.positionMock.a = Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance).toEqual(2);
						});
					});
				});

				describe("when facing it from front/left", function() {
					beforeEach(function() {
						this.positionMock.a = 0.75*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance.toFixed(10)).toEqual(Math.sqrt(8).toFixed(10));
						});
					});
				});

				describe("when looking parallel to it, facing the top wall", function() {
					beforeEach(function() {
						this.positionMock.a = 0.5*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
						});
					});
				});

				describe("when facing it from front/right", function() {
					beforeEach(function() {
						this.positionMock.a = -0.75*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance.toFixed(10)).toEqual(Math.sqrt(8).toFixed(10));
						});
					});
				});

				describe("when looking parallel to it, facing the bottom wall", function() {
					beforeEach(function() {
						this.positionMock.a = -0.5*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
						});
					});
				});
			});

			describe("when close to the right wall", function() {
				beforeEach(function() {
					this.positionMock.x = 398;
					this.positionMock.y = 200;
				});

				describe("when not facing it", function() {
					beforeEach(function() {
						this.positionMock.a = Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to the viewDistance", function() {
							expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
						});
					});
				});

				describe("when facing it", function() {
					beforeEach(function() {
						this.positionMock.a = 0;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance).toEqual(2);
						});
					});
				});

				describe("when facing it from front/left", function() {
					beforeEach(function() {
						this.positionMock.a = -0.25*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance.toFixed(10)).toEqual(Math.sqrt(8).toFixed(10));
						});
					});
				});

				describe("when looking parallel to it, facing the bottom wall", function() {
					beforeEach(function() {
						this.positionMock.a = -0.5*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
						});
					});
				});

				describe("when facing it from front/right", function() {
					beforeEach(function() {
						this.positionMock.a = 0.25*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance.toFixed(10)).toEqual(Math.sqrt(8).toFixed(10));
						});
					});
				});

				describe("when looking parallel to it, facing the top wall", function() {
					beforeEach(function() {
						this.positionMock.a = 0.5*Math.PI;
					});

					describe("when the eyes sense", function() {
						beforeEach(function() {
							this.sensed = this.sut.sense(undefined, undefined);
						});

						it("should have distance equal to 2", function() {
							expect(this.sensed.wall.distance).toEqual(this.sensorGeneMock.viewDistance);
						});
					});
				});
			});
		});
	});
});