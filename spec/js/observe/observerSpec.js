define(['observe/Observer'], function(Observer) {
    "use strict";

    describe("observe.Observer", function() {
        describe("when a consumer creates an observer", function() {
            beforeEach(function() {
                this.consumer = {
                    cb: function () {
                    }
                };

                spyOn(this.consumer, 'cb');

                this.sut = new Observer(this.consumer.cb);
            });

            describe("when update on the observer is called", function() {
                beforeEach(function() {
                    this.expectedContext = 1;
                    this.sut.update(this.expectedContext);
                });

                it("should call the function on the client", function() {
                    expect(this.consumer.cb).toHaveBeenCalledWith(this.expectedContext);
                });
            });
        });
    });
});