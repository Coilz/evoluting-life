define(['observe/Subject'], function(Subject) {
    "use strict";

    describe("observe.Subject", function() {
        beforeEach(function() {
            this.sut = new Subject();
        });

        describe("when an observer has been added", function() {
            beforeEach(function() {
                this.anObserver = {};
                this.sut.addObserver(this.anObserver);
            });

            describe("when a notification is sent", function() {
                beforeEach(function(done) {
                    this.expectedContext = 1;
                    this.anObserver.update = function(context) {
                        done();
                    };

                    spyOn(this.anObserver, 'update').and.callThrough();

                    this.sut.notifyAsync(this.expectedContext);
                });

                it("should notify the observer", function() {
                    expect(this.anObserver.update).toHaveBeenCalledWith(this.expectedContext);
                });
            });

            describe("when the specific observer is removed", function() {
                beforeEach(function() {
                    this.sut.removeObserver(this.anObserver);
                });

                describe("when a notification is sent", function() {
                    beforeEach(function(done) {
                        this.expectedContext = 1;
                        this.anObserver.update = function(context) {
                            done();
                        };

                        spyOn(this.anObserver, 'update').and.callThrough();

                        this.sut.notifyAsync(this.expectedContext);

                        setTimeout(function() {
                            done();
                        }, 10);
                    });

                    it("should not notify the observer anymore when a notification is sent", function() {
                        expect(this.anObserver.update).not.toHaveBeenCalled();
                    });
                });
            });
        });
    });
});