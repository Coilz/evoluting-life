define(['observe/observerList'], function(ObserverList) {
	"use strict";

	describe("observe.ObserverList", function() {
		beforeEach(function() {
			this.sut = new ObserverList();
		});

		describe("when no items have been added", function() {
			it("should indicate that the list is empty", function() {
				expect(this.sut.count()).toEqual(0);
			});
		});

		describe("when an item has been added", function() {
			beforeEach(function() {
				this.addItem = {};
				this.sut.add(this.addItem);
			});

			it("should indicate that the list has elements", function() {
				expect(this.sut.count()).toBeGreaterThan(0);
			});

			it("should return the object when it is asked for", function() {
				expect(this.sut.get(0)).toEqual(this.addItem);
			});

			it("should give the index of the object when it is asked for", function() {
				expect(this.sut.indexOf(this.addItem, 0)).toEqual(0);
			});

			describe("when the item is removed", function() {
				beforeEach(function() {
					this.sut.remove(this.addItem);
				});

				it("should indicate that the list is empty", function() {
					expect(this.sut.count()).toEqual(0);
				});
			});

			describe("when the item is removed by index", function() {
				beforeEach(function() {
					this.sut.removeAt(0);
				});

				it("should indicate that the list is empty", function() {
					expect(this.sut.count()).toEqual(0);
				});
			});
		});

		describe("when 4 items have been added", function() {
			beforeEach(function() {
				this.sut.add({});
				this.sut.add({});

				this.addItem = {};
				this.sut.add(this.addItem);
				this.sut.add({});
			});

			it("should indicate that the list has 4 elements", function() {
				expect(this.sut.count()).toEqual(4);
			});

			it("should return the object when it is asked for", function() {
				expect(this.sut.get(2)).toEqual(this.addItem);
			});

			it("should give the index of the object when it is asked for", function() {
				expect(this.sut.indexOf(this.addItem, 0)).toEqual(2);
			});

			describe("when a specific item is removed", function() {
				beforeEach(function() {
					this.sut.remove(this.addItem);
				});

				it("should indicate that the list has 3 items", function() {
					expect(this.sut.count()).toEqual(3);
				});

				it("should indicate that the specific item has been removed", function() {
					expect(this.sut.indexOf(this.addItem, 0)).toEqual(-1);
				});
			});
		});
	});
});
