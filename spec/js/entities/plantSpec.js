define(['genetics/options', 'entities/options'], function (geneticsOptions, entitiesOptions) {
	"use strict";

	describe("entities.Plant", function () {
		beforeEach(function () {
			geneticsOptions.reset();
			entitiesOptions.reset();

			this.genomeMock = {
        getLife: function () {
          return {
            oldAge: 2000,
            nutrition: 1.0
          };
        }
      };

			var Mock = function () {
				return {
					width: 400,
					height: 400
				};
			};

			define('worldCanvasMock', [], function () {
				return new Mock();
			});

			require.undef('entities/plant');
			require.undef('entities/organism');
			require.config({
				map: {
					'entities/organism': {
						'worldCanvas': 'worldCanvasMock'
					}
				}
			});

			this.position = { x: 3, y: 7 };
		});

		afterEach(function () {
			require.undef('worldCanvasMock');

			require.config({
				map: {
					'entities/organism': {
						'worldCanvas': 'worldCanvas'
					}
				}
			});
		});

		describe("when a plant is created with a position", function () {
			beforeEach(function (done) {
				var that = this;
				require(['entities/plant'], function (Plant) {
					that.sut = new Plant(that.genomeMock, that.position);
					done();
				});
			});

			it("the position of the plant should be the given position", function () {
				expect(this.sut.position).toEqual(this.position);
			});

			it("the size should be greater or equal to the minFoodSize", function () {
				expect(this.sut.getSize()).not.toBeLessThan(entitiesOptions.plant.minFoodSize.get());
			});

			it("the size should be smaller or equal to the maxFoodSize", function () {
				expect(this.sut.getSize()).not.toBeGreaterThan(entitiesOptions.plant.maxFoodSize.get());
			});

			describe("when the plant is consumed", function () {
				beforeEach(function () {
					this.size = this.sut.getSize();
					this.consumed = this.sut.consume();
				});

				it("the size has decreased with 1", function () {
					expect(this.sut.getSize()).toEqual(this.size - 1);
				});

				it("the consumed energy is equal to the nutrition", function () {
					expect(this.consumed).toEqual(this.sut.getNutrition());
				});
			});

			describe("when the plant lives one iteration", function () {
				beforeEach(function () {
					this.age = this.sut.life.age;
					this.sut.run();
				});

				it("the age should have increased with 1", function () {
					expect(this.sut.life.age).toEqual(this.age + 1);
				});

				it("it will be alive", function () {
					expect(this.sut.lives()).toEqual(true);
				});
			});

			describe("when the plant has reached oldAge", function () {
				beforeEach(function () {
					this.sut.life.age = this.genomeMock.getLife().oldAge;
				});

				describe("when the plant lives one iteration", function () {
					beforeEach(function () {
						this.age = this.sut.life.age;
						this.sut.run();
					});

					it("the age should have increased with 1", function () {
						expect(this.sut.life.age).toEqual(this.age + 1);
					});

					describe("when Math.random always returns 0.005", function () {
						beforeEach(function () {
							this.rnd = Math.random;
							Math.random = function () { return 0.005; };
						});

						afterEach(function () {
							Math.random = this.rnd;
						});

						it("it will be dead", function () {
							expect(this.sut.lives()).toEqual(false);
						});
					});

					describe("when Math.random always returns 0.015", function () {
						beforeEach(function () {
							this.rnd = Math.random;
							Math.random = function () { return 0.015; };
						});

						afterEach(function () {
							Math.random = this.rnd;
						});

						it("it will be alive", function () {
							expect(this.sut.lives()).toEqual(true);
						});
					});
				});
			});
		});

		describe("when minFoodSize is 1, maxFoodSize is 1, minGrowthPercentage is 0 and maxGrowthPercentage is 0", function () {
			beforeEach(function () {
				entitiesOptions.plant.minFoodSize.set(1);
				entitiesOptions.plant.maxFoodSize.set(1);
				entitiesOptions.plant.minGrowthPercentage.set(0);
				entitiesOptions.plant.maxGrowthPercentage.set(0);
			});

			describe("when a plant is created", function () {
				beforeEach(function (done) {
					var that = this;
					require(['entities/plant'], function (Plant) {
						that.sut = new Plant(that.genomeMock, that.position);
						done();
					});
				});

				it("the size should be 1", function () {
					expect(this.sut.getSize()).toEqual(1);
				});

				it("it will be alive", function () {
					expect(this.sut.lives()).toEqual(true);
				});

				describe("when the plant is consumed", function () {
					beforeEach(function () {
						this.sut.consume();
					});

					it("the size should have decreased with 1", function () {
						expect(this.sut.getSize()).toEqual(0);
					});

					it("it will be dead", function () {
						expect(this.sut.lives()).toEqual(false);
					});
				});

				describe("when the plant lives one iteration", function () {
					beforeEach(function () {
						this.sut.run();
					});

					it("the size should not have increased", function () {
						expect(this.sut.getSize()).toEqual(1);
					});
				});
			});
		});

		describe("when minFoodSize is 1, maxFoodSize is 9, minGrowthPercentage is 1 and maxGrowthPercentage is 17", function () {
			beforeEach(function () {
				entitiesOptions.plant.minFoodSize.set(1);
				entitiesOptions.plant.maxFoodSize.set(9);
				entitiesOptions.plant.minGrowthPercentage.set(1);
				entitiesOptions.plant.maxGrowthPercentage.set(17);
			});

			describe("when Math.random always returns 0.5", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.5; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when a plant is created", function () {
					beforeEach(function (done) {
						var that = this;
						require(['entities/plant'], function (Plant) {
							that.sut = new Plant(that.genomeMock, that.position);
							done();
						});
					});

					it("the size should be 5", function () {
						expect(this.sut.getSize()).toEqual(5);
					});

					it("the q should be 5/9", function () {
						expect(this.sut.life.healthN()).toEqual(5 / 9);
					});

					describe("when the plant lives one iteration", function () {
						beforeEach(function () {
							this.sut.run();
						});

						it("the size should have increased with 9%", function () {
							expect(this.sut.getSize()).toEqual(5 * 109 / 100);
						});

						it("the q should be 5/9 increased with 9%", function () {
							expect(this.sut.life.healthN()).toEqual(5 * 109 / 100 / 9);
						});
					});
				});
			});

			describe("when Math.random always returns 0.25", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.25; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when a plant is created", function () {
					beforeEach(function (done) {
						var that = this;
						require(['entities/plant'], function (Plant) {
							that.sut = new Plant(that.genomeMock, that.position);
							done();
						});
					});

					it("the size should be 3", function () {
						expect(this.sut.getSize()).toEqual(3);
					});

					it("the q should be 3/9", function () {
						expect(this.sut.life.healthN()).toEqual(3 / 9);
					});

					describe("when the plant lives one iteration", function () {
						beforeEach(function () {
							this.sut.run();
						});

						it("the size should have increased with 5%", function () {
							expect(this.sut.getSize()).toEqual(3 * 105 / 100);
						});

						it("the q should be 3/9 increased with 5%", function () {
							expect(this.sut.life.healthN()).toEqual(3 * 105 / 100 / 9);
						});
					});
				});
			});

			describe("when Math.random always returns 0.75", function () {
				beforeEach(function () {
					this.rnd = Math.random;
					Math.random = function () { return 0.75; };
				});

				afterEach(function () {
					Math.random = this.rnd;
				});

				describe("when a plant is created", function () {
					beforeEach(function (done) {
						var that = this;
						require(['entities/plant'], function (Plant) {
							that.sut = new Plant(that.genomeMock, that.position);
							done();
						});
					});

					it("the size should be 7", function () {
						expect(this.sut.getSize()).toEqual(7);
					});

					it("the q should be 7/9", function () {
						expect(this.sut.life.healthN()).toEqual(7 / 9);
					});

					describe("when the plant lives one iteration", function () {
						beforeEach(function () {
							this.sut.run();
						});

						it("the size should have increased with 13%", function () {
							expect(this.sut.getSize()).toEqual(7 * 113 / 100);
						});

						it("the q should be 7/9 increased with 13%", function () {
							expect(this.sut.life.healthN()).toEqual(7 * 113 / 100 / 9);
						});
					});
				});
			});

			xdescribe("when the plant is cloned", function () {
				beforeEach(function (done) {
					var that = this;
					require(['entities/plant'], function (Plant) {
						that.sut = new Plant(that.genomeMock, that.position);
						that.clone = that.sut.clone();
						done();
					});
				});

				it("the clone should not be the same object as the source", function () {
					expect(this.clone).not.toBe(this.sut);
				});

				it("the clone should have the same strength", function () {
					expect(this.clone.strength).toEqual(this.sut.strength);
				});
			});
		});

		describe("when a plant is created", function () {
			beforeEach(function (done) {
				geneticsOptions.life.minNutrition.set(0);
				geneticsOptions.life.maxNutrition.set(2);

				var that = this;
				require(['entities/plant'], function (Plant) {
					that.sut = new Plant(that.genomeMock, that.position);
					done();
				});
			});

			it("the nutrition should be 1", function () {
				expect(this.sut.getNutrition()).toEqual(1);
			});

			it("the normalized nutrition should be 0.5", function () {
				expect(this.sut.getNutritionN()).toEqual(0.5);
			});
		});
	});
});