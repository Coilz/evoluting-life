define(['utils/permutations'], function(Permutations) {
	"use strict";

	describe("utils.Permutations", function() {
		beforeEach(function() {
		});

		describe("when a Permutations object is created with positionCount is 3 and two different objects", function() {
			beforeEach(function() {
				this.A = { value: "A" };
				this.B = { value: "B" };

				this.sut = new Permutations(this.A, this.B, 3);
			});

			describe("when Math.random always returns 3/Math.pow(2, 3)", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 3/Math.pow(2, 3);};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when getRandomPermutation is called", function() {
					beforeEach(function() {
						this.permutation = this.sut.getRandomPermutation();
					});

					it("should return a permutation with 3 positions", function() {
						expect(this.permutation.length).toBe(3);
					});

					it("should return a permutation with BAA", function() {
						// 0: BBB, 1: BBA, 2: BAB, 3: BAA, 4: ABB, 5: ABA, 6: AAB, 7: AAA
						expect(this.permutation[0].value).toBe("B");
						expect(this.permutation[1].value).toBe("A");
						expect(this.permutation[2].value).toBe("A");
					});
				});

				describe("when getRandomPermutationPair is called", function() {
					beforeEach(function() {
						this.permutationPair = this.sut.getRandomPermutationPair();
					});

					it("should return a permutationPair", function() {
						expect(this.permutationPair.length).toBe(2);
					});

					it("should return a permutation with ABB at the first position, BAA at the second position", function() {
						// 0: BBB, 1: BBA, 2: BAB, 3: BAA, 4: ABB, 5: ABA, 6: AAB, 7: AAA
						expect(this.permutationPair[0][0].value).toBe("A");
						expect(this.permutationPair[0][1].value).toBe("B");
						expect(this.permutationPair[0][2].value).toBe("B");

						expect(this.permutationPair[1][0].value).toBe("B");
						expect(this.permutationPair[1][1].value).toBe("A");
						expect(this.permutationPair[1][2].value).toBe("A");
					});
				});
			});

			describe("when Math.random always returns 5/Math.pow(2, 3)", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 5/Math.pow(2, 3);};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when getRandomPermutation is called", function() {
					beforeEach(function() {
						this.permutation = this.sut.getRandomPermutation();
					});

					it("should return a permutation with 3 positions", function() {
						expect(this.permutation.length).toBe(3);
					});

					it("should return a permutation with ABA", function() {
						// 0: BBB, 1: BBA, 2: BAB, 3: BAA, 4: ABB, 5: ABA, 6: AAB, 7: AAA
						expect(this.permutation[0].value).toBe("A");
						expect(this.permutation[1].value).toBe("B");
						expect(this.permutation[2].value).toBe("A");
					});
				});
			});

			describe("when Math.random always returns 7/Math.pow(2, 3)", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 7/Math.pow(2, 3);};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				describe("when getRandomPermutation is called", function() {
					beforeEach(function() {
						this.permutation = this.sut.getRandomPermutation();
					});

					it("should return a permutation with 3 positions", function() {
						expect(this.permutation.length).toBe(3);
					});

					it("should return a permutation with AAA", function() {
						// 0: BBB, 1: BBA, 2: BAB, 3: BAA, 4: ABB, 5: ABA, 6: AAB, 7: c
						expect(this.permutation[0].value).toBe("A");
						expect(this.permutation[1].value).toBe("A");
						expect(this.permutation[2].value).toBe("A");
					});
				});
			});
		});
	});
});