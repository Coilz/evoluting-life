define(['utils/range'], function(range) {
	"use strict";

	describe("utils.Range", function() {
		describe("when a range is created with a lower and upper bound", function() {
			beforeEach(function() {
				this.lower = 1;
				this.upper = 5;
				this.sut = range(this.lower, this.upper);
			});

			describe("when a value within the range is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(3);
				});

				it("should return the value", function() {
					expect(this.checkedValue).toEqual(3);
				});
			});

			describe("when a value larger than the upper bound is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(6);
				});

				it("should return the upper value", function() {
					expect(this.checkedValue).toEqual(this.upper);
				});
			});

			describe("when a value smaller than the lower bound is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(0);
				});

				it("should return the lower value", function() {
					expect(this.checkedValue).toEqual(this.lower);
				});
			});

			describe("when Math.random always returns 0.5", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.5;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				it("should return 3 for a random number", function() {
					expect(this.sut.random()).toEqual(3);
				});

				it("should return 0 for a mutation", function() {
					expect(this.sut.mutation(1.0)).toEqual(0);
				});
			});

			describe("when Math.random always returns 0.25", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.25;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				it("should return 2 for a random number", function() {
					expect(this.sut.random()).toEqual(2);
				});

				it("should return -2 for a mutation", function() {
					expect(this.sut.mutation(1.0)).toEqual(-2); // (5-4)*0.25 range(-1, 1)=4*-0.5
				});
			});
		});

		describe("when a range is created with the lower bound undefined", function() {
			beforeEach(function() {
				this.upper = 4;
				this.sut = range(this.lower, this.upper);
			});

			describe("when a value within the range is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(3);
				});

				it("should return the value", function() {
					expect(this.checkedValue).toEqual(3);
				});
			});

			describe("when a value larger than the upper bound is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(6);
				});

				it("should return the upper value", function() {
					expect(this.checkedValue).toEqual(this.upper);
				});
			});

			describe("when a value smaller than the lower bound is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(0);
				});

				it("should return the lower value", function() {
					expect(this.checkedValue).toEqual(0);
				});
			});

			describe("when Math.random always returns 0.5", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.5;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				it("should return 2 for a random number", function() {
					expect(this.sut.random()).toEqual(2);
				});
			});

			describe("when Math.random always returns 0.25", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.25;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				it("should return 1 for a random number", function() {
					expect(this.sut.random()).toEqual(1);
				});
			});
		});

		describe("when a range is created with the upper bound undefined", function() {
			beforeEach(function() {
				this.lower = -4;
				this.sut = range(this.lower, this.upper);
			});

			describe("when a value within the range is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(-3);
				});

				it("should return the value", function() {
					expect(this.checkedValue).toEqual(-3);
				});
			});

			describe("when a value larger than the upper bound is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(6);
				});

				it("should return the upper value", function() {
					expect(this.checkedValue).toEqual(0);
				});
			});

			describe("when a value smaller than the lower bound is checked", function() {
				beforeEach(function() {
					this.checkedValue = this.sut.check(-5);
				});

				it("should return the lower value", function() {
					expect(this.checkedValue).toEqual(this.lower);
				});
			});

			describe("when Math.random always returns 0.5", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.5;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				it("should return -2 for a random number", function() {
					expect(this.sut.random()).toEqual(-2);
				});
			});

			describe("when Math.random always returns 0.25", function() {
				beforeEach(function() {
					this.rnd = Math.random;
					Math.random = function() {return 0.25;};
				});

				afterEach(function() {
					Math.random = this.rnd;
				});

				it("should return -3 for a random number", function() {
					expect(this.sut.random()).toEqual(-3);
				});
			});
		});

		describe("when a range is created with a lower bound greater than the upper bound", function() {
			beforeEach(function() {
				this.throwsAnError = function() {
					range(5, 1);
				};
			});

			it("should throw an error", function() {
				expect(this.throwsAnError).toThrow();
			});
		});
	});
});