define(['utils/cloning'], function(utils) {
	"use strict";

	describe("utils.cloning", function() {
		beforeEach(function() {
			this.source = {
				a: 1,
				b: 2,
				c: [3,4,5]
			};
		});

		describe("when an object is cloned", function() {
			beforeEach(function() {
				this.clone = utils.clone(this.source);
			});

			it("the clone is not the same object as the source", function() {
				expect(this.clone).not.toBe(this.source);
			});

			it("the value-type properties of the clone have the same values as the source", function() {
				expect(this.clone.a).toBe(this.source.a);
				expect(this.clone.b).toBe(this.source.b);
			});

			it("the reference-type properties of the clone are not the same as the source", function() {
				expect(this.clone.c).not.toBe(this.source.c);
			});

			it("the properties of the clone that are arrays have the same length", function() {
				expect(this.clone.c.length).toBe(this.source.c.length);
			});

			it("the properties of the clone that are arrays have the same values", function() {
				for (var i = 0; i < this.clone.c.length; i++) {
					expect(this.clone.c[i]).toBe(this.source.c[i]);
				}
			});

			describe("when the values of items of an array-property of the clone change", function() {
				beforeEach(function() {
					for (var i = 0; i < this.clone.c.length; i++) {
						this.clone.c[i]++;
					}
				});

				it("should not affect the values in the source array", function() {
					for (var i = 0; i < this.clone.c.length; i++) {
						expect(this.clone.c[i]).not.toBe(this.source.c[i]);
					}
				});
			});
		});

		describe("when the properties of an object are copied", function() {
			beforeEach(function() {
				this.destination = {
					d: 3,
					e: 4
				};
				this.destination = utils.mixin(this.source, this.destination, utils.clone);
			});

			it("the destination is not the same as the source", function() {
				expect(this.destination).not.toBe(this.source);
			});

			it("the value-type properties of the destination have the same values as the source", function() {
				expect(this.destination.a).toBe(this.source.a);
				expect(this.destination.b).toBe(this.source.b);
			});

			it("the reference-type properties of the destination are not the same as the source", function() {
				expect(this.destination.c).not.toBe(this.source.c);
			});

			it("the properties of the destination that are arrays have the same length", function() {
				expect(this.destination.c.length).toBe(this.source.c.length);
			});

			it("the properties of the clone that are arrays have the same values", function() {
				for (var i = 0; i < this.destination.c.length; i++) {
					expect(this.destination.c[i]).toBe(this.source.c[i]);
				}
			});

			it("the other properties of the destination are left alone", function() {
				expect(this.destination.d).toBeDefined();
				expect(this.destination.e).toBeDefined();
			});

			describe("when the values of items of an array-property of the destination change", function() {
				beforeEach(function() {
					for (var i = 0; i < this.destination.c.length; i++) {
						this.destination.c[i]++;
					}
				});

				it("should not affect the values in the source array", function() {
					for (var i = 0; i < this.destination.c.length; i++) {
						expect(this.destination.c[i]).not.toBe(this.source.c[i]);
					}
				});
			});
		});

		describe("when an object is shallow cloned", function() {
			beforeEach(function() {
				this.clone = utils.shallowClone(this.source);
			});

			it("the clone is not the same object as the source", function() {
				expect(this.clone).not.toBe(this.source);
			});

			it("the properties of the shallow clone have the same values", function() {
				expect(this.clone.a).toBe(this.source.a);
				expect(this.clone.b).toBe(this.source.b);
			});

			it("the reference-type properties of the clone are the same as the source", function() {
				expect(this.clone.c).toBe(this.source.c);
			});

			it("the properties of the shallow clone that are arrays have the same length", function() {
				expect(this.clone.c.length).toBe(this.source.c.length);
			});

			it("the properties of the shallow clone that are arrays have the same values", function() {
				for (var i = 0; i < this.clone.c.length; i++) {
					expect(this.clone.c[i]).toBe(this.source.c[i]);
				}
			});

			describe("when the values of items of an array-property of the shallow clone change", function() {
				beforeEach(function() {
					for (var i = 0; i < this.clone.c.length; i++) {
						this.clone.c[i]++;
					}
				});

				it("should affect the values in the source array", function() {
					for (var i = 0; i < this.clone.c.length; i++) {
						expect(this.clone.c[i]).toBe(this.source.c[i]);
					}
				});
			});
		});

		describe("when the properties of an object are shallow copied", function() {
			beforeEach(function() {
				this.destination = {
					d: 3,
					e: 4
				};
				this.destination = utils.shallowMixin(this.source, this.destination);
			});

			it("the destination is not the same as the source", function() {
				expect(this.destination).not.toBe(this.source);
			});

			it("the value-type properties of the destination have the same values as the source", function() {
				expect(this.destination.a).toBe(this.source.a);
				expect(this.destination.b).toBe(this.source.b);
			});

			it("the reference-type properties of the destination are not the same as the source", function() {
				expect(this.destination.c).toBe(this.source.c);
			});

			it("the properties of the destination that are arrays have the same length", function() {
				expect(this.destination.c.length).toBe(this.source.c.length);
			});

			it("the properties of the clone that are arrays have the same values", function() {
				for (var i = 0; i < this.destination.c.length; i++) {
					expect(this.destination.c[i]).toBe(this.source.c[i]);
				}
			});

			it("the other properties of the destination are left alone", function() {
				expect(this.destination.d).toBeDefined();
				expect(this.destination.e).toBeDefined();
			});

			describe("when the values of items of an array-property of the destination change", function() {
				beforeEach(function() {
					for (var i = 0; i < this.destination.c.length; i++) {
						this.destination.c[i]++;
					}
				});

				it("should affect the values in the source array", function() {
					for (var i = 0; i < this.destination.c.length; i++) {
						expect(this.destination.c[i]).toBe(this.source.c[i]);
					}
				});
			});
		});
	});
});