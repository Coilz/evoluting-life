# README #

This experiment is based on the genetic algorithm applied to a population of artificial creatures. There are two type of creatures: Plants and Animals. Creatures can live a maximum amount of time and are reproduced over time. Reproduction of the Animal genes occurs following the rules of [roulette wheel selection by rank](http://www.obitko.com/tutorials/genetic-algorithms/selection.php). The reproduction of Plant genes occurs randomly for now. Animals gain energy by eating Plants and Animals. They loose energy by spending time, moving, turning and being eaten. Plants grow over time.

## Set up
### Steps to run the app in a browser
```bash
git clone https://Coilz@bitbucket.org/Coilz/evoluting-life.git # Clone the repository
cd evoluting-life   # Change directory
npm install         # Install the npm dependencies
bower install       # Install the app dependencies
```
Open `evoluting-life/app/Evolution.html`

### Steps to run the jasmine tests in a browser
1. Download [jasmine-standalone](https://github.com/jasmine/jasmine/releases/download/v2.3.2/jasmine-standalone-2.3.2.zip)
2. Extract the zip file
3. Copy the contents of lib to `evoluting-life/spec/libs`

To run the tests, open `evoluting-life/spec/SpecRunner.html`

## Contribution guidelines
Please submit pull requests via feature branches using the semi-standard workflow of:

```bash
git clone git@bitbucket.org:yourUserName/evoluting-life.git    # Clone your fork
cd evoluting-life                                              # Change directory
git remote add upstream https://Coilz@bitbucket.org/Coilz/evoluting-life.git    # Assign original repository to a remote named 'upstream'
git fetch upstream                                             # Pull in changes not present in your local repository
git checkout -b my-new-feature                                 # Create your feature branch
git commit -am 'Add some feature'                              # Commit your changes
git push origin my-new-feature                                 # Push to the branch
```

Once you've pushed a feature branch to your forked repo, you're ready to open a pull request. We favor pull requests with very small, single commits with a single purpose.

## Contact

* Repo owner and admin : https://bitbucket.org/Coilz
* Follow me on Twitter: [@Coilz](https://twitter.com/Coilz)

## TODO
* mock Obesrver in `countersCollectorSpec`
* mock Obesrver in `statsCollectorSpec`
* fix failing jasmine tests for eyes and plant. The solution might be refactoring the `worldCanvas` module
* make sure there are 2 or more layers in `brains/brain` or make sure the code doesn't fail

[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
