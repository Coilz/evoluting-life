module.exports = function(grunt) {
   "use strict";

   var banner = '/*! <%= pkg.name %> version <%= pkg.version %>, <%= grunt.template.today("isoDateTime") %> */\n';

   // Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		ts: {
			default : {
				src: ['app/**/*.ts', '!node_modules/**/*.ts']
			}
		},
		watch: {
			gruntfile: {
				files: 'Gruntfile.js',
				tasks: ['jshint:gruntfile'],
			},
			src: {
				files: ['app/**/*.js', '!libs/**/*.js'],
				tasks: ['jshint:src', 'jasmine', 'uglify'],
			},
			test: {
				files: ['spec/**/*Spec.js'],
				tasks: ['jshint:test', 'jasmine'],
			}
		},

		jasmine: {
			run: {
				src: [
					'app/js/brains/**/*.js',
					'app/js/display/**/*.js',
					'app/js/entities/**/*.js',
					'app/js/evolution/**/*.js',
					'app/js/genetics/**/*.js',
					'app/js/observe/**/*.js',
					'app/js/sensors/**/*.js',
					'app/js/utils/**/*.js',

					'!app/libs/**/*.js',
					'!app/**/worldCanvas.js',
					'!app/**/main.js',
					'!app/**/foodSupply.js',
					'!app/**/*Printer.js',
					'!app/**/*Graph.js',
					'!app/**/*Diagram.js',
					'!app/**/*Div.js'
				],
				options: {
					vendor: [
						'app/libs/**/*.js',
					],
					specs: [
						'spec/js/brains/**/*Spec.js',
						'spec/js/display/**/*Spec.js',
						'spec/js/entities/**/*Spec.js',
						'spec/js/evolution/**/*Spec.js',
						'spec/js/genetics/**/*Spec.js',
						'spec/js/observe/**/*Spec.js',
						'spec/js/sensors/**/*Spec.js',
						'spec/js/utils/**/*Spec.js',
					],
					//helpers: 'spec/*Helper.js',
					template: require('grunt-template-jasmine-requirejs'),
					templateOptions: {
						requireConfig: {
							baseUrl: 'app/js/',
							// requireConfigFile: ['app/js/config.js', 'spec/config.js']
						}
					}
				}
			}
		},

		bower_concat: {
			all: {
				dest: 'build/_bower.js',
				cssDest: 'build/_bower.css',
			}
		},

		concat: {
			options: {
				banner: banner
			},
			build: {
				src: 'app/js/**/*.js',
				dest: 'build/<%= pkg.name %>.js'
			},
		},

		uglify: {
			options: {
				compress: {
					properties    : true,  // optimize property access: a["foo"] → a.foo
					comparisons   : true,  // optimize comparisons
					evaluate      : true,  // evaluate constant expressions
					booleans      : true,  // optimize boolean expressions
					loops         : true,  // optimize loops
					unused        : false, // drop unused variables/functions
					hoist_funs    : true,  // hoist function declarations
					hoist_vars    : true,  // hoist variable declarations
					join_vars     : true,  // join var declarations
					warnings      : true,  // warn about potentially dangerous optimizations/code
					drop_console  : true,  // discard calls to console.* functions
				},
				banner: banner
			},
			build: {
				src: 'app/js/**/*.js',
				dest: 'build/<%= pkg.name %>.min.js'
			}
		},

		jshint: {
			options: {
				force: true,
				ignores: ['spec/libs/**/*.js']
			},
			gruntfile: ['Gruntfile.js'],
			src: ['app/**/*.js'],
			test: ['spec/**/*.js'],
		}
	});

	// Load the plugins
	//require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

	grunt.loadNpmTasks('grunt-bower-concat');

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-jasmine');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-template-jasmine-requirejs');
	grunt.loadNpmTasks("grunt-ts");

	// Default task(s).
	grunt.registerTask('default', ['watch']);
	//grunt.registerTask('test', ['connect', 'jasmine']);
};